Pod::Spec.new do |s|

  s.name         = "MGeneratorFramework"
  s.version      = "0.0.1"
  s.summary      = "Base for shops"
  s.platform      = :ios
  s.description  = <<-DESC
                   Base for generating shops
                   DESC

  s.homepage     = "http://layouter.ovh"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "Adam Sliwakowski" => "sliwakowski@gmail.com" }

  s.ios.deployment_target = "8.0"

  s.source = { :git => "https://sliwakowski@bitbucket.org/sliwakowski/mg.git", :tag => "#{s.version}" }
  s.source_files  = "MGeneratorFramework/**/*.{h,m}", 'P24/P24.h', "MGeneratorFramework/Res/*",
  s.resources = "MGeneratorFramework/Assets/*"

  s.frameworks = 'SystemConfiguration', 'Security', 'MapKit', 'QuartzCore', 'CoreImage', 'CoreData', 'Foundation', 'CoreGraphics', 'UIKit'

  s.dependency 'MagicalRecord'
  s.dependency 'SDWebImage'
  s.dependency 'MBProgressHUD'
  s.dependency 'AFNetworking'

  s.xcconfig = { 'OTHER_LDFLAGS' => '-lstdc++' }
  s.libraries = 'xml2', 'z'

  s.vendored_libraries = 'MGeneratorFramework/P24/libP24-1.4.3.a'

  s.prefix_header_file = 'MGeneratorFramework/PrefixHeader.pch'

  s.requires_arc = true

end