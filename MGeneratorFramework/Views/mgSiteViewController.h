//
//  mgSiteViewController.h
//  aplikacja
//
//  Created by Macbook Pro on 05.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgNavigationBarHandler.h"
#import "mgNavigationController.h"
#import "Strona.h"
#import "mgTextHelper.h"
@interface mgSiteViewController : UIViewController <mgNavigationBarHandlerDelegate>

@property (nonatomic, strong) Strona*                       site;
@property (nonatomic, strong) NSString*                     webpageUrl;

@property (nonatomic, strong) IBOutlet UIWebView*           webView;

@property (nonatomic, strong) mgNavigationController*       navController;

@property (nonatomic, strong) UILabel*                      titleLabel;
@property (nonatomic, strong) UIButton*                     favoritesButton;
@property (nonatomic, strong) UIButton*                     basketButton;

@end
