//
//  mgMapViewController.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 14.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgJSONHandler.h"
#import <MapKit/MapKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "mgNavigationController.h"
#import "mgNavigationBarHandler.h"
@interface mgMapViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, mgNavigationBarHandlerDelegate>

@property (nonatomic, strong) IBOutlet MKMapView* mapView;
@property (nonatomic, strong) IBOutlet UITableView* tableView;
@property (nonatomic, strong) IBOutlet UIBarButtonItem* prowadzButton;
@property (nonatomic, strong) NSMutableArray* locationsArray;
@property (nonatomic, strong) NSDictionary* selectedLocationObject;
@property (nonatomic, strong) NSMutableArray* annotationsArray;
@property (nonatomic, strong) NSMutableArray* distanceArray;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (nonatomic, assign) UIInterfaceOrientation orientation;

@property (nonatomic, strong) mgNavigationController*       navController;

@property (nonatomic, strong) UILabel*                      titleLabel;
@property (nonatomic, strong) UIButton*                     favoritesButton;
@property (nonatomic, strong) UIButton*                     basketButton;
@end
