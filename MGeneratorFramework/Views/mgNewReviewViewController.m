//
//  mgNewReviewViewController.m
//  aplikacja
//
//  Created by Macbook Pro on 05.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgNewReviewViewController.h"
#import "MBProgressHUD.h"
@interface mgNewReviewViewController ()

@end

@implementation mgNewReviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // NICK BOTTOM BORDER
    CALayer *nickBottomBorder = [CALayer layer];
    nickBottomBorder.frame = CGRectMake(_nickTextField.frame.origin.x, _nickTextField.frame.origin.y+_nickTextField.frame.size.height+5, _nickTextField.frame.size.width, 1.0f);
    nickBottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    [self.view.layer addSublayer:nickBottomBorder];
    //RATING BAR
    _currentRating = 5;
    [self refreshRatingBar];
    //COMMENT TEXT
    _commentPlaceholder = _commentTextView.text;
}
- (void)viewWillAppear:(BOOL)animated {
    //NAVIGATION BAR
    mgNavigationBarHandler* navigationBarHandler = [mgNavigationBarHandler new];
    navigationBarHandler.delegate = self;
    [navigationBarHandler configureNavigationBar:self.navigationController];
    _titleLabel = [navigationBarHandler titleLabel:@"Komentarz"];
    _sendButton = [navigationBarHandler rightButtonWithTitle:@"Wyślij"];
    [_sendButton addTarget:self action:@selector(submitForm) forControlEvents:UIControlEventTouchUpInside];
}
-(void)viewDidAppear:(BOOL)animated {
    [[self navigationController] setToolbarHidden:YES animated:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [_titleLabel removeFromSuperview];
    [_sendButton removeFromSuperview];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewWillDisappear:YES];
    [self viewWillAppear:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Rating Bar
- (void)refreshRatingBar{
    // AVERAGE RATE
    [_ratingBar removeFromSuperview];
    _ratingBar = [[mgFullStarView alloc] initWithSize:30 activeStars:_currentRating maxStars:5 activeStarColor:UIColorFromRGB(0xd5375f) inactiveStarColor:UIColorFromRGB(0x666666)];
    UITapGestureRecognizer* ratingBarTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleRatingBarTapGesture:)];
    [_ratingBar addGestureRecognizer:ratingBarTapRecognizer];
    [_ratingView addSubview:_ratingBar];
}
- (void)handleRatingBarTapGesture:(UITapGestureRecognizer*)recognizer{
    CGPoint point = [recognizer locationInView:_ratingView];
    float tapOffsetX = point.x;
    _currentRating =  (tapOffsetX / (_ratingBar.starSize+_ratingBar.starsMargin))+1;
    [self refreshRatingBar];
}
#pragma mark - UITextView Delegate
- (void) textViewDidBeginEditing:(UITextView *) textView {
    if([textView.text isEqualToString:_commentPlaceholder]){
        [textView setText:@""];
    }
}
#pragma mark - Form submit
- (void) submitForm {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.margin = 10.f;
    hud.yOffset = 30.f;
    hud.hidden = YES;
    if([_nickTextField.text length]<1){
        //NICK VALIDATION
        hud.hidden = NO;
        hud.labelText = @"Wpisz swój nick";
        [hud hide:YES afterDelay:1];
    }
    else if([_commentTextView.text length]<1 || [_commentTextView.text isEqualToString:_commentPlaceholder]) {
        // COMMENT VALIDATION
        hud.hidden = NO;
        hud.labelText = @"Wpisz komentarz";
        [hud hide:YES afterDelay:1];
    }
    else {
        MBProgressHUD* progressHud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        progressHud.mode = MBProgressHUDModeIndeterminate;
        progressHud.labelText = @"Wysyłam komentarz...";
        progressHud.removeFromSuperViewOnHide = YES;

        [mgPOSTHandler postCommentWithProductId:[NSString stringWithFormat:@"%i", [_produkt.identyfikator intValue]] comment:_commentTextView.text nick:_nickTextField.text rate:_currentRating completionHandler:^(NSString *response) {
            if([response isEqualToString:@"1"]){
                [progressHud hide:YES];
                hud.hidden = NO;
                hud.removeFromSuperViewOnHide = YES;
                hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
                hud.mode = MBProgressHUDModeCustomView;
                hud.labelText = @"Wysłano komentarz";
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                hud.hidden = NO;
                hud.mode = MBProgressHUDModeText;
                hud.labelText = @"Wystąpił problem przy wysyłaniu";;
                hud.removeFromSuperViewOnHide = YES;
            }
            [hud hide:YES afterDelay:1];
        }];

    }
}
#pragma mark - Modal Views
- (void)goToMainView{
    [_navController goToMainView];
}
- (void)goToBasket{
    [_navController goToBasketAnimated:YES];
}
- (void)goToFavorites{
    [_navController goToFavoritesAnimated:YES];
}
- (void)openMenu{
    [_navController openMenu];
}

@end
