//
//  mgFavoritesViewController.m
//  aplikacja
//
//  Created by Macbook Pro on 24.05.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgFavoritesViewController.h"
#import <MagicalRecord/MagicalRecord.h>

@interface mgFavoritesViewController ()

@end

@implementation mgFavoritesViewController

#pragma mark - INIT METHODS

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    _navController = (mgNavigationController*) self.navigationController;
    //EDYTUJ ZAMOWIENIE BORDER
    _edytujButton.layer.cornerRadius = 5.0f;
    _edytujButton.layer.borderWidth = 1.0;
    _edytujButton.layer.borderColor = [mgTextHelper mainColor].CGColor;
    _edytujButton.layer.masksToBounds = YES;
    [self getData];
}
-(void)viewDidAppear:(BOOL)animated {
    [[self navigationController] setToolbarHidden:YES animated:YES];
}
- (void)getData {
    
    NSNumber* ulubione = [NSNumber numberWithBool:YES];
    _koszykArray = (NSArray*) [Koszyk MR_findByAttribute:@"ulubione" withValue:ulubione];
    _products = [NSMutableArray new];
    for(Koszyk* koszyk in _koszykArray) {
        Produkt* singleKoszykProdukt = [Produkt MR_findFirstByAttribute:@"identyfikator" withValue:koszyk.identyfikator];
        [_products addObject:singleKoszykProdukt];
    }
    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void)viewWillAppear:(BOOL)animated {
    UIInterfaceOrientation oldOrientation = _orientation;
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(oldOrientation!=_orientation){
        [_navController popViewControllerAnimated:NO];
        [_navController goToFavoritesAnimated:NO];
    }
    //NAVIGATION BAR
    mgNavigationBarHandler* navigationBarHandler = [mgNavigationBarHandler new];
    navigationBarHandler.delegate = self;
    [navigationBarHandler configureNavigationBar:self.navigationController];
    _favoritesButton = [navigationBarHandler favoritesButton];
    [_favoritesButton addTarget:self action:@selector(goToFavorites) forControlEvents:UIControlEventTouchUpInside];
    _basketButton = [navigationBarHandler basketButton];
    [_basketButton addTarget:self action:@selector(goToBasket) forControlEvents:UIControlEventTouchUpInside];
    _homeButton = [navigationBarHandler homeButton];
    [_homeButton addTarget:self action:@selector(goToMainView) forControlEvents:UIControlEventTouchUpInside];
}
- (void)viewWillDisappear:(BOOL)animated {
    [_favoritesButton removeFromSuperview];
    [_homeButton removeFromSuperview];
    [_basketButton removeFromSuperview];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewWillDisappear:YES];
    [self viewWillAppear:YES];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if([_products count]==0){
        _edytujButton.hidden=YES;
        _brakLabel.hidden=NO;
    } else {
        _edytujButton.hidden=NO;
        _brakLabel.hidden=YES;
    }
    return [_products count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    mgTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"productCell" forIndexPath:indexPath];
    
    Produkt*produkt = [_products objectAtIndex:indexPath.row];
    //ZDJECIE
    UIImageView*image = (UIImageView*) [cell viewWithTag:1];
    [image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@&w=500&h=500",produkt.mainImage]]
          placeholderImage:[UIImage imageNamed:@"iphoneList.png"]];
    
    //TITLE
    UITextView*title = (UITextView*) [cell viewWithTag:2];
    title.text = produkt.name;
    [title setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
	[title setTextColor:UIColorFromRGB(0x939393)];
    //DOSTEPNY
    UILabel*dostepny = (UILabel*) [cell viewWithTag:7];
    dostepny.text = produkt.disable ? @"Dostępny" : @"Niedostępny";
    //NOWA CENA
    UILabel*nowaCena = (UILabel*) [cell viewWithTag:4];
    [mgTextHelper configureNewPriceLabel:nowaCena withProdukt:produkt];
    nowaCena.frame = CGRectMake(cell.frame.size.width-nowaCena.frame.size.width-5, nowaCena.frame.origin.y, nowaCena.frame.size.width, nowaCena.frame.size.height);
    //STARA CENA
    UILabel*staraCena = (UILabel*) [cell viewWithTag:5];
    [mgTextHelper configureOldPriceLabel:staraCena withNewPriceLabel:nowaCena withProdukt:produkt];
    //KROPKA
    UIView*kropka = [cell viewWithTag:6];
    [kropka setBackgroundColor:[mgTextHelper mainColor]];
    [kropka.layer setCornerRadius:3.0f];
    if([produkt.disable intValue]!=0) {
        title.textColor = UIColorFromRGB(0xdddddd);
        nowaCena.textColor = UIColorFromRGB(0xd4d4d4);
        kropka.backgroundColor = UIColorFromRGB(0xd4d4d4);
        dostepny.textColor = UIColorFromRGB(0xd4d4d4);
    }
    
    //BOOL bestseller = produkt.isBestseller;
    //BOOL promocja = produkt.isPromotion;
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Produkt* produkt = [_products objectAtIndex:indexPath.row];
    Koszyk* koszyk = [_koszykArray objectAtIndex:indexPath.row];
    [_navController goToProduct:produkt fromFavorite:koszyk animated:YES];
}

#pragma mark - DELETE METHODS
- (IBAction)toggleTableViewEditing:(id)sender {
    if(self.tableView.editing==YES){
        [self.tableView setEditing:NO animated:YES];
        [self.menuContainerViewController setMenuSlideAnimationEnabled:YES];
        [_edytujButton setTitle:@"Edytuj ulubione" forState:UIControlStateNormal];
    }
    else {
        [self.tableView setEditing:YES animated:YES];
        [self.menuContainerViewController setMenuSlideAnimationEnabled:NO];
        [_edytujButton setTitle:@"Zakończ edycję" forState:UIControlStateNormal];
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        Koszyk* elementKoszyka = [_koszykArray objectAtIndex:indexPath.row];
        [_products removeObjectAtIndex:indexPath.row];
        [elementKoszyka MR_deleteEntity];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"Usunięto produkt z ulubionych";
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:1];
    }
}

#pragma mark - SEGUE METHOD

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        //HIDE TOOLBAR
        [[self navigationController] setToolbarHidden:YES animated:YES];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Produkt*produkt = [_products objectAtIndex:indexPath.row];
        [[segue destinationViewController] setDetailItem:produkt];
    }
}
#pragma mark - Modal Views
- (void)goToMainView{
    [_navController goToMainView];
}
- (void)goToBasket{
    [_navController goToBasketAnimated:YES];
}
- (void)goToFavorites{
    [_navController goToFavoritesAnimated:YES];
}
- (void)openMenu{
    [_navController openMenu];
}
@end
