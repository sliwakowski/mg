//
//  mgReviewCellView.m
//  aplikacja
//
//  Created by Macbook Pro on 04.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgReviewCellView.h"
#import <QuartzCore/QuartzCore.h>
#import "mgStarView.h"
#import "mgFullStarView.h"
@implementation mgReviewCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}
- (id)initWithComment:(Komentarz*)comment atPosition:(float)position
{
    float height = 200;
    _comment = comment;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    CGRect frame = CGRectMake(0, position, IDIOM==IPAD ? 728 : 320, height);
    if(orientation==UIInterfaceOrientationLandscapeLeft || orientation==UIInterfaceOrientationLandscapeRight) {
        frame = CGRectMake(252, position, 480, height);
    }
    self = [super initWithFrame:frame];
    if (self) {
        [self createNameLabel];
        [self createDescriptionLabel];
        [self calculateAndSetCellHeight];
        [self createStars];
    }
    return self;
}
- (void)createNameLabel{
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 150, 25)];
    _nameLabel.text = _comment.nick;
    _descriptionLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f];
    _nameLabel.textColor = [UIColor colorWithWhite:0.6 alpha:1];
    [self addSubview:_nameLabel];
}
- (void)createDescriptionLabel{
    _descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, _nameLabel.frame.origin.y+_nameLabel.frame.size.height+5, 300, 20)];
    _descriptionLabel.text = _comment.comment;
    _descriptionLabel.textColor = [UIColor colorWithWhite:0.6 alpha:1];
    _descriptionLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
    _descriptionLabel.numberOfLines = 0;
    _descriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_descriptionLabel sizeToFit];
    [self addSubview:_descriptionLabel];
}
- (void)calculateAndSetCellHeight{
    _cellHeight = _descriptionLabel.frame.origin.y+_descriptionLabel.frame.size.height+5;
    CGRect newFrame = self.frame;
    newFrame.size.height = _cellHeight;
    self.frame = newFrame;
}
- (void)createStars{
    mgFullStarView* activeStars = [[mgFullStarView alloc] initWithSize:15 activeStars:[_comment.rate intValue] maxStars:5 activeStarColor:UIColorFromRGB(0xd5375f) inactiveStarColor:UIColorFromRGB(0x666666)];
    [self addSubview:activeStars];
    CGRect activeStarsFrame =activeStars.frame;
    if(IDIOM==IPAD){
        activeStarsFrame.origin.x = self.frame.size.width - activeStarsFrame.size.width - 10;
    }
    else {
        activeStarsFrame.origin.x = self.frame.size.width - activeStarsFrame.size.width - 10;    }
    activeStarsFrame.origin.y = 7;
    activeStars.frame = activeStarsFrame;
}
@end
