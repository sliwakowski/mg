//
//  mgFullStarView.h
//  aplikacja
//
//  Created by Macbook Pro on 04.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mgFullStarView : UIView

- (id)initWithSize:(float)starSize activeStars:(float)activeStars maxStars:(int)maxStars activeStarColor:(UIColor*)activeStarColor inactiveStarColor:(UIColor*)inactiveStarColor;
@property (nonatomic, assign) float starSize;
@property (nonatomic, assign) float starsMargin;
@property (nonatomic, assign) int maxStars;
@property (nonatomic, assign) int activeStars;
@property (nonatomic, strong) UIColor* activeStarColor;
@property (nonatomic, strong) UIColor* inactiveStarColor;
@end
