//
//  mgFavoritesViewController.h
//  aplikacja
//
//  Created by Macbook Pro on 24.05.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgNavigationBarHandler.h"
#import "mgDetailViewController.h"
#import "mgNavigationController.h"
#import "mgTableViewCell.h"
@interface mgFavoritesViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, mgNavigationBarHandlerDelegate>

@property (strong, nonatomic) NSArray*                      koszykArray;
@property (nonatomic, strong) NSMutableArray*               products;

@property (nonatomic, strong) IBOutlet UIButton*            edytujButton;
@property (nonatomic, strong) IBOutlet UILabel*             brakLabel;
@property (nonatomic, strong) mgNavigationController*       navController;
@property (nonatomic, strong) UIButton*                     homeButton;
@property (nonatomic, strong) UIButton*                     favoritesButton;
@property (nonatomic, strong) UIButton*                     basketButton;
@property (nonatomic, assign) UIInterfaceOrientation        orientation;
@end
