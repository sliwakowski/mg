//
//  mgNavigationController.h
//  aplikacja
//
//  Created by Macbook Pro on 31.05.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgNavigationBarHandler.h"
#import "MFSideMenu.h"
#import "Strona.h"
#import "Kategoria.h"
#import "Produkt.h"
#import "Koszyk.h"
#import "Reachability.h"
@interface mgNavigationController : UINavigationController

@property (nonatomic, strong) UIStoryboard*                 mainStoryboard;
@property (nonatomic, strong) UIStoryboard*                 landscapeStoryboard;
@property (nonatomic, strong) Reachability*                 reach;
- (void)goToMainView;
- (void)goToBasketAnimated:(BOOL)animated;
- (void)goToFavoritesAnimated:(BOOL)animated;
- (void)openMenu;
- (void)openSite:(Strona*)site;
- (void)openWebpage:(NSString*)url;

- (void)goToPage:(NSString*)pageName animated:(BOOL)animated;
- (void)goToProduct:(Produkt*)produkt animated:(BOOL)animated;
- (void)goToProduct:(Produkt*)produkt fromBasket:(Koszyk*)koszyk animated:(BOOL)animated;
- (void)goToProduct:(Produkt*)produkt fromFavorite:(Koszyk*)koszyk animated:(BOOL)animated;
- (void)loadAllSubcategories:(NSArray*)subcategories;
- (void)loadProductsFilteredBy:(NSString*)query;
- (void)goToCategory:(Kategoria*)kategoria;
- (void)goToLoginWithBasket:(UIViewController*)basketVC animated:(BOOL)animated;
@end
