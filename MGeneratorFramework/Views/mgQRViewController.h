//
//  mgQRViewController.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 14.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDZQRScanningViewController.h"
#import "mgNavigationController.h"
#import "mgNavigationBarHandler.h"
#import "mgTextHelper.h"
@interface mgQRViewController : UIViewController <mgNavigationBarHandlerDelegate>
@property (nonatomic, assign) UIInterfaceOrientation orientation;
@property (nonatomic, strong) NSMutableArray* qrCodeHistory;
@property (nonatomic, strong) IBOutlet UITableView* tableView;
@property (nonatomic, strong) mgNavigationController*       navController;
@property (nonatomic, strong) UILabel*                      titleLabel;
@property (nonatomic, strong) UIButton*                     favoritesButton;
@property (nonatomic, strong) UIButton*                     basketButton;

@end
