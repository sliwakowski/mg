//
//  mgStarView.m
//  aplikacja
//
//  Created by Macbook Pro on 04.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgStarView.h"

@implementation mgStarView

- (id)initWithSize:(float)starSize maxStars:(float)maxStars activeStarColor:(UIColor*)activeStarColor
{
    _starMargin = starSize/5;
    CGRect containerFrame = CGRectMake(0, 0, starSize*maxStars+_starMargin*(int)maxStars, starSize);
    
    self = [super initWithFrame:containerFrame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        _starSize = starSize;
        _maxStars = maxStars;
        _activeStarColor = activeStarColor;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    int aSize = _starSize;
    CGColorRef aColor = _activeStarColor.CGColor;
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, aSize);
    CGFloat xCenter = aSize/2;
    CGFloat yCenter = aSize/2;
    
    float  w = _starSize;
    double r = w / 2.0;
    float flip = -1.0;
    
    for (NSUInteger i=0; i<_maxStars; i++)
    {
        
        CGContextSetFillColorWithColor(context, aColor);
        CGContextSetStrokeColorWithColor(context, aColor);
        
        double theta = 2.0 * M_PI * (2.0 / 5.0); // 144 degrees
        
        CGContextMoveToPoint(context, xCenter, r*flip+yCenter);
        
        for (NSUInteger k=1; k<5; k++)
        {
            float x = r * sin(k * theta);
            float y = r * cos(k * theta);
            CGContextAddLineToPoint(context, x+xCenter, y*flip+yCenter);
        }
        xCenter += aSize+_starMargin;
    }
    CGContextClosePath(context);
    CGContextFillPath(context);
}
@end
