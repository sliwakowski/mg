//
//  mgCategoriesTableViewController.h
//  aplikacja
//
//  Created by Macbook Pro on 19.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgNavigationBarHandler.h"
#import "mgNavigationController.h"
#import "Kategoria.h"
@interface mgCategoriesTableViewController : UITableViewController <mgNavigationBarHandlerDelegate>

@property (nonatomic, strong) mgNavigationController*       navController;
@property (nonatomic, strong) Kategoria* kategoria;
@property (nonatomic, strong) NSMutableArray* categories;


@property (nonatomic, strong) UIButton*                     homeButton;
@property (nonatomic, strong) UIButton*                     favoritesButton;
@property (nonatomic, strong) UIButton*                     basketButton;

@end
