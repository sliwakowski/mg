//
//  mgFullStarView.m
//  aplikacja
//
//  Created by Macbook Pro on 04.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgFullStarView.h"
#import "mgStarView.h"
#import "mgTextHelper.h"
@implementation mgFullStarView

- (id)initWithSize:(float)starSize activeStars:(float)activeStars maxStars:(int)maxStars activeStarColor:(UIColor*)activeStarColor inactiveStarColor:(UIColor*)inactiveStarColor
{
    _starsMargin = starSize/5;
    CGRect containerFrame = CGRectMake(0, 0, (starSize+_starsMargin)*maxStars, starSize);
    self = [super initWithFrame:containerFrame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        _starSize = starSize;
        _activeStars = activeStars;
        _maxStars = maxStars;
        //_activeStarColor = activeStarColor;
        //activeStarColor = [mgTextHelper mainColor];
        activeStarColor = UIColorFromRGB(0xfcc60a);
        
        int doubledActiveStars = 2*activeStars;
        activeStars = doubledActiveStars/2.0;
        mgStarView* inactiveStarsView = [[mgStarView alloc] initWithSize:starSize maxStars:5 activeStarColor:inactiveStarColor];
        [self addSubview:inactiveStarsView];
        CGRect newInactiveStarsViewFrame =inactiveStarsView.frame;
        newInactiveStarsViewFrame.origin.x = 0;
        inactiveStarsView.frame = newInactiveStarsViewFrame;
        mgStarView* activeStarsView = [[mgStarView alloc] initWithSize:starSize maxStars:activeStars activeStarColor:activeStarColor];
        [self addSubview:activeStarsView];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
