//
//  mgNavigationController.m
//  aplikacja
//
//  Created by Macbook Pro on 31.05.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgNavigationController.h"
#import "mgSiteViewController.h"
#import "mgCategoriesTableViewController.h"
#import "mgDetailViewController.h"

#import "mgBasketViewController.h"
#import "mgLoginViewController.h"
#import <MagicalRecord/MagicalRecord.h>
@interface mgNavigationController ()

@end

@implementation mgNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(IDIOM==IPAD){
        _mainStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle: nil];
    }
    else {
        _mainStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    }
    _landscapeStoryboard = [UIStoryboard storyboardWithName:@"Landscape_iPad" bundle: nil];
    
    // Allocate a reachability object
    _reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    // Tell the reachability that we DON'T want to be reachable on 3G/EDGE/CDMA
    //_reach.reachableOnWWAN = NO;
    
    // Here we set up a NSNotification observer. The Reachability that caused the notification
    // is passed in the object parameter
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    [_reach startNotifier];
}
- (void)reachabilityChanged:(NSNotification *) notification{
    if([_reach isReachable])
    { }
    else
    { }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Menu Handler
- (IBAction)toggleMenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{}];
}

#pragma mark - Modal Views
- (IBAction)showBasket:(id)sender {
   UIViewController* basketVC  = [_mainStoryboard instantiateViewControllerWithIdentifier: @"basketVC"];
    [self pushViewController:basketVC animated:YES];
}
- (IBAction)showFavorites:(id)sender {
    UIViewController* favoritesVC  = [_mainStoryboard instantiateViewControllerWithIdentifier: @"favoritesVC"];
    [self pushViewController:favoritesVC animated:YES];
}
- (IBAction)showSite:(id)sender withSite:(Strona*)site{
    mgSiteViewController* siteVC  = [_mainStoryboard instantiateViewControllerWithIdentifier: @"siteVC"];
    siteVC.site = site;
    [self pushViewController:siteVC animated:YES];
}

- (void)goToMainView{
    [self popToRootViewControllerAnimated:YES];
    NSMutableDictionary*userInfo = [NSMutableDictionary new];
    [userInfo setObject:@"all" forKey:@"query"];
    [userInfo setObject:@"type" forKey:@"type"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"filterOffers" object:userInfo];

}
- (void)goToBasketAnimated:(BOOL)animated{
    UIViewController* vc  = [_mainStoryboard instantiateViewControllerWithIdentifier: @"basketVC"];
    if([mgTextHelper isLandscape]) vc = [_landscapeStoryboard instantiateViewControllerWithIdentifier: @"basketVC"];
    [self pushViewController:vc animated:animated];
}
- (void)goToFavoritesAnimated:(BOOL)animated{
    UIViewController* vc  = [_mainStoryboard instantiateViewControllerWithIdentifier: @"favoritesVC"];
    if([mgTextHelper isLandscape]) vc = [_landscapeStoryboard instantiateViewControllerWithIdentifier: @"favoritesVC"];
    [self pushViewController:vc animated:YES];
}
- (void)openMenu{
    [self toggleMenu:nil];
}
- (void)openSite:(Strona*)site{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    mgSiteViewController* vc  = [_mainStoryboard instantiateViewControllerWithIdentifier: @"siteVC"];
    vc.site = site;
    [self pushViewController:vc animated:YES];
}
- (void)openWebpage:(NSString*)url{
    mgSiteViewController* vc  = [_mainStoryboard instantiateViewControllerWithIdentifier: @"siteVC"];
    vc.webpageUrl = url;
    [self pushViewController:vc animated:YES];
    
}
- (void)goToPage:(NSString*)pageName animated:(BOOL)animated{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    UIViewController* vc  = [_mainStoryboard instantiateViewControllerWithIdentifier: pageName];
    if([mgTextHelper isLandscape]) vc = [_landscapeStoryboard instantiateViewControllerWithIdentifier: pageName];
    [self pushViewController:vc animated:animated];
}
- (void)goToCategory:(Kategoria*)kategoria{
    mgCategoriesTableViewController* vc = [_mainStoryboard instantiateViewControllerWithIdentifier:@"categoriesVC"];
    vc.kategoria = kategoria;
    [self pushViewController:vc animated:YES];
}
- (void)loadProductsFilteredBy:(NSString*)query{
    NSMutableDictionary*userInfo = [NSMutableDictionary new];
    [userInfo setObject:@"type" forKey:@"type"];
    [userInfo setObject:query forKey:@"query"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"filterOffers" object:userInfo];
}
- (void)loadAllSubcategories:(NSArray*)subcategories{
    [self popToRootViewControllerAnimated:NO];
    NSMutableArray* mutableSubcategories = [NSMutableArray new];
    for(Kategoria*subcategory in subcategories){
        [mutableSubcategories addObject:subcategory];
        NSArray* subsubcategories = [Kategoria MR_findByAttribute:@"parentId" withValue:subcategory.identyfikator];
        for(Kategoria*subsubcategory in subsubcategories){
            [mutableSubcategories addObject:subsubcategory];
        }
    }
    NSMutableDictionary*userInfo = [NSMutableDictionary new];
    [userInfo setObject:@"podkategorie" forKey:@"type"];
    [userInfo setObject:mutableSubcategories forKey:@"subcategories"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"filterOffers" object:userInfo];
}
- (void)goToProduct:(Produkt*)produkt animated:(BOOL)animated{
    mgDetailViewController* vc = [_mainStoryboard instantiateViewControllerWithIdentifier:@"detailVC"];
    if([mgTextHelper isLandscape]) vc = [_landscapeStoryboard instantiateViewControllerWithIdentifier: @"detailVC"];
    vc.produkt = produkt;
    [self pushViewController:vc animated:animated];
}
- (void)goToProduct:(Produkt*)produkt fromBasket:(Koszyk*)koszyk animated:(BOOL)animated{
    mgDetailViewController* vc = [_mainStoryboard instantiateViewControllerWithIdentifier:@"detailVC"];
    if([mgTextHelper isLandscape]) vc = [_landscapeStoryboard instantiateViewControllerWithIdentifier: @"detailVC"];
    vc.produkt = produkt;
    vc.koszyk = koszyk;
    [self pushViewController:vc animated:animated];
}
- (void)goToProduct:(Produkt*)produkt fromFavorite:(Koszyk*)koszyk animated:(BOOL)animated{
    mgDetailViewController* vc = [_mainStoryboard instantiateViewControllerWithIdentifier:@"detailVC"];
    if([mgTextHelper isLandscape]) vc = [_landscapeStoryboard instantiateViewControllerWithIdentifier: @"detailVC"];
    vc.produkt = produkt;
    vc.ulubiony = koszyk;
    [self pushViewController:vc animated:animated];
}
- (void)goToLoginWithBasket:(UIViewController*)basketVC animated:(BOOL)animated{
    mgLoginViewController* vc = [_mainStoryboard instantiateViewControllerWithIdentifier:@"loginVC"];
    if([mgTextHelper isLandscape]) vc = [_landscapeStoryboard instantiateViewControllerWithIdentifier: @"loginVC"];
    vc.basketVC=(mgBasketViewController*)basketVC;
    [self pushViewController:vc animated:YES];
}


@end
