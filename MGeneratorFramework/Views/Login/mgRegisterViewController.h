//
//  mgRegisterViewController.h
//  aplikacja
//
//  Created by Macbook Pro on 07.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgNavigationBarHandler.h"
#import "mgTextHelper.h"
#import "mgNavigationController.h"
#import "mgPOSTHandler.h"

@interface mgRegisterViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView*        scrollView;
@property (nonatomic, strong) IBOutlet UITextField*         nazwaUzytkownikaTextField;
@property (nonatomic, strong) IBOutlet UITextField*         hasloTextField;
@property (nonatomic, strong) IBOutlet UITextField*         powtorzHasloTextField;
@property (nonatomic, strong) IBOutlet UITextField*         emailTextField;
@property (nonatomic, strong) IBOutlet UITextField*         miastoTextField;
@property (nonatomic, strong) IBOutlet UITextField*         ulicaTextField;
@property (nonatomic, strong) IBOutlet UITextField*         kodPocztowyTextField;

@property (nonatomic, assign) BOOL                          isKeyboardOpened;

@property (nonatomic, strong) mgNavigationController*       navController;
@property (nonatomic, strong) UILabel*                      titleLabel;
@property (nonatomic, strong) UIButton*                     sendButton;
@end
