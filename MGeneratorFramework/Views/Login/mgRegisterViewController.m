//
//  mgRegisterViewController.m
//  aplikacja
//
//  Created by Macbook Pro on 07.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgRegisterViewController.h"
#import "mgTextHelper.h"
#import "MBProgressHUD.h"
#import "NSString+Base64.h"
@interface mgRegisterViewController ()

@end

@implementation mgRegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    float scrollViewContentHeight = 10;
    [super viewDidLoad];
    
    NSArray*textFields = [_scrollView subviews];
    for(NSObject*view in textFields){
        if([view isKindOfClass:[UITextField class]]){
            UITextField* textField = (UITextField*) view;
            textField.delegate = self;
            CALayer *textFieldBottomBorder = [CALayer layer];
            textFieldBottomBorder.frame = CGRectMake(0, textField.frame.size.height-1, textField.frame.size.width, 1.0f);
            textFieldBottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
            [textField.layer addSublayer:textFieldBottomBorder];
            scrollViewContentHeight+=textField.frame.size.height;
        }
    }
    
    //CODE FOR SMALL IPHONE
    if(!IsIphone5){
        _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y+88, _scrollView.frame.size.width, _scrollView.frame.size.height);
        scrollViewContentHeight+=88;
    }
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, scrollViewContentHeight);
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated {
    //NAVIGATION BAR
    mgNavigationBarHandler* navigationBarHandler = [mgNavigationBarHandler new];
    [navigationBarHandler configureNavigationBar:self.navigationController];
    _titleLabel = [navigationBarHandler titleLabel:@"Użytkownik"];
    _sendButton = [navigationBarHandler rightButtonWithTitle:@"Rejestruj"];
    [_sendButton addTarget:self action:@selector(registerUser) forControlEvents:UIControlEventTouchUpInside];
}
- (void)viewWillDisappear:(BOOL)animated {
    [_titleLabel removeFromSuperview];
    [_sendButton removeFromSuperview];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Buttons responders 
-(void)loginUser{
    
}
-(void)registerUser{
    MBProgressHUD *completed = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    NSString*imienazwisko = _nazwaUzytkownikaTextField.text;
    NSString*haslo = _hasloTextField.text;
    NSString*haslo2 = _powtorzHasloTextField.text;
    NSString*email = _emailTextField.text;
    NSString*ulica = _ulicaTextField.text;
    NSString*miasto = _miastoTextField.text;
    NSString*kodpocztowy = _kodPocztowyTextField.text;
    NSString* registerFormValidationMessage = [mgTextHelper isRegistrationFormValid:imienazwisko haslo:haslo haslo2:haslo2 email:email ulica:ulica miasto:miasto kodpocztowy:kodpocztowy];
    if(registerFormValidationMessage.length==0){
        NSString* base64Password = [haslo base64];
        NSDictionary* deliveryAddress = @{@"name": imienazwisko, @"address": ulica, @"city":miasto, @"post_code":kodpocztowy};
        NSDictionary* fullData = @{@"email": email, @"password":base64Password ,@"delivery_address":deliveryAddress};

        
        
        
        
        
        [mgPOSTHandler registerUserWith:fullData update:NO completionHandler:^(NSDictionary *response) {
            if([response[@"status"] isEqual:@1]){
                [mgPOSTHandler postCheckUserLogin:email withPassword:haslo completionHandler:^(NSString *response) {
                    
                    completed.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
                    completed.mode = MBProgressHUDModeCustomView;
                    completed.labelText = @"Pomyślnie zarejestrowano";
                    //LOGIN COMPLETED
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshMenu" object:nil];
                }];
            }
            else {
                completed.mode = MBProgressHUDModeText;
                if(response[@"error"][@"info"]) completed.labelText = response[@"error"][@"info"];
                else completed.labelText = @"Wystąpił problem przy rejestracji";
            }
        }];
    }
    else {
        completed.mode = MBProgressHUDModeText;
        completed.labelText = registerFormValidationMessage;
    }
    
    completed.removeFromSuperViewOnHide = YES;
    [completed hide:YES afterDelay:2];
    
}
#pragma mark - Text Field Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if(!_isKeyboardOpened){
        float keyboardHeight = 216;
        CGRect newScrollViewFrame = _scrollView.frame;
        newScrollViewFrame.size.height = self.view.frame.size.height-keyboardHeight;
        _scrollView.frame = newScrollViewFrame;
        _isKeyboardOpened = YES;
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField {
    if(!_isKeyboardOpened){
        CGRect newScrollViewFrame = _scrollView.frame;
        newScrollViewFrame.size.height = 508;
        _scrollView.frame = newScrollViewFrame;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    _isKeyboardOpened = NO;
    [textField resignFirstResponder];
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
