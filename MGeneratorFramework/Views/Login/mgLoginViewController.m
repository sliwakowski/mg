//
//  mgLoginViewController.m
//  aplikacja
//
//  Created by Macbook Pro on 05.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgLoginViewController.h"
#import "mgPOSTHandler.h"
@interface mgLoginViewController ()

@end

@implementation mgLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    _loginButton.backgroundColor = [mgTextHelper mainColor];
    //REGISTER BUTTON BORDER
    _registerButton.layer.cornerRadius = 5.0f;
    _registerButton.layer.borderWidth = 1.0;
    _registerButton.layer.borderColor = [mgTextHelper mainColor].CGColor;
    _registerButton.layer.masksToBounds = YES;
    _registerButton.tintColor = [mgTextHelper mainColor];
    //LOGIN TEXT FIELD BOTTOM BORDER
    CALayer *loginBottomBorder = [CALayer layer];
    loginBottomBorder.frame = CGRectMake(0, _loginTextField.frame.size.height-1, _loginTextField.frame.size.width, 1.0f);
    loginBottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    [_loginTextField.layer addSublayer:loginBottomBorder];
    //PASSWORD TEXT FIELD BOTTOM BORDER
    CALayer *passwordBottomBorder = [CALayer layer];
    passwordBottomBorder.frame = CGRectMake(0, _passwordTextField.frame.size.height-1, _passwordTextField.frame.size.width, 1.0f);
    passwordBottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    [_passwordTextField.layer addSublayer:passwordBottomBorder];
    //LOGIN TEXT FIELD AND PASSWORD TEXT FIELD TEXT COLOR
    _loginTextField.textColor = [mgTextHelper mainColor];
    _passwordTextField.textColor = [mgTextHelper mainColor];
    
    _navController = (mgNavigationController*)self.navigationController;
}
- (void)viewWillAppear:(BOOL)animated {
    UIInterfaceOrientation oldOrientation = _orientation;
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(oldOrientation!=_orientation){
        [_navController popViewControllerAnimated:NO];
        if(_basketVC){
            [_navController goToLoginWithBasket:_basketVC animated:NO];
        }
        else {
            [_navController goToPage:@"loginVC" animated:NO];
        }
        [_navController goToBasketAnimated:NO];
    }
    //NAVIGATION BAR
    mgNavigationBarHandler* navigationBarHandler = [mgNavigationBarHandler new];
    navigationBarHandler.delegate = self;
    [navigationBarHandler configureNavigationBar:self.navigationController];
    _homeButton = [navigationBarHandler homeButton];
    [_homeButton addTarget:self action:@selector(goToMainView) forControlEvents:UIControlEventTouchUpInside];
    
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [_homeButton removeFromSuperview];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewWillDisappear:YES];
    [self viewWillAppear:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Button Handlers
-(IBAction)zaloguj:(id)sender{
    MBProgressHUD *completed = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    NSString* email = _loginTextField.text;
    NSString* password = _passwordTextField.text;
    NSString* loginFormValidationMessage = [mgTextHelper isLoginFormValid:email password:password];
    if(loginFormValidationMessage.length==0){
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Trwa logowanie ...";
        hud.removeFromSuperViewOnHide = YES;
        [hud show:YES];
        [mgPOSTHandler postCheckUserLogin:email withPassword:password completionHandler:^(NSString *response) {
            [hud hide:YES];
            if([response isEqualToString:@"1"]){
                completed.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
                completed.mode = MBProgressHUDModeCustomView;
                completed.labelText = @"Pomyślnie zalogowano";
                //LOGIN COMPLETED
                if(_basketVC){
                    [_navController popToViewController:_basketVC animated:YES];
                    [_basketVC realizujZamowienie];
                }else {
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshMenu" object:nil];
                }
            }
            else {
                completed.mode = MBProgressHUDModeText;
                completed.labelText = @"Logowanie się nie powiodło";
                
            }
            completed.removeFromSuperViewOnHide = YES;
            [completed hide:YES afterDelay:2];
        }];
    }
    else {
        completed.mode = MBProgressHUDModeText;
        completed.labelText = loginFormValidationMessage;
        completed.removeFromSuperViewOnHide = YES;
        [completed hide:YES afterDelay:2];
    }
    
}
#pragma mark - Text Field Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    float offset = 10;
    [UIView animateWithDuration:0.5 animations:^{
        CGRect newLoginButtonFrame = _loginButton.frame;
        newLoginButtonFrame.origin.y = newLoginButtonFrame.origin.y-offset;
        _loginButton.frame = newLoginButtonFrame;
        
        CGRect newRegisterButtonFrame = _registerButton.frame;
        newRegisterButtonFrame.origin.y = newRegisterButtonFrame.origin.y-offset;
        _registerButton.frame = newRegisterButtonFrame;
        
        CGRect newForgotButtonFrame = _forgotButton.frame;
        newForgotButtonFrame.origin.y = newForgotButtonFrame.origin.y-offset;
        _forgotButton.frame = newForgotButtonFrame;
        
    }];
}
-(void)textFieldDidEndEditing:(UITextField *)textField {
    float offset = 10;
    [UIView animateWithDuration:0.5 animations:^{
        CGRect newLoginButtonFrame = _loginButton.frame;
        newLoginButtonFrame.origin.y = newLoginButtonFrame.origin.y+offset;
        _loginButton.frame = newLoginButtonFrame;
        
        CGRect newRegisterButtonFrame = _registerButton.frame;
        newRegisterButtonFrame.origin.y = newRegisterButtonFrame.origin.y+offset;
        _registerButton.frame = newRegisterButtonFrame;
        
        CGRect newForgotButtonFrame = _forgotButton.frame;
        newForgotButtonFrame.origin.y = newForgotButtonFrame.origin.y+offset;
        _forgotButton.frame = newForgotButtonFrame;
        
    }];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Modal Views
- (void)goToMainView{
    [_navController goToMainView];
}
- (void)goToBasket{
    [_navController goToBasketAnimated:YES];
}
- (void)goToFavorites{
    [_navController goToFavoritesAnimated:YES];
}
- (void)openMenu{
    [_navController openMenu];
}

@end
