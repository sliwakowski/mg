//
//  mgLoginViewController.m
//  aplikacja
//
//  Created by Macbook Pro on 05.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgDemoLoginViewController.h"
#import "mgPOSTHandler.h"
@interface mgDemoLoginViewController ()

@end

@implementation mgDemoLoginViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    _loginButton.backgroundColor = [mgTextHelper mainColor];
    //REGISTER BUTTON BORDER
    //LOGIN TEXT FIELD BOTTOM BORDER
    CALayer *loginBottomBorder = [CALayer layer];
    loginBottomBorder.frame = CGRectMake(0, _loginTextField.frame.size.height-1, _loginTextField.frame.size.width, 1.0f);
    loginBottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    [_loginTextField.layer addSublayer:loginBottomBorder];
    
    //PASSWORD TEXT FIELD BOTTOM BORDER
    CALayer *passwordBottomBorder = [CALayer layer];
    passwordBottomBorder.frame = CGRectMake(0, _passwordTextField.frame.size.height-1, _passwordTextField.frame.size.width, 1.0f);
    passwordBottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    [_passwordTextField.layer addSublayer:passwordBottomBorder];
    //LOGIN TEXT FIELD AND PASSWORD TEXT FIELD TEXT COLOR
    _loginTextField.textColor = [mgTextHelper mainColor];
    _passwordTextField.textColor = [mgTextHelper mainColor];
    _loginTextField.text = [[UserDefaults objectForKey:@"login"] objectForKey:@"appKey"];
    
    _navController = (mgNavigationController*)self.navigationController;
    [self setupView];
}
- (void)setupView {
    self.logoImageView.center = CGPointMake(self.view.center.x, self.logoImageView.center.y);
    self.inputView.center = CGPointMake(self.view.center.x, self.inputView.center.y);
    self.loginButton.center = CGPointMake(self.view.center.x, self.loginButton.center.y);
}
- (void)viewWillAppear:(BOOL)animated {
    UIInterfaceOrientation oldOrientation = _orientation;
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(oldOrientation!=_orientation){
        [_navController popViewControllerAnimated:NO];
        if(_basketVC){
            [_navController goToLoginWithBasket:_basketVC animated:NO];
        }
        else {
            [_navController goToPage:@"loginVC" animated:NO];
        }
        [_navController goToBasketAnimated:NO];
    }
    //NAVIGATION BAR
    mgNavigationBarHandler* navigationBarHandler = [mgNavigationBarHandler new];
    navigationBarHandler.delegate = self;
    [navigationBarHandler configureNavigationBar:self.navigationController];
    _homeButton = [navigationBarHandler homeButton];
    [_homeButton addTarget:self action:@selector(goToMainView) forControlEvents:UIControlEventTouchUpInside];
    
    
    [[self navigationController] setToolbarHidden:YES animated:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [_homeButton removeFromSuperview];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewWillDisappear:YES];
    [self viewWillAppear:YES];
    [self setupView];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Button Handlers
-(IBAction)zaloguj:(id)sender{
    NSString* appKey = _loginTextField.text;
    NSString* loginFormValidationMessage = @"";
    if(loginFormValidationMessage.length==0){
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Trwa logowanie ...";
        hud.removeFromSuperViewOnHide = YES;
        [hud show:YES];
        [UserDefaults setObject:@{@"appKey":appKey} forKey:@"login"];
        //Authenticate
        [mgJSONHandler checkAuthAppPreview:appKey completionBlock:^(BOOL a) {
            [hud hide:YES afterDelay:2];
            //Hide view
            [self dismissViewControllerAnimated:YES completion:^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"demoLoginCompleted" object:nil userInfo:nil];
            }];
        }];
    }
}
#pragma mark - Text Field Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    float offset = 10;
    [UIView animateWithDuration:0.5 animations:^{
        CGRect newLoginButtonFrame = _loginButton.frame;
        newLoginButtonFrame.origin.y = newLoginButtonFrame.origin.y-offset;
        _loginButton.frame = newLoginButtonFrame;
        
    }];
}
-(void)textFieldDidEndEditing:(UITextField *)textField {
    float offset = 10;
    [UIView animateWithDuration:0.5 animations:^{
        CGRect newLoginButtonFrame = _loginButton.frame;
        newLoginButtonFrame.origin.y = newLoginButtonFrame.origin.y+offset;
        _loginButton.frame = newLoginButtonFrame;
        
    }];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Modal Views
- (void)goToMainView{
    [_navController goToMainView];
}
- (void)openMenu{
    [_navController openMenu];
}
- (void)goToFavorites{}
- (void)goToBasket{}

@end
