//
//  mgDemoLoginViewController.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 05.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "mgNavigationBarHandler.h"
#import "mgTextHelper.h"
#import "mgNavigationController.h"
#import "MBProgressHUD.h"
#import "mgBasketViewController.h"
@interface mgDemoLoginViewController : UIViewController <mgNavigationBarHandlerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField*                  loginTextField;
@property (nonatomic, strong) IBOutlet UITextField*                  passwordTextField;

@property (nonatomic, strong) IBOutlet UIButton*                     loginButton;
@property (nonatomic, strong) IBOutlet UIImageView*                  logoImageView;
@property (nonatomic, strong) IBOutlet UIView*                       inputView;
@property (nonatomic, assign) UIInterfaceOrientation                 orientation;

@property (nonatomic, strong) mgBasketViewController*       basketVC;
@property (nonatomic, strong) mgNavigationController*       navController;
@property (nonatomic, strong) UIButton*                     homeButton;


@end
