//
//  mgLoginViewController.h
//  aplikacja
//
//  Created by Macbook Pro on 05.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgNavigationBarHandler.h"
#import "mgTextHelper.h"
#import "mgNavigationController.h"
#import "MBProgressHUD.h"
#import "mgBasketViewController.h"
@interface mgLoginViewController : UIViewController <mgNavigationBarHandlerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField*                  loginTextField;
@property (nonatomic, strong) IBOutlet UITextField*                  passwordTextField;

@property (nonatomic, strong) IBOutlet UIButton*                     loginButton;
@property (nonatomic, strong) IBOutlet UIButton*                     registerButton;
@property (nonatomic, strong) IBOutlet UIButton*                     forgotButton;
@property (nonatomic, assign) UIInterfaceOrientation                 orientation;

@property (nonatomic, strong) mgBasketViewController*       basketVC;
@property (nonatomic, strong) mgNavigationController*       navController;
@property (nonatomic, strong) UIButton*                     homeButton;


@end
