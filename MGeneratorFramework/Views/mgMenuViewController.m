    //
//  mgMenuViewController.m
//  mGenerator
//
//  Created by Macbook Pro on 11.03.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgMenuViewController.h"
#import "mgJSONHandler.h"
#import "Kategoria.h"
#import "Strona.h"
#import "mgNavigationBarHandler.h"
#import "mgLoginViewController.h"
#import "mgMenuTableViewCell.h"
#import <MagicalRecord/MagicalRecord.h>
@implementation mgMenuViewController
- (id)init {
    self = [super init];
    
    if (self) {
            }
    return self;
}

#pragma mark - View management

- (void)viewDidLoad {
    [super viewDidLoad];
    [self synchronizeCategories];
    _openedCategoryIndex = 9999;
    _closedCategoryIndex = 9999;
    _actions = [NSMutableArray new];
    _products = [NSMutableArray new];
    [_products addObject:@{@"name": @"Promocje", @"detail": @"isPromotion"}];
    [_products addObject:@{@"name": @"Nowości", @"detail": @"isNew"}];
    [_products addObject:@{@"name": @"Bestsellery", @"detail": @"isBestseller"}];
    [_products addObject:@{@"name": @"Wszystkie produkty", @"detail": @"all"}];
    [self refreshMenu];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMenu)
                                                 name:@"refreshMenu"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(menuStateEventOccurred:)
                                                 name:MFSideMenuStateNotificationEvent
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(synchronizeCategories) name:@"demoLoginCompleted" object:nil];
}
- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"asd");
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (void)refreshMenu{
    
    NSString*userName = [mgTextHelper client:@"name"];
    if(![userName isEqualToString:@""]){
        _actions = [@[@"Skaner QR", @"Mapa", @"Moje konto"] mutableCopy];
    }
    else {
        _actions = [@[@"Skaner QR", @"Mapa", @"Zaloguj"] mutableCopy] ;
    }
    
    [self.tableView reloadData];
}
#pragma mark - Table Header
-(void)addHeaderView{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 65)];
    headerView.backgroundColor = [UIColor colorWithRed:248.0f/255.0f green:248.0f/255.0f blue:248.0f/255.0f alpha:1.0f];
    self.tableView.tableHeaderView = headerView;
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(85, 20, 130, 45);
    UIImage*buttonImage = [UIImage imageNamed:@"logo_m.png"];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [headerView addSubview:button];
    button.userInteractionEnabled = YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section==0) return [_products count];
    else if(section==1) return [_categories count];
    else if(section==2) return [_sites count];
    else if(section==3) return [_actions count];
    else return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    mgMenuTableViewCell *cell = [[mgMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MenuCell"];
    if(indexPath.section==0){
        NSDictionary* actionDictionary = [_products objectAtIndex:indexPath.row];
        cell.title =  actionDictionary[@"name"];
        cell.detailObject = actionDictionary[@"detail"];
        
    }
    else if(indexPath.section==1){
        
        Kategoria* kategoria = [_categories objectAtIndex:indexPath.row];
        
        if([kategoria.parentId intValue]>0) {
            cell.isChild = YES;
        }
        if(indexPath.row == _openedCategoryIndex) {
            cell.isOpened = YES;
        }
        else if(indexPath.row == _closedCategoryIndex) {
            cell.isClosed = YES;
        }
        cell.title = kategoria.name;
        cell.detailObject = kategoria;
    }
    else if(indexPath.section==2){
        Strona* strona = [_sites objectAtIndex:indexPath.row];
        cell.title =  strona.title;
        cell.detailObject = strona;

    }
    else if(indexPath.section==3){
        NSString* actionName = [_actions objectAtIndex:indexPath.row];
        cell.title =  actionName;
        cell.detailObject = actionName;

    }
    if([cell isKindOfClass:[mgMenuTableViewCell class]]){
        [cell setViews];
    }
    
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if([self tableView:self.tableView numberOfRowsInSection:section]>0){
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
        /* Create custom view to display section header... */
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 3, tableView.frame.size.width, 18)];
        [label setFont:[UIFont boldSystemFontOfSize:12]];
        [label setTextColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.7]];
        NSString *string = nil;
        if(section==0){
            string = @"Produkty";
        }
        else if(section==1){
            string = @"Kategorie";
        }
        else if(section==2){
            string = @"Strony";
        }
        else if(section==3){
            string = @"Sklep";
        }
        [label setText:string];
        [view addSubview:label];
        [view setBackgroundColor:[UIColor colorWithRed:40.0f/255.0f green:37.0f/255.0f blue:35.0f/255.0f alpha:1]]; //your background color...
        return view;
    }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if([self tableView:self.tableView numberOfRowsInSection:section]>0){
        return 24.0f;
    }
    else return 0;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section==0){
        [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
            [_navigationController popToRootViewControllerAnimated:YES];
            NSDictionary* actionDictionary = [_products objectAtIndex:indexPath.row];
            [_navigationController loadProductsFilteredBy:actionDictionary[@"detail"]];
        }];
    }
    else if(indexPath.section==1){
        Kategoria*selectedCategory = [_categories objectAtIndex:indexPath.row];
        NSMutableArray* newCategories = [NSMutableArray arrayWithArray:_categories];
        NSMutableArray* childCategories = [[Kategoria MR_findByAttribute:@"parentId" withValue:selectedCategory.identyfikator] mutableCopy];
        int deletedCategories = 0;
        int insertedCategories = 0;
        BOOL selectedCategoryOpened = NO;
        
        //Kliknieto podkategorie.
        if([selectedCategory.parentId intValue]!=0){
            //Kliknieto wszystkie produkty
            if([selectedCategory.identyfikator intValue]==[selectedCategory.parentId intValue] ){
                //selectedCategory = [_categories objectAtIndex:_openedCategoryIndex];
                childCategories = [[Kategoria MR_findByAttribute:@"parentId" withValue:selectedCategory.identyfikator] mutableCopy];
                [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
                    [_navigationController loadAllSubcategories:childCategories];
                }];
                return;
            }
            [self goToCategory:selectedCategory withChildCategories:childCategories];
        }
        else {
            //Kliknieto glowna kategorie, brak podkategorii -> przechodzi do produktów
            if([childCategories count]==0) {
                [self goToCategory:selectedCategory withChildCategories:nil];
                return;
            }
            //Kliknieto glowna kategorie, pokazuje podkategorie
            for(Kategoria*category in _categories){
                
                if([category.parentId intValue]==[selectedCategory.identyfikator intValue]){
                    selectedCategoryOpened = YES;
                }
                if([category.parentId intValue]!=0){
                    
                    [newCategories removeObject:category];
                    deletedCategories++;
                }
            }
            NSMutableArray *insertIndexPaths = [NSMutableArray new];
            if(!selectedCategoryOpened){
                int oldCategoryIndex = _openedCategoryIndex;
                _openedCategoryIndex = (int)indexPath.row;
                _closedCategoryIndex = 9999;
                NSMutableArray*podkategorie = [[Kategoria MR_findByAttribute:@"parentId" withValue:selectedCategory.identyfikator] mutableCopy];
                
                Kategoria* wszystkieProdukty = [[Kategoria MR_findByAttribute:@"name" withValue:@"Wszystkie produkty"] objectAtIndex:0];
                wszystkieProdukty.identyfikator = selectedCategory.identyfikator;
                wszystkieProdukty.parentId = selectedCategory.identyfikator;
                [podkategorie insertObject:wszystkieProdukty atIndex:0];
                for(Kategoria*podkategoria in podkategorie){
                    int indexToInsert = (int)indexPath.row+1;
                    if(indexToInsert>[newCategories count] || _openedCategoryIndex>oldCategoryIndex) {
                        indexToInsert = (int)indexPath.row+1-deletedCategories;
                    }
                    [newCategories insertObject:podkategoria atIndex:indexToInsert];
                    //[newCategories addObject:podkategoria];
                    [insertIndexPaths addObject:[NSIndexPath indexPathForRow:insertedCategories inSection:0]];
                    insertedCategories++;
                }
                [podkategorie addObject:selectedCategory];
                //[_navigationController loadAllSubcategories:podkategorie];
            }
            else {
                _closedCategoryIndex = (int)indexPath.row;
                _openedCategoryIndex = 9999;
            }
            _categories = newCategories;
//            if(!selectedCategoryOpened && 1==2){
//                [self.tableView beginUpdates];
//                [self.tableView insertRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationFade];
//                [self.tableView endUpdates];
//            }
//            else {
                [self.tableView reloadData];
//            }
            
        }
    }
    else if(indexPath.section==2){
        Strona* strona = [_sites objectAtIndex:(indexPath.row)];
        [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
            [_navigationController openSite:strona];
        }];
    }
    else if(indexPath.section==3){
        NSString* actionName = [_actions objectAtIndex:(indexPath.row)];
        if([actionName isEqualToString:@"Zaloguj"]){
            [_navigationController goToPage:@"loginVC" animated:YES];
        }
        else if([actionName rangeOfString:@"Moje konto"].location==0){
            //[self logout];
            [_navigationController goToPage:@"accountVC" animated:YES];
        }
        else if([actionName isEqualToString:@"Mapa"]){
            [_navigationController goToPage:@"mapVC" animated:YES];
        }
        else if([actionName isEqualToString:@"Skaner QR"]){
            [_navigationController goToPage:@"qrVC" animated:YES];
        }
    }
}
- (void)expandItemAtIndex:(int)index {
}
- (void)goToCategory:(Kategoria*)selectedCategory withChildCategories:(NSArray*)childCategories{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        if([childCategories count]==0){
            NSMutableDictionary*userInfo = [NSMutableDictionary new];
            [userInfo setObject:selectedCategory forKey:@"kategoria"];
            [userInfo setObject:@"kategoria" forKey:@"type"];
            [_navigationController popToRootViewControllerAnimated:NO];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterOffers" object:userInfo];
        }
        else {
            [_navigationController goToCategory:selectedCategory];
        }
    }];
}
- (void)logout{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"user"];
    [defaults synchronize];
    [_navigationController.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [self refreshMenu];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"Pomyślnie wylogowano";
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:1];
    }];

}
- (void)synchronizeCategories{
    NSArray*kategorie = [NSArray new];
    _categories = [NSMutableArray new];
    kategorie = [Kategoria MR_findByAttribute:@"disable" withValue:@NO];
    for(Kategoria*kategoria in kategorie){
        if([kategoria.parentId intValue]==0){
            [_categories addObject:kategoria];
        }
    }
    [self.tableView reloadData];
    [mgJSONHandler synchronizeCategories:^(BOOL finished) {
        if(finished){
            NSArray*kategorie = [NSArray new];
            kategorie = [Kategoria MR_findByAttribute:@"disable" withValue:@NO];
            _categories = [NSMutableArray new];
            for(Kategoria*kategoria in kategorie){
                if([kategoria.parentId intValue]==0){
                    [_categories addObject:kategoria];
                }
            }
            [self.tableView reloadData];
            [self synchronizeSites];
        }
        else {
            [self synchronizeSites];
        }
    }];
}
- (void)synchronizeSites{
    _sites = [[Strona MR_findAll] mutableCopy];
    [self.tableView reloadData];
    [mgJSONHandler synchronizeSites:^(BOOL finished) {
        _sites = [[Strona MR_findAll] mutableCopy];
        [self.tableView reloadData];
    }];
}
- (void)menuStateEventOccurred:(NSNotification *)notification {
    MFSideMenuStateEvent event = [[[notification userInfo] objectForKey:@"eventType"] intValue];
    if(event == MFSideMenuStateEventMenuWillOpen) {
        [self refreshMenu];
    }
    // ...
}

@end
