//
//  mgNewReviewViewController.h
//  aplikacja
//
//  Created by Macbook Pro on 05.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgNavigationController.h"
#import "mgFullStarView.h"
#import "mgPOSTHandler.h"
#import "Produkt.h"

@interface mgNewReviewViewController : UIViewController <mgNavigationBarHandlerDelegate, UITextViewDelegate>

@property (nonatomic, strong) Produkt*                      produkt;
@property (nonatomic, strong) IBOutlet UIView*              ratingView;
@property (nonatomic, strong) IBOutlet UITextField*         nickTextField;
@property (nonatomic, strong) IBOutlet UITextView*          commentTextView;
@property (nonatomic, assign) int                           currentRating;
@property (nonatomic, strong) mgFullStarView*               ratingBar;
@property (nonatomic, strong) NSString*                     commentPlaceholder;

@property (nonatomic, strong) mgNavigationController*       navController;
@property (nonatomic, strong) UILabel*                      titleLabel;
@property (nonatomic, strong) UIButton*                     sendButton;

@end
