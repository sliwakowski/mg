//
//  mgDetailViewController.m
//  mGenerator
//
//  Created by Macbook Pro on 17.03.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgDetailViewController.h"
#import "mgSimilarProductView.h"
#import <MagicalRecord/MagicalRecord.h>
@interface mgDetailViewController () {
    int imagesScrollPages;
    float topAttrOffset;
    float descriptionOffset;
    float rightScrollHeight;
    NSArray *pickerData;
    NSString *placeholderImage;
    NSMutableArray* attributesTitles;
    UITextField *attributeTextField;
    NSMutableDictionary *attributesDictionary;
}
@end
@implementation mgDetailViewController
#pragma mark - Init methods
- (void)setDetailItem:(Produkt*)newDetailItem {
    if (_produkt != newDetailItem) {
        _produkt = newDetailItem;
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //PORTRAIT / LANDSCAPE HANDLING
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    _navController = (mgNavigationController*) self.navigationController;
    //CODE FOR SMALL IPHONE
    if(!IsIphone5){
        _mainScrollView.frame = CGRectMake(_mainScrollView.frame.origin.x, _mainScrollView.frame.origin.y+88, _mainScrollView.frame.size.width, _mainScrollView.frame.size.height);
    }
    //HIDE SECOND VIEW
    _recenzjaScrollView.alpha = 0;
    //TYTUL
    [_titleTextView setText:_produkt.name];
    //[_titleTextView setTextAlignment:NSTextAlignmentCenter];
    [_titleTextView setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:20]];
    [_titleTextView setTextColor:UIColorFromRGB(0x939393)];
    [_titleTextView setScrollEnabled:NO];
    [_titleTextView sizeToFit];
    [_titleTextView layoutIfNeeded];
    CGRect newTitleTextViewFrame = _titleTextView.frame;
    newTitleTextViewFrame.size.width = 300;
    _titleTextView.frame = newTitleTextViewFrame;
    //NOWA CENA
    [mgTextHelper configureNewPriceLabel:_nowaCenaLabel withProdukt:_produkt];
    CGRect nowaCenaFrame = _nowaCenaLabel.frame;
    nowaCenaFrame.origin.x = 3;
    _nowaCenaLabel.frame = nowaCenaFrame;
    //STARA CENA
    [mgTextHelper configureOldPriceLabel:_staraCenaLabel withNewPriceLabel:_nowaCenaLabel withProdukt:_produkt];
    CGRect staraCenaFrame = _staraCenaLabel.frame;
    staraCenaFrame.origin.x = _nowaCenaLabel.frame.size.width+_nowaCenaLabel.frame.origin.x + 10;
    _staraCenaLabel.frame = staraCenaFrame;
    
    //ZDJECIA
    if(_produkt.images.length>0)
    {
        NSString*zdjeciaString = [_produkt.images substringFromIndex:1];
        _pageImages = (NSMutableArray*)[zdjeciaString componentsSeparatedByString:@";"];
    }
    else _pageImages = nil;
    //RELAYOUT FOR IPHONE
    if(IDIOM!=IPAD)[self relayoutForIphone];
    //BUTTONS
    _dodajDoKoszykaButton.layer.cornerRadius = 5.0f;
    _dodajDoKoszykaButton.layer.masksToBounds = YES;
    _dodajDoKoszykaButton.backgroundColor = [mgTextHelper mainColor];
    _dodajDoKoszykaButton.tintColor = [UIColor whiteColor];
    _dodajDoUlubionychButton.layer.cornerRadius = 5.0f;
    _dodajDoUlubionychButton.layer.borderWidth = 1.0;
    _dodajDoUlubionychButton.layer.borderColor = [mgTextHelper mainColor].CGColor;
    _dodajDoUlubionychButton.layer.masksToBounds = YES;
    //ILOSC CONTAINER
    CALayer *iloscContainerTopBorder = [CALayer layer];
    iloscContainerTopBorder.frame = CGRectMake(10.0f, 0.0f, 310.0f, 1.0f);
    iloscContainerTopBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    [_iloscContainer.layer addSublayer:iloscContainerTopBorder];
    //ILOSC
    _iloscLabel.layer.cornerRadius = 5.0f;
    _iloscLabel.layer.borderWidth = 1.0;
    _iloscLabel.layer.borderColor = [mgTextHelper mainColor].CGColor;
    _iloscLabel.layer.masksToBounds = YES;
    _decremenetIlosc.backgroundColor = [mgTextHelper mainColor];
    _incrementIlosc.backgroundColor = [mgTextHelper mainColor];
    //ATRYBUTY
    if(IDIOM==IPAD) topAttrOffset = 90;
    else topAttrOffset = _iloscContainer.frame.origin.y+_iloscContainer.frame.size.height+10;
    descriptionOffset = 0;
    [self configureAttributes];
    if([_produkt.text length]>1)[self configureDescriptionWithTitle:@"Opis:" andText:_produkt.text];
    if([_produkt.details length]>1)[self configureDescriptionWithTitle:@"Specyfikacja:" andText:_produkt.details];
    //SETTERS
    [self setVariables];
    //NAVIGATION BAR
    mgNavigationBarHandler* navigationBarHandler = [mgNavigationBarHandler new];
    [navigationBarHandler configureNavigationBar:self.navigationController];
    //SIMILAR OFFERS
    [self layoutSimilarOffers];
    //FIND IF IN ULUBIONE
    if(!_koszyk){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identyfikator==%d and ulubione=YES", [_produkt.identyfikator intValue]];
        if([[Koszyk MR_findAllWithPredicate:predicate] count]>0){
            _ulubiony = [[Koszyk MR_findAllWithPredicate:predicate] objectAtIndex:0];
        }
    }
}

- (void)viewDidLayoutSubviews {
    [self layoutAttributes];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //PORTRAIT / LANDSCAPE HANDLING
    UIInterfaceOrientation oldOrientation = _orientation;
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(oldOrientation!=_orientation){
        [_navController popViewControllerAnimated:NO];
        [_navController goToProduct:_produkt animated:NO];
    }
    //COMMENTS
    [self synchronizeComments];
    [_recenzjaScrollView setContentOffset:CGPointZero animated:YES];
    //NAVIGATION BAR
    _navigationBarHandler = [mgNavigationBarHandler new];
    _navigationBarHandler.delegate = self;
    [_navigationBarHandler configureNavigationBar:self.navigationController];
    _favoritesButton = [_navigationBarHandler favoritesButton];
    [_favoritesButton addTarget:self action:@selector(goToFavorites) forControlEvents:UIControlEventTouchUpInside];
    _basketButton = [_navigationBarHandler basketButton];
    [_basketButton addTarget:self action:@selector(goToBasket) forControlEvents:UIControlEventTouchUpInside];
    _homeButton = [_navigationBarHandler homeButton];
    [_homeButton addTarget:self action:@selector(goToMainView) forControlEvents:UIControlEventTouchUpInside];
    NSInteger pageCount = _pageImages.count;
    [self loadToMainImageView:0];
    // Set up the page control
    _pageControl.currentPage = 0;
    _pageControl.numberOfPages = pageCount;
    _pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [_pageViews addObject:[NSNull null]];
    }
    // Set up the content size of the scroll view
    CGSize pagesScrollViewSize = _imagesScrollView.frame.size;
    _imagesScrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * pageCount, pagesScrollViewSize.height);
    if(IDIOM==IPAD){
        _imagesScrollView.contentSize = CGSizeMake(pagesScrollViewSize.width/3 * pageCount, pagesScrollViewSize.height);
    }
    // Load the initial set of pages that are on screen
    [self loadVisiblePages];
    
    //HIDE TOOLBAR
    [[self navigationController] setToolbarHidden:YES animated:YES];
    
    //BASKET
    [self setDetailForBasket];
}
- (void)viewWillDisappear:(BOOL)animated {
    [_favoritesButton removeFromSuperview];
    [_homeButton removeFromSuperview];
    [_basketButton removeFromSuperview];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewWillDisappear:YES];
    [self viewWillAppear:YES];
}
- (void)setDetailForBasket{
    if(_koszyk){
        _iloscLabel.text = [NSString stringWithFormat:@"%@", _koszyk.ilosc];
        [_dodajDoKoszykaButton setTitle:@"Aktualizuj" forState:UIControlStateNormal];
        NSError*e;
        NSDictionary* attributes = [NSJSONSerialization JSONObjectWithData:[_koszyk.attributes dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
        NSArray*attributesKeys = [attributes allKeys];
        for(NSString*singleAttribute in attributesKeys){
            NSDictionary*singleAttributeDictionary = [attributesDictionary objectForKey:singleAttribute];
            NSArray*singleAttributeValues = [singleAttributeDictionary objectForKey:@"values"];
            UIPickerView* picker = [singleAttributeDictionary objectForKey:@"picker"];
            UITextField* textField = [singleAttributeDictionary objectForKey:@"textfield"];
            NSString* attributeValue = [attributes objectForKey:singleAttribute];
            int index = (int)[singleAttributeValues indexOfObject:attributeValue];
            textField.text = attributeValue;
            pickerData = singleAttributeValues;
            [picker reloadAllComponents];
            [picker selectRow:index inComponent:0 animated:YES];
        }
    }
    if(_ulubiony){
        [_dodajDoUlubionychButton setTitle:@"Usuń z ulubionych" forState:UIControlStateNormal];
    }
}
- (void)setVariables{
    if(IDIOM == IPAD) {
        placeholderImage = @"ipadPortraitBlank.png";
    }
    else {
        placeholderImage = @"iphoneGallery.png";
    }
    rightScrollHeight = 755;
    imagesScrollPages = 1;
    if(IDIOM==IPAD) imagesScrollPages = 3;
}
- (void)synchronizeComments{
    NSArray*komentarze = [Komentarz MR_findAll];
    _comments = [NSMutableArray new];
    for(Komentarz*komentarz in komentarze){
        if([komentarz.produktId intValue]==[_produkt.identyfikator intValue]){
            [_comments addObject:komentarz];
        }
    }
    [self layoutComments];
    [mgJSONHandler synchronizeCommentsForProductId:[_produkt.identyfikator intValue] completionBlock:^(BOOL finished) {
        if(finished){
            NSArray*komentarze = [Komentarz MR_findAllSortedBy:@"komentarzId" ascending:NO];
            _comments = [NSMutableArray new];
            for(Komentarz*komentarz in komentarze){
                if([komentarz.produktId intValue]==[_produkt.identyfikator intValue]){
                    [_comments addObject:komentarz];
                }
            }
            [self layoutComments];
        }
    }];
}
- (void)refreshBasketButton{
    [_basketButton removeFromSuperview];
    _basketButton = [_navigationBarHandler basketButton];
    [_basketButton addTarget:self action:@selector(goToBasket) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - COMMENTS
- (void)layoutComments {
    int kSingleCommentCellTag = 110;
    float commentOffset = _averageRatingTitle.frame.origin.y + _averageRatingTitle.frame.size.height + 30;
    float averageRate = 0;
    //REMOVE CELLS
    UIView *removeView;
    while((removeView = [_recenzjaScrollView viewWithTag:kSingleCommentCellTag]) != nil) {
        [removeView removeFromSuperview];
    }
    //ADD CELLS
    for(int commentNumber = 0; commentNumber<[_comments count]; commentNumber++){
        Komentarz* singleComment = [_comments objectAtIndex:commentNumber];
        mgReviewCellView* singleCommentCell = [[mgReviewCellView alloc] initWithComment:singleComment atPosition:commentOffset];
        singleCommentCell.tag = kSingleCommentCellTag;
        [_recenzjaScrollView addSubview:singleCommentCell];
        if((commentNumber+1)<[_comments count] || IDIOM!=IPAD){
            commentOffset+=singleCommentCell.cellHeight+15;
        }
        
        averageRate+=[singleComment.rate floatValue];
    }
    averageRate/=[_comments count];
    
    // AVERAGE RATE
    [_activeStars removeFromSuperview];
    [_mainActiveStars removeFromSuperview];
    _activeStars = [[mgFullStarView alloc] initWithSize:30 activeStars:averageRate maxStars:5 activeStarColor:[mgTextHelper mainColor] inactiveStarColor:UIColorFromRGB(0x666666)];
    _mainActiveStars = [[mgFullStarView alloc] initWithSize:20 activeStars:averageRate maxStars:5 activeStarColor:[mgTextHelper mainColor] inactiveStarColor:UIColorFromRGB(0x666666)];
    if([_comments count]==0){
        _averageRatingTitle.text = @"Brak komentarzy";
    }
    else {
        _averageRatingTitle.text = @"Średnia ocena";
    }
    [_averageRatingView addSubview:_activeStars];
    [_mainAverageRatingView addSubview:_mainActiveStars];
    
    //DODAJ KOMENTARZ BUTTON
    CGRect newKomentarzButtonFrame = _dodajKomentarzButton.frame;
    newKomentarzButtonFrame.origin.y = commentOffset;
    //newKomentarzButtonFrame.origin.y = 0;
    
    _dodajKomentarzButton.frame = newKomentarzButtonFrame;
    _dodajKomentarzButton.backgroundColor = [mgTextHelper mainColor];
    //SCROLL VIEW FIX
    [_recenzjaScrollView setContentSize:CGSizeMake(320, _dodajKomentarzButton.frame.origin.y + _dodajKomentarzButton.frame.size.height + 100)];
    CGRect recenzjaScrollViewFrame = _recenzjaScrollView.frame;
    recenzjaScrollViewFrame.origin.x = self.view.center.x - (recenzjaScrollViewFrame.size.width/2);
    if(_orientation==UIInterfaceOrientationLandscapeRight || _orientation==UIInterfaceOrientationLandscapeLeft){
        [_recenzjaScrollView setCenter:CGPointMake(1024/2, _recenzjaScrollView.center.y)];
    }
    _recenzjaScrollView.frame = recenzjaScrollViewFrame;
    
}
- (void)relayoutForIphone {
    _imagesScrollView.frame = CGRectMake(_imagesScrollView.frame.origin.x, _titleTextView.frame.origin.y+_titleTextView.frame.size.height+10, _imagesScrollView.frame.size.width, _imagesScrollView.frame.size.height);
    _pageControl.frame = CGRectMake(_pageControl.frame.origin.x, _imagesScrollView.frame.origin.y+_imagesScrollView.frame.size.height-10, _pageControl.frame.size.width, _pageControl.frame.size.height);
    _nowaCenaLabel.frame = CGRectMake(_nowaCenaLabel.frame.origin.x, _imagesScrollView.frame.origin.y+_imagesScrollView.frame.size.height+30, _nowaCenaLabel.frame.size.width, _nowaCenaLabel.frame.size.height);
    [_nowaCenaLabel setCenter:CGPointMake(self.view.center.x, _nowaCenaLabel.center.y)];
    _staraCenaLabel.frame = CGRectMake(_nowaCenaLabel.frame.origin.x+_nowaCenaLabel.frame.size.width+10, _imagesScrollView.frame.origin.y+_imagesScrollView.frame.size.height+32, _staraCenaLabel.frame.size.width, _staraCenaLabel.frame.size.height);
    _dodajDoKoszykaButton.frame = CGRectMake(_dodajDoKoszykaButton.frame.origin.x, _nowaCenaLabel.frame.size.height+_nowaCenaLabel.frame.origin.y+20, _dodajDoKoszykaButton.frame.size.width, _dodajDoKoszykaButton.frame.size.height);
    _dodajDoUlubionychButton.frame = CGRectMake(_dodajDoUlubionychButton.frame.origin.x, _dodajDoKoszykaButton.frame.size.height+_dodajDoKoszykaButton.frame.origin.y+10, _dodajDoUlubionychButton.frame.size.width, _dodajDoUlubionychButton.frame.size.height);
    _iloscContainer.frame = CGRectMake(_iloscContainer.frame.origin.x, _dodajDoUlubionychButton.frame.size.height+_dodajDoUlubionychButton.frame.origin.y+10, _iloscContainer.frame.size.width, _iloscContainer.frame.size.height);
}
#pragma mark - ATTRIBUTES
- (void)layoutAttributes {
    CGRect staticFrame = _staticView.frame;
    staticFrame.origin.y =_titleTextView.frame.origin.y + _titleTextView.frame.size.height + 10;
    _staticView.frame = staticFrame;
    CGRect atrybutyFrame = _atrybutyView.frame;
    atrybutyFrame.origin.y = staticFrame.origin.y+staticFrame.size.height;
    //atrybutyFrame.size.height = rightScrollHeight-staticFrame.origin.y-staticFrame.size.height;
    atrybutyFrame.size.height = topAttrOffset;
    //[atrybutyView setContentSize:CGSizeMake(320, topAttrOffset)];
    atrybutyFrame.size.height = topAttrOffset;
    //[atrybutyView setFrame:atrybutyFrame];
    if(IDIOM!=IPAD){
        _atrybutyView.frame = atrybutyFrame;
        [_atrybutyView layoutIfNeeded];
        [_atrybutyView setNeedsDisplay];
        float ax = atrybutyFrame.origin.y + topAttrOffset;
        [_mainScrollView setContentSize:CGSizeMake(320, ax)];
    } else {
        _atrybutyView.contentSize = CGSizeMake(atrybutyFrame.size.width, topAttrOffset);
        [_atrybutyView layoutIfNeeded];
        [_atrybutyView setNeedsDisplay];
    }
}
- (void)configureDescriptionWithTitle:(NSString*)title andText:(NSString*)details{
    //ATTRIBUTE CONTAINER
    UIView*attributeContainer = [[UIView alloc] initWithFrame:CGRectMake(0, topAttrOffset, 320, 60)];
    if(IDIOM==IPAD) {
        if(_orientation == 0 || _orientation == UIInterfaceOrientationPortrait || _orientation==UIInterfaceOrientationPortraitUpsideDown) {
            [_atrybutyView addSubview:attributeContainer];
        }
        else {
            [_descriptionContainerView addSubview:attributeContainer];
            attributeContainer.frame = CGRectMake(attributeContainer.frame.origin.x, descriptionOffset, attributeContainer.frame.size.width, attributeContainer.frame.size.height);
        }
    }
    else [_mainScrollView addSubview:attributeContainer];
    //TOP LINE
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(10.0f, 0.0f, 300.0f, 1.0f);
    topBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    if(descriptionOffset!=0)[attributeContainer.layer addSublayer:topBorder];
    //OPIS
    UILabel*tytulAtrybutu = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 290, 18)];
    if(IDIOM==IPAD ) tytulAtrybutu = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 300, 18)];
    tytulAtrybutu.text = [title capitalizedString];
    tytulAtrybutu.textColor = UIColorFromRGB(0x9e9e9e);
    [attributeContainer addSubview:tytulAtrybutu];
    //TEXT FIELD
    UITextView*trescOpisu = [[UITextView alloc] initWithFrame:CGRectMake(8, 38, 296, 32)];
    if(IDIOM==IPAD ) trescOpisu = [[UITextView alloc] initWithFrame:CGRectMake(8, 38, 306, 32)];
    [trescOpisu setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16.0f]];
    trescOpisu.textColor = UIColorFromRGB(0x9e9e9e);
    NSString*opis = details;
    while ([opis rangeOfString:@"  "].location!=NSNotFound) {
        opis = [opis stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    trescOpisu.text = opis;
    [trescOpisu sizeToFit];
    [trescOpisu layoutIfNeeded];
    [attributeContainer addSubview:trescOpisu];
    if(_orientation == 0 || _orientation == UIInterfaceOrientationPortrait || _orientation==UIInterfaceOrientationPortraitUpsideDown) {
        topAttrOffset+=tytulAtrybutu.frame.size.height+trescOpisu.frame.size.height+30;
    }
    descriptionOffset+=tytulAtrybutu.frame.size.height+trescOpisu.frame.size.height+30;
    float landscapeScrollHeight = topAttrOffset+_atrybutyView.frame.origin.y > descriptionOffset ? topAttrOffset+_atrybutyView.frame.origin.y : descriptionOffset;
    [_landscapeScrollView setContentSize:CGSizeMake(_landscapeScrollView.frame.size.width, landscapeScrollHeight+80)];
}
- (void)configureAttributes {
    NSArray*attrElements = [_produkt.attributes componentsSeparatedByString:@"&"];
    attributesTitles = [NSMutableArray new];
    attributesDictionary = [NSMutableDictionary new];
    for (NSString*singleAttribute in attrElements) {
        if([singleAttribute length]>0){
            NSArray*singleAttrElements = [singleAttribute componentsSeparatedByString:@"#"];
            NSString* singleAttrTitle =singleAttrElements[0];
            NSArray* singleAttrValues = [singleAttrElements[1] componentsSeparatedByString:@";"];
            [attributesTitles addObject:singleAttrTitle];
            //ATTRIBUTE CONTAINER
            UIView*attributeContainer = [[UIView alloc] initWithFrame:CGRectMake(0, topAttrOffset, 320, 90)];
            if(IDIOM==IPAD)[_atrybutyView addSubview:attributeContainer];
            else [_mainScrollView addSubview:attributeContainer];
            //TOP LINE
            CALayer *topBorder = [CALayer layer];
            topBorder.frame = CGRectMake(10.0f, 0.0f, 310.0f, 1.0f);
            topBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
            [attributeContainer.layer addSublayer:topBorder];
            //OPIS
            UILabel*tytulAtrybutu = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 18)];
            tytulAtrybutu.text = [NSString stringWithFormat:@"%@:", [singleAttrTitle capitalizedString]];
            tytulAtrybutu.textColor = UIColorFromRGB(0x9e9e9e);
            [attributeContainer addSubview:tytulAtrybutu];
            //TEXT FIELD
            UIView*wartoscAtrybutuHolder = [[UIView alloc] initWithFrame:CGRectMake(10, 40, 310, 32)];
            if(IDIOM!=IPAD) wartoscAtrybutuHolder = [[UIView alloc] initWithFrame:CGRectMake(10, 40, 300, 32)];
            [attributeContainer addSubview:wartoscAtrybutuHolder];
            UITextField*wartoscAtrybutu = [[UITextField alloc] initWithFrame:CGRectMake(25, 49, 310, 16)];
            if(IDIOM!=IPAD) wartoscAtrybutu = [[UITextField alloc] initWithFrame:CGRectMake(25, 49, 300, 16)];
            wartoscAtrybutu.textColor = UIColorFromRGB(0x9e9e9e);
            wartoscAtrybutu.delegate = self;
            wartoscAtrybutu.text = singleAttrValues[0];
            wartoscAtrybutuHolder.layer.cornerRadius = 5.0f;
            wartoscAtrybutuHolder.layer.borderWidth = 1.0;
            wartoscAtrybutuHolder.layer.borderColor = UIColorFromRGB(0x9b9b9b).CGColor;
            wartoscAtrybutuHolder.layer.masksToBounds = YES;
            wartoscAtrybutuHolder.userInteractionEnabled = YES;
            UIPickerView *picker = [[UIPickerView alloc] init];
            picker.dataSource = self;
            picker.delegate = self;
            wartoscAtrybutu.inputView = picker;
            NSMutableDictionary*attributeObjects = [NSMutableDictionary new];
            [attributeObjects setObject:wartoscAtrybutu forKey:@"textfield"];
            [attributeObjects setObject:picker forKey:@"picker"];
            [attributeObjects setObject:singleAttrValues forKey:@"values"];
            [attributesDictionary setObject:attributeObjects forKey:singleAttrTitle];
            pickerData = singleAttrValues;
            //[wartoscAtrybutuHolder addSubview:wartoscAtrybutu];
            [attributeContainer addSubview:wartoscAtrybutu];
            [[UIPickerView appearance] setBackgroundColor:[UIColor whiteColor]];
            topAttrOffset+=85.0f;
        }
    }
    
    float landscapeScrollHeight = topAttrOffset+_atrybutyView.frame.origin.y > descriptionOffset ? topAttrOffset+_atrybutyView.frame.origin.y : descriptionOffset;
    [_landscapeScrollView setContentSize:CGSizeMake(_landscapeScrollView.frame.size.width, landscapeScrollHeight+80)];
}
#pragma mark - GALLERY
- (IBAction)scrollImageTap:(id)sender {
    CGPoint point = [sender locationInView:_imagesScrollView];
    int imageNumber = floor(point.x/(_imagesScrollView.frame.size.width/imagesScrollPages));
    if(imageNumber<[_pageImages count] ){
        [self loadToMainImageView:imageNumber];
    }
}
- (void)loadToMainImageView:(int)index {
    [_mainImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@&w=500",[_pageImages objectAtIndex:index]]] placeholderImage:[UIImage imageNamed:@"ipadPortrait.png"]];
    [_mainImageView setContentMode:UIViewContentModeScaleAspectFit];
}
- (void)loadVisiblePages {
    // First, determine which page is currently visible
    CGFloat pageWidth = _imagesScrollView.frame.size.width/3;
    NSInteger page = (NSInteger)floor((_imagesScrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    if(page<1)page=0;
    _pageControl.currentPage = page;
    // Work out which pages you want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 10;
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) { [self purgePage:i]; }
    for (NSInteger i=firstPage; i<=lastPage; i++) { [self loadPage:i];  }
    for (NSInteger i=lastPage+1; i<_pageImages.count; i++) { [self purgePage:i]; }
}
- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= _pageImages.count) return; // If it's outside the range of what we have to display, then do nothing
    UIView *pageView = [_pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {
        CGRect frame = _imagesScrollView.bounds;
        frame.size.width = frame.size.width / imagesScrollPages;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        if(IDIOM == IPAD){  frame.origin.x = frame.origin.x; }
        frame = CGRectInset(frame, 5.0f, 0.0f);
        UIImageView *newPageView = [UIImageView new];
        [newPageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@&w=500",[_pageImages objectAtIndex:page]]] placeholderImage:[UIImage imageNamed:placeholderImage]];
        [newPageView setContentMode:UIViewContentModeScaleAspectFit];
        newPageView.frame = frame;
        [_imagesScrollView addSubview:newPageView];
        [_pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
}
- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= _pageImages.count) return;
    UIView *pageView = [_pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [_pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //NSLog(@"%f", scrollView.contentOffset.y);
    _pageControl.currentPage = (int) scrollView.contentOffset.x/_imagesScrollView.frame.size.width;
}
#pragma mark - SIMILAR OFFERS

- (void)layoutSimilarOffers{
    CALayer *similarOffersTopBorder = [CALayer layer];
    similarOffersTopBorder.frame = CGRectMake(0.0f, 0.0f, _similarOffersView.frame.size.width, 1.0f);
    similarOffersTopBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    [_similarOffersView.layer addSublayer:similarOffersTopBorder];

    NSFetchRequest *request = (NSFetchRequest*) [Produkt MR_requestAllWhere:@"categoryId" isEqualTo:_produkt.categoryId];
    [request setFetchLimit:4];
    if(_orientation==UIInterfaceOrientationLandscapeLeft || _orientation==UIInterfaceOrientationLandscapeRight) [request setFetchLimit:5];
    float similarOfferWidth = 180.0f;
    float similarOfferGap = 10.0f;
    NSArray *similarOffers = [Produkt MR_executeFetchRequest:request];
    for(int i=0; i<[similarOffers count]; i++){
        mgSimilarProductView* similarProductView = [[mgSimilarProductView alloc] initWithFrame:CGRectMake(similarOfferGap*(i+1)+similarOfferWidth*i, 10, similarOfferWidth, _similarOffersView.frame.size.height) andProduct:[similarOffers objectAtIndex:i]];
        [_similarOffersView addSubview:similarProductView];
        UITapGestureRecognizer* similarOfferGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSimilarOffer:)];
        [similarProductView addGestureRecognizer:similarOfferGesture];
    }
}
-(void)openSimilarOffer:(UITapGestureRecognizer*)sender{
    mgSimilarProductView* similarProductView = (mgSimilarProductView*) sender.view;
    Produkt* produkt = similarProductView.produkt;
    [_navController goToProduct:produkt animated:YES];
}
#pragma mark - ATTRIBUTE PICKER

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {return pickerData.count;}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {return  1;}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {return pickerData[row];}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    attributeTextField.text = pickerData[row];
    [pickerView resignFirstResponder];
    [[self view] endEditing:YES];
    [[self view] endEditing:NO];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    attributeTextField = textField;
    for(NSString*attributeTitle in attributesTitles){
        NSDictionary*attributeObject = [attributesDictionary objectForKey:attributeTitle];
        if([textField isEqual:[attributeObject objectForKey:@"textfield"]]){
            NSArray* values = [attributeObject objectForKey:@"values"];
            pickerData = values;
        }
    }
}

#pragma mark - ACTIONS
- (IBAction)segmentSwitch:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        //NSLog(@"segment 0");
        _recenzjaScrollView.alpha = 0;
        _mainScrollView.alpha = 1;
        _segmentedControl.selectedSegmentIndex = 0;
    }
    else{
        //NSLog(@"segment 1");
        _recenzjaScrollView.alpha = 1;
        _mainScrollView.alpha = 0;
        _recenzjaSegmentedControl.selectedSegmentIndex = 1;
    }
}
- (IBAction)addProductToBasket:(id)sender {
    if([_produkt.disable intValue]!=0){
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"Ten produkt jest niedostępny";
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:1];
    }
    else {
        //ATTRIBUTES
        NSArray* dictKeys = attributesDictionary.allKeys;
        NSMutableDictionary* outputAttributesDictionary = [[NSMutableDictionary alloc] init];
        NSString *outputAttributesJSON = @"";
        for(NSString* singleKey in dictKeys) {
            NSDictionary* singleAttributeDictionary = [attributesDictionary objectForKey:singleKey];
            UITextField* wartoscAtrybutuTextField = [singleAttributeDictionary objectForKey:@"textfield"];
            NSString* wartoscAtrybutu = wartoscAtrybutuTextField.text;
            [outputAttributesDictionary setObject:wartoscAtrybutu forKey:singleKey];
        }
        NSError *error;
        NSData *outputAttributesData = [NSJSONSerialization dataWithJSONObject:outputAttributesDictionary options:NSJSONWritingPrettyPrinted  error:&error];
        if (outputAttributesData) {
            outputAttributesJSON = [[NSString alloc] initWithData:outputAttributesData encoding:NSUTF8StringEncoding];
        }
        //DELETE OLD KOSZYK
        //NEW KOSZYK
        Koszyk*koszyk = [Koszyk MR_createEntity];
        koszyk.attributes = outputAttributesJSON;
        koszyk.identyfikator = _produkt.identyfikator;
        koszyk.ulubione = [NSNumber numberWithBool:NO];
        koszyk.timestamp = @0;
        koszyk.ilosc = [NSNumber numberWithInteger:[_iloscLabel.text integerValue]];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"Dodano produkt do koszyka";
        if(_koszyk) hud.labelText = @"Zaktualizowano koszyk";
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:1];
        
        [_koszyk MR_deleteEntity];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        [self refreshBasketButton];
        if(_koszyk) {
            [_navController popViewControllerAnimated:YES];
        }
    }
}
- (IBAction)addProductFavorites:(id)sender {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = @"Ten produkt jest niedostępny";
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;

    if(_ulubiony){
        [_ulubiony MR_deleteEntity];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        hud.labelText = @"Usunięto z ulubionych";
        [_dodajDoUlubionychButton setTitle:@"Dodaj do ulubionych" forState:UIControlStateNormal];
        _ulubiony=nil;
    }
    else if([_produkt.disable intValue]!=0){
        hud.labelText = @"Ten produkt jest niedostępny";
    }
    else {
        //NEW KOSZYK
        _ulubiony = [Koszyk MR_createEntity];
        _ulubiony.attributes = @"";
        _ulubiony.identyfikator = _produkt.identyfikator;
        _ulubiony.ulubione = [NSNumber numberWithBool:YES];
        _ulubiony.timestamp = @0;
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        
        hud.labelText = @"Dodano produkt do ulubionych";
        [_dodajDoUlubionychButton setTitle:@"Usuń z ulubionych" forState:UIControlStateNormal];
       

    }
    [hud hide:YES afterDelay:1];
}
- (IBAction)incrementIlosc:(id)sender {
    NSString* iloscString = _iloscLabel.text;
    int ilosc = [iloscString intValue];
    ilosc++;
    _iloscLabel.text = [NSString stringWithFormat:@"%i", ilosc];
}
- (IBAction)decrementIlosc:(id)sender {
    NSString* iloscString = _iloscLabel.text;
    int ilosc = [iloscString intValue];
    if(ilosc>0){
        ilosc--;
    }
    _iloscLabel.text = [NSString stringWithFormat:@"%i", ilosc];
}
- (IBAction)showMainImage:(id)sender {
    mgImagePopupViewController *imageVC = [mgImagePopupViewController new];
    UINavigationController *imageNavVC = [[UINavigationController alloc] initWithRootViewController:imageVC];
    UIImage* imageToShow = _mainImageView.image;
    if(IDIOM!=IPAD){
        NSArray*subviews = _imagesScrollView.subviews;
        UIImageView* imageView = (UIImageView*)[subviews objectAtIndex:_pageControl.currentPage];
        imageToShow = imageView.image;
    }
    imageVC.image = imageToShow;
    // present the view controller full-screen on iPhone; in a form sheet on iPad:
    _navController.modalPresentationStyle = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? UIModalPresentationFullScreen : UIModalPresentationFormSheet;
    [self presentViewController:imageNavVC animated:YES completion:nil];
}
#pragma mark - SEGUE METHOD

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"newComment"]) {
        //HIDE TOOLBAR
        mgNewReviewViewController* newReviewVC = (mgNewReviewViewController*)[segue destinationViewController];
        newReviewVC.produkt = _produkt;
    }
}
#pragma mark - OTHER
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark - Modal Views
- (void)goToMainView{
    [_navController goToMainView];
}
- (void)goToBasket{
    [_navController goToBasketAnimated:YES];
}
- (void)goToFavorites{
    [_navController goToFavoritesAnimated:YES];
}
- (void)openMenu{
    [_navController openMenu];
}

@end
