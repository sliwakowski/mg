//
//  mgTableViewController.m
//  mGenerator
//
//  Created by Macbook Pro on 16.03.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgTableViewController.h"
#import <MagicalRecord/MagicalRecord.h>
@interface mgTableViewController ()
@end

@implementation mgTableViewController
#pragma mark - INIT METHODS
- (void)viewDidLoad {
    [super viewDidLoad];
    _navController = (mgNavigationController*) self.navigationController;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(filterOffers:) name:@"filterOffers" object:nil];
    [self getData];
}
-(void)viewDidAppear:(BOOL)animated {
    [[self navigationController] setToolbarHidden:NO animated:YES];
}
- (void)viewWillAppear:(BOOL)animated {
    //NAVIGATION BAR
    mgNavigationBarHandler* navigationBarHandler = [mgNavigationBarHandler new];
    navigationBarHandler.delegate = self;
    [navigationBarHandler configureNavigationBar:_navController];
    _favoritesButton = [navigationBarHandler favoritesButton];
    [_favoritesButton addTarget:self action:@selector(goToFavorites) forControlEvents:UIControlEventTouchUpInside];
    _basketButton = [navigationBarHandler basketButton];
    [_basketButton addTarget:self action:@selector(goToBasket) forControlEvents:UIControlEventTouchUpInside];
    _homeButton = [navigationBarHandler homeButton];
    [_homeButton addTarget:self action:@selector(goToMainView) forControlEvents:UIControlEventTouchUpInside];
}
- (void)viewWillDisappear:(BOOL)animated {
    [_favoritesButton removeFromSuperview];
    [_homeButton removeFromSuperview];
    [_basketButton removeFromSuperview];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewWillDisappear:YES];
    [self viewWillAppear:YES];
}
- (void)getData {
    _products = [NSMutableArray new];
    _products = (NSMutableArray*)[Produkt MR_findAll];
    [self.tableView reloadData];
    [mgJSONHandler synchronizeProducts:^(BOOL finished) {
        if(finished){
            _products = (NSMutableArray*)[Produkt MR_findAll];
            [self.tableView reloadData];
        }
    }];
    [mgJSONHandler synchronizeShippings:^(BOOL finished) { }];
}
- (void)didReceiveMemoryWarning { [super didReceiveMemoryWarning]; }

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView { return 1; }
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section { return [_products count]; }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"productCell" forIndexPath:indexPath];
    
    Produkt*produkt = [_products objectAtIndex:indexPath.row];
    //ZDJECIE
    UIImageView*image = (UIImageView*) [cell viewWithTag:1];
    [image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@&w=500&h=500",produkt.mainImage]]
          placeholderImage:[UIImage imageNamed:@"iphoneList.png"]];
    
    //TITLE
    UITextView*title = (UITextView*) [cell viewWithTag:2];
    title.text = produkt.name;
    [title setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
    title.textColor = UIColorFromRGB(0x666666);
    //DOSTEPNY
    UILabel*dostepny = (UILabel*) [cell viewWithTag:7];
    dostepny.text = @"Dostępny";
    //NOWA CENA
    UILabel*nowaCena = (UILabel*) [cell viewWithTag:4];
    [mgTextHelper configureNewPriceLabel:nowaCena withProdukt:produkt];
    nowaCena.frame = CGRectMake(cell.frame.size.width-nowaCena.frame.size.width-5, nowaCena.frame.origin.y, nowaCena.frame.size.width, nowaCena.frame.size.height);
    //STARA CENA
    UILabel*staraCena = (UILabel*) [cell viewWithTag:5];
    [mgTextHelper configureOldPriceLabel:staraCena withNewPriceLabel:nowaCena withProdukt:produkt];
    //KROPKA
    UIView*kropka = [cell viewWithTag:6];
    kropka.backgroundColor = [mgTextHelper mainColor];
    [kropka.layer setCornerRadius:3.0f];
    if([produkt.disable intValue]!=0) {
        title.textColor = UIColorFromRGB(0xdddddd);
        nowaCena.textColor = UIColorFromRGB(0xd4d4d4);
        kropka.backgroundColor = UIColorFromRGB(0xd4d4d4);
        dostepny.textColor = UIColorFromRGB(0xd4d4d4);
        dostepny.text = @"Niedostępny";
    }
    else {
        title.textColor = [UIColor darkGrayColor];
        nowaCena.textColor = [mgTextHelper mainColor];
        kropka.backgroundColor = [mgTextHelper mainColor];
        dostepny.textColor = [UIColor darkGrayColor];
    }
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //HIDE TOOLBAR
    [[self navigationController] setToolbarHidden:YES animated:YES];
    Produkt*produkt = [_products objectAtIndex:indexPath.row];
    [_navController goToProduct:produkt animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath { return 70.0f; }

#pragma mark - SEGUE METHOD

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        //HIDE TOOLBAR
        [[self navigationController] setToolbarHidden:YES animated:YES];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Produkt*produkt = [_products objectAtIndex:indexPath.row];
        [[segue destinationViewController] setDetailItem:produkt];
    }
}

#pragma mark - SEARCH BAR METHODS

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
    [self.searchBar resignFirstResponder];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBarParam{
    [self filterDataWithType:@"search" query:searchBarParam.text];
}
- (void)filterDataWithType:(NSString*)type query:(NSString*)query{
    _products = [[self filterDataWithType:type query:query products:_products searchBar:self.searchBar segmentedControl:_segmentedControl] mutableCopy];
    [self.tableView reloadData];
}
- (void)filterOffers:(NSNotification *) notification{
    NSDictionary* userInfo = [notification object];
    NSString* filterType = [userInfo objectForKey:@"type"];
    if([filterType isEqualToString:@"kategoria"]){
        Kategoria* kategoria = [userInfo objectForKey:@"kategoria"];
        _products = [[Produkt MR_findByAttribute:@"categoryId" withValue:kategoria.identyfikator andOrderBy:@"identyfikator" ascending:NO] mutableCopy];
    }
    else if([filterType isEqualToString:@"podkategorie"]){
        NSArray*podkategorie = [userInfo objectForKey:@"subcategories"];
        _products = [NSMutableArray new];
        for(Kategoria* kategoria in podkategorie){
            NSArray*tempArray = [[Produkt MR_findByAttribute:@"categoryId" withValue:kategoria.identyfikator andOrderBy:@"identyfikator" ascending:NO] mutableCopy];
            for(Produkt* produkt in tempArray){
                [_products addObject:produkt];
            }
        }
    }
    else {
        [self filterDataWithType:filterType query:[userInfo objectForKey:@"query"]];
        return;
    }
    [self.tableView scrollsToTop];
    [self.tableView reloadData];
    
}
#pragma mark - SEGMENTED BAR

- (IBAction)segmentedBarChanged:(id)sender {
    if(_segmentedControl.selectedSegmentIndex == _selectedSegmentedBarIndex) {
        [self filterDataWithType:@"type" query:@"all"];
    }else if(_segmentedControl.selectedSegmentIndex == 2){
        [self filterDataWithType:@"type" query:@"isBestseller"];
    }else if(_segmentedControl.selectedSegmentIndex == 0){
        [self filterDataWithType:@"type" query:@"isPromotion"];
    }
    else if(_segmentedControl.selectedSegmentIndex == 1){
        [self filterDataWithType:@"type" query:@"isNew"];
    }
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSMutableArray*productsToSort = [_products mutableCopy];
    if(actionSheet.tag==1){
        if (buttonIndex == 0 || buttonIndex == 1) {
            _products = [[productsToSort sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                return [[(Produkt*)a name] compare:[(Produkt*)b name]];
            }] mutableCopy];
        } else if (buttonIndex == 2 || buttonIndex == 3 ) {
            _products = [[productsToSort sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                return [[(Produkt*)a price] compare:[(Produkt*)b price]];
            }] mutableCopy];
        }
        if(buttonIndex==1 || buttonIndex == 3) _products = [[[_products reverseObjectEnumerator] allObjects] mutableCopy];
        [self.tableView reloadData];
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
    else if(actionSheet.tag==2){
        if(buttonIndex==0) {
            [self filterDataWithType:@"type" query:@"all"];
        }
        else if(buttonIndex==1) {
            [self filterDataWithType:@"type" query:@"isPromotion"];
        }
        else if(buttonIndex==2) {
            [self filterDataWithType:@"type" query:@"isNew"];
        }
        else if(buttonIndex==3) {
            [self filterDataWithType:@"type" query:@"isBestseller"];
        }
    }
    
}
#pragma mark - Modal Views
- (void)goToMainView{ [self filterDataWithType:@"type" query:@"all"]; }
- (void)goToBasket{  [_navController goToBasketAnimated:YES]; }
- (void)goToFavorites{ [_navController goToFavoritesAnimated:YES]; }
- (void)openMenu{ [_navController openMenu]; }
- (IBAction)toggleMenu{ [self openMenu]; }

@end
