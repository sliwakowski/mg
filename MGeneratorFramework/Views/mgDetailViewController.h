//
//  mgDetailViewController.h
//  mGenerator
//
//  Created by Macbook Pro on 17.03.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Produkt.h"
#import <QuartzCore/QuartzCore.h>
#import "mgTextHelper.h"
#import "UIImageView+WebCache.h"
#import "Koszyk.h"
#import "mgNavigationController.h"
#import "mgNavigationBarHandler.h"
#import "MFSideMenu.h"
#import "MBProgressHUD.h"
#import "Komentarz.h"
#import "mgJSONHandler.h"
#import "mgReviewCellView.h"
#import "mgFullStarView.h"
#import "mgNewReviewViewController.h"
#import "mgImagePopupViewController.h"

@interface mgDetailViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, mgNavigationBarHandlerDelegate, UIScrollViewDelegate>
@property (strong, nonatomic) Produkt*                      produkt;
@property (strong, nonatomic) Koszyk*                       koszyk;
@property (strong, nonatomic) Koszyk*                       ulubiony;
@property (nonatomic, strong) IBOutlet UIScrollView*        mainScrollView;
@property (nonatomic, strong) IBOutlet UIScrollView*        landscapeScrollView;
@property (nonatomic, strong) IBOutlet UIView*              descriptionContainerView;
@property (nonatomic, strong) mgNavigationBarHandler*       navigationBarHandler;
@property (nonatomic, strong) IBOutlet UIScrollView*        recenzjaScrollView;
@property (nonatomic, strong) IBOutlet UISegmentedControl*  segmentedControl;
@property (nonatomic, strong) IBOutlet UISegmentedControl*  recenzjaSegmentedControl;

@property (nonatomic, strong) IBOutlet UITextView*          titleTextView;

@property (nonatomic, strong) IBOutlet UIImageView*         mainImageView;
@property (nonatomic, strong) IBOutlet UIScrollView*        imagesScrollView;
@property (nonatomic, strong) IBOutlet UIPageControl*       pageControl;

@property (nonatomic, strong) IBOutlet UILabel*             nowaCenaLabel;
@property (nonatomic, strong) IBOutlet UILabel*             staraCenaLabel;
@property (nonatomic, strong) IBOutlet UILabel*             tylkoNowaCenaLabel;

@property (nonatomic, strong) IBOutlet UIButton*            dodajDoKoszykaButton;
@property (nonatomic, strong) IBOutlet UIButton*            dodajDoUlubionychButton;

@property (nonatomic, strong) IBOutlet UIButton*            decremenetIlosc;
@property (nonatomic, strong) IBOutlet UIButton*            incrementIlosc;

@property (nonatomic, strong) IBOutlet UIView*              iloscContainer;
@property (nonatomic, strong) IBOutlet UILabel*             iloscLabel;

@property (nonatomic, strong) IBOutlet UIView*              staticView;
@property (nonatomic, strong) IBOutlet UIScrollView*        atrybutyView;

@property (nonatomic, strong) NSMutableArray*               pageImages;
@property (nonatomic, strong) NSMutableArray*               pageViews;

@property (nonatomic, strong) IBOutlet UIView*              similarOffersView;

@property (nonatomic, strong) mgFullStarView*               activeStars;
@property (nonatomic, strong) mgFullStarView*               mainActiveStars;
@property (nonatomic, strong) IBOutlet UIView*              mainAverageRatingView;
@property (nonatomic, strong) IBOutlet UIView*              averageRatingView;
@property (nonatomic, strong) IBOutlet UIView*              commentsView;
@property (nonatomic, strong) IBOutlet UILabel*             averageRatingTitle;
@property (nonatomic, strong) IBOutlet UIButton*            dodajKomentarzButton;

@property (nonatomic, strong) mgNavigationController*       navController;
@property (nonatomic, strong) UIButton*                     homeButton;
@property (nonatomic, strong) UIButton*                     favoritesButton;
@property (nonatomic, strong) UIButton*                     basketButton;

@property (nonatomic, strong) NSMutableArray*               comments;
@property (nonatomic, assign) UIInterfaceOrientation        orientation;

- (void)setDetailItem:(Produkt*)newDetailItem;


@end
