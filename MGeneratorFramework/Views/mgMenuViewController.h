//
//  mgMenuViewController.h
//  mGenerator
//
//  Created by Macbook Pro on 11.03.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
#import "mgNavigationController.h"

@interface mgMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView* tableView;
@property (nonatomic, strong) mgNavigationController* navigationController;
@property (nonatomic, assign) int openedCategoryIndex;
@property (nonatomic, assign) int closedCategoryIndex;
@property (nonatomic, strong) NSMutableArray* categories;
@property (nonatomic, strong) NSMutableArray* sites;
@property (nonatomic, strong) NSMutableArray* actions;
@property (nonatomic, strong) NSMutableArray* products;

@end
