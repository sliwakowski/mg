//
//  mgImagePopupViewController.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 27.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgImagePopupViewController.h"
#import "mgTextHelper.h"
@interface mgImagePopupViewController ()

@end

@implementation mgImagePopupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _imageView = [UIImageView new];
    CGRect imageViewFrame = CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64);
    if([mgTextHelper isLandscape]) imageViewFrame = CGRectMake(0, 64, self.view.frame.size.height, self.view.frame.size.width-64);
    _imageView.frame = imageViewFrame;
    _imageView.backgroundColor = [UIColor whiteColor];
    [_imageView setImage:_image];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:_imageView];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Anuluj" style:UIBarButtonItemStyleDone target:self action:@selector(cancelItemSelected:)];
}
- (void)cancelItemSelected:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
