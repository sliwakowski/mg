//
//  mgMenuTableViewCell.m
//  aplikacja
//
//  Created by Macbook Pro on 08.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgMenuTableViewCell.h"
#import "mgTextHelper.h"
#import "Kategoria.h"
#import <MagicalRecord/MagicalRecord.h>
@implementation mgMenuTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _title = @"pusto";
        NSString*color = [mgTextHelper getSimpleValueForKeyFromConfiguration:@"base_color"];
        _mainColor = [mgTextHelper pxColorWithHexValue:color];
        self.backgroundColor = _isChild ?  UIColorFromRGB(0x2a2a2a) : UIColorFromRGB(0x343230);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return self;
}
- (void)setViews{
    if(_isChild) self.backgroundColor = [UIColor colorWithWhite:1 alpha:0.01];
    [self setTitleLabel];
    [self setArrow];
}
- (void)setTitleLabel{
    UILabel* titleLabel = _isChild ? [[UILabel alloc] initWithFrame:CGRectMake(30, 5, 220, 32)] : [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 240, 32)];
    titleLabel.text = _isChild ? [_title capitalizedString] : [_title capitalizedString];
    //titleLabel.textColor = _isChild ? [UIColor whiteColor]: _mainColor;
    titleLabel.textColor = _isChild ? [UIColor whiteColor]: [UIColor whiteColor];
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0f];
    [self addSubview:titleLabel];
}
- (void)setArrow{
    if([_detailObject isKindOfClass:[Kategoria class]] && !_isChild){
        Kategoria* kategoria = (Kategoria*) _detailObject;
        NSMutableArray* childCategories = [[Kategoria MR_findByAttribute:@"parentId" withValue:kategoria.identyfikator] mutableCopy];
        if([childCategories count]>0){
            UIImageView* arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(260, 15, 17, 9)];
            arrowImageView.alpha = 0.6;
            //UIImage* arrowImage = [[UIImage imageNamed:@"menuarrow.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIImage* arrowImage = [UIImage imageNamed:@"menuarrow.png"];
            [arrowImageView setImage:arrowImage];
            [self addSubview:arrowImageView];
            float   angle = M_PI;  //rotate 180°, or 1 π radians
            if(_isOpened){
                [UIView animateWithDuration:0.2 animations:^{
                    arrowImageView.layer.transform = CATransform3DMakeRotation(angle, 0, 0.0, 1.0);
                }];
            }
            else if(_isClosed){
                arrowImageView.layer.transform = CATransform3DMakeRotation(angle, 0, 0.0, 1.0);
                [UIView animateWithDuration:0.2 animations:^{
                    arrowImageView.layer.transform = CATransform3DMakeRotation(0, 0, 0.0, 1.0);
                }];
            }
 
        }
    }
}
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
