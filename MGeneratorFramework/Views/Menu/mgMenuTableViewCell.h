//
//  mgMenuTableViewCell.h
//  aplikacja
//
//  Created by Macbook Pro on 08.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mgMenuTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString* title;
@property (nonatomic, assign) BOOL isChild;
@property (nonatomic, assign) BOOL isOpened;
@property (nonatomic, assign) BOOL isClosed;
@property (nonatomic, strong) UIColor* mainColor;
@property (nonatomic, strong) NSObject* detailObject;
- (void)setViews;

@end
