//
//  mgBasketViewController.m
//  aplikacja
//
//  Created by Macbook Pro on 24.05.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgBasketViewController.h"
#import "mgTableViewCell.h"
#import "mgLoginViewController.h"
#import <MagicalRecord/MagicalRecord.h>
@interface mgBasketViewController ()

@end

@implementation mgBasketViewController

#pragma mark - INIT METHODS

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    _navController = (mgNavigationController*) self.navigationController;
    
    _realizujButton.backgroundColor = [mgTextHelper mainColor];
    
    //EDYTUJ ZAMOWIENIE BORDER
    _edytujButton.layer.cornerRadius = 5.0f;
    _edytujButton.layer.borderWidth = 1.0;
    _edytujButton.layer.borderColor = [mgTextHelper mainColor].CGColor;
    _edytujButton.layer.masksToBounds = YES;
    
}
-(void)viewDidAppear:(BOOL)animated {
    [[self navigationController] setToolbarHidden:YES animated:YES];
}
- (void)viewWillAppear:(BOOL)animated {
    UIInterfaceOrientation oldOrientation = _orientation;
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(oldOrientation!=_orientation){
        [_navController popViewControllerAnimated:NO];
        [_navController goToBasketAnimated:NO];
    }
    else if(_shouldOpenGamefication){
        NSString* gameficationUrl = [NSString stringWithFormat:@"%@%@", [mgTextHelper getSimpleValueForKeyFromConfiguration:@"gamefication"], @"63"];
        [_navController openWebpage:gameficationUrl];
        _shouldOpenGamefication = NO;
    }
    //NAVIGATION BAR
    _navigationBarHandler = [mgNavigationBarHandler new];
    _navigationBarHandler.delegate = self;
    [_navigationBarHandler configureNavigationBar:self.navigationController];
    _favoritesButton = [_navigationBarHandler favoritesButton];
    [_favoritesButton addTarget:self action:@selector(goToFavorites) forControlEvents:UIControlEventTouchUpInside];
    _basketButton = [_navigationBarHandler basketButton];
    [_basketButton addTarget:self action:@selector(goToBasket) forControlEvents:UIControlEventTouchUpInside];
    _homeButton = [_navigationBarHandler homeButton];
    [_homeButton addTarget:self action:@selector(goToMainView) forControlEvents:UIControlEventTouchUpInside];
    
    //SUMMARY PRICE
    _summaryPrice = [NSNumber numberWithDouble:0];
    [self setupShippingPicker];
    [self getData];
}
- (void)viewWillDisappear:(BOOL)animated {
    [_favoritesButton removeFromSuperview];
    [_homeButton removeFromSuperview];
    [_basketButton removeFromSuperview];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewWillDisappear:YES];
    [self viewWillAppear:YES];
}
- (void)getData {
    NSNumber* ulubione = [NSNumber numberWithBool:NO];
    _koszykArray = (NSArray*) [Koszyk MR_findByAttribute:@"ulubione" withValue:ulubione];
    _products = [NSMutableArray new];
    for(Koszyk* koszyk in _koszykArray) {
        Produkt* singleKoszykProdukt = [Produkt MR_findFirstByAttribute:@"identyfikator" withValue:koszyk.identyfikator];
        [_products addObject:singleKoszykProdukt];
    }
    [self.tableView reloadData];
    [self refreshSummaryPrice];
}
- (void)refreshBasketButton{
    [_basketButton removeFromSuperview];
    _basketButton = [_navigationBarHandler basketButton];
    [_basketButton addTarget:self action:@selector(goToBasket) forControlEvents:UIControlEventTouchUpInside];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupShippingPicker {
    NSMutableArray*tempShippings = [[Dostawa MR_findAll] mutableCopy];
    _shippingTypes = [NSMutableArray new];
    _shippingTypesStrings = [NSMutableArray new];
    for (Dostawa*tempShipping in tempShippings) {
        [_shippingTypes addObject:tempShipping];
    }
    for(Dostawa*rodzajDostawy in _shippingTypes) {
        [_shippingTypesStrings addObject:[NSString stringWithFormat:@"%@ %g zł", rodzajDostawy.name, [rodzajDostawy.price floatValue]]];
    }
    _shippingSelected = [_shippingTypes objectAtIndex:0];
    //_shippingSelected = nil;
    
    _shippingTextField.textColor = UIColorFromRGB(0x9e9e9e);
    _shippingTextField.text = [_shippingTypesStrings objectAtIndex:0];
    _shippingTextField.layer.cornerRadius = 5.0f;
    _shippingTextField.layer.borderWidth = 1.0;
    _shippingTextField.layer.borderColor = UIColorFromRGB(0x7b7b7b).CGColor;
    _shippingTextField.layer.masksToBounds = YES;
    _shippingTextField.userInteractionEnabled = YES;
    UIPickerView *picker = [[UIPickerView alloc] init];
    picker.dataSource = self;
    picker.delegate = self;
    picker.tag=1;
    _shippingTextField.inputView = picker;

}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if([_products count]==0){
        _realizacjaContainer.hidden=YES;
        _brakContainer.hidden=NO;
    } else {
        _realizacjaContainer.hidden=NO;
        _brakContainer.hidden=YES;
    }
    return [_products count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    mgTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"productCell" forIndexPath:indexPath];
    
    Produkt*produkt = [_products objectAtIndex:indexPath.row];
    Koszyk*koszyk = [_koszykArray objectAtIndex:indexPath.row];
    //ZDJECIE
    UIImageView*image = (UIImageView*) [cell viewWithTag:1];
    [image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@&w=500&h=500",produkt.mainImage]]
          placeholderImage:[UIImage imageNamed:@"iphoneList.png"]];
    //TITLE
    UITextView*title = (UITextView*) [cell viewWithTag:2];
    title.text = produkt.name;
    [title setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
	[title setTextColor:UIColorFromRGB(0x939393)];
    //ILOSC
    UILabel*ilosc = (UILabel*) [cell viewWithTag:7];
    ilosc.text = [NSString stringWithFormat:@"Ilość: %i", [koszyk.ilosc intValue]];
    //NOWA CENA
    UILabel*nowaCena = (UILabel*) [cell viewWithTag:4];
    //STARA CENA
    UILabel*staraCena = (UILabel*) [cell viewWithTag:5];
    if(self.tableView.editing==NO){
        [mgTextHelper configureNewPriceLabel:nowaCena withProdukt:produkt];
        [mgTextHelper configureOldPriceLabel:staraCena withNewPriceLabel:nowaCena withProdukt:produkt];
        nowaCena.frame = CGRectMake(cell.frame.size.width-nowaCena.frame.size.width-5, nowaCena.frame.origin.y, nowaCena.frame.size.width, nowaCena.frame.size.height);
    }
    else {
        nowaCena.text = @"";
        staraCena.text = @"";
    }
    //KROPKA
    UIView*kropka = [cell viewWithTag:6];
    [kropka setBackgroundColor:[mgTextHelper mainColor]];
    [kropka.layer setCornerRadius:3.0f];
    if([produkt.disable intValue]!=0) {
        title.textColor = UIColorFromRGB(0xdddddd);
        nowaCena.textColor = UIColorFromRGB(0xd4d4d4);
        ilosc.backgroundColor = UIColorFromRGB(0xd4d4d4);
        ilosc.textColor = UIColorFromRGB(0xd4d4d4);
    }
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Produkt* produkt = [_products objectAtIndex:indexPath.row];
    Koszyk* koszyk = [_koszykArray objectAtIndex:indexPath.row];
    [_navController goToProduct:produkt fromBasket:koszyk animated:YES];
}
#pragma mark - DELETE METHODS
- (IBAction)toggleTableViewEditing:(id)sender {
    if(self.tableView.editing==YES){
        [self.tableView setEditing:NO animated:YES];
        [self.menuContainerViewController setMenuSlideAnimationEnabled:YES];
        [_edytujButton setTitle:@"Edytuj zamówienie" forState:UIControlStateNormal];
    }
    else {
        [self.tableView setEditing:YES animated:YES];
        [self.menuContainerViewController setMenuSlideAnimationEnabled:NO];
        [_edytujButton setTitle:@"Zakończ edycję" forState:UIControlStateNormal];
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        Koszyk* elementKoszyka = [_koszykArray objectAtIndex:indexPath.row];
        [_products removeObjectAtIndex:indexPath.row];
        [elementKoszyka MR_deleteEntity];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self refreshSummaryPrice];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"Usunięto produkt z koszyka";
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:1];
        [self refreshBasketButton];
    }
}

#pragma mark - ATTRIBUTE PICKER

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(pickerView.tag==1){
        return _shippingTypesStrings.count;
    }
    else return 1;
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {return  1;}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(pickerView.tag==1){
        return _shippingTypesStrings[row];
    }
    else return @"";
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(pickerView.tag==1){
        _shippingSelected = _shippingTypes[row];
        _shippingTextField.text = _shippingTypesStrings[row];
        [pickerView resignFirstResponder];
        [[self view] endEditing:YES];
        [[self view] endEditing:NO];
        [self refreshSummaryPrice];
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

#pragma mark - SUMMARY PRICE
- (void)refreshSummaryPrice {
    _summaryPrice = [NSNumber numberWithDouble:0];
    for(Koszyk* koszyk in _koszykArray) {
        Produkt* singleKoszykProdukt = [Produkt MR_findFirstByAttribute:@"identyfikator" withValue:koszyk.identyfikator];
        //ADD SUMMARY PRICE
        double currentProduktPriceMultipliedByQuantity = 0;
        if([singleKoszykProdukt.promotionPrice doubleValue]==0){
            currentProduktPriceMultipliedByQuantity = [singleKoszykProdukt.price doubleValue]*[koszyk.ilosc intValue];
        }
        else {
            currentProduktPriceMultipliedByQuantity = [singleKoszykProdukt.promotionPrice doubleValue]*[koszyk.ilosc intValue];
        }
        _summaryPrice = [NSNumber numberWithDouble:(currentProduktPriceMultipliedByQuantity + _summaryPrice.doubleValue)];
    }
    if(_shippingSelected==nil){
        _summaryPrice = [NSNumber numberWithDouble:(9.0f+ _summaryPrice.doubleValue)];
    }
    else _summaryPrice = [NSNumber numberWithDouble:([_shippingSelected.price floatValue]+ _summaryPrice.doubleValue)];
    _summaryPriceLabel.text = [NSString stringWithFormat:@"%@ zł",[_summaryPrice stringValue]];
    _summaryPriceLabel.textColor = [mgTextHelper mainColor];
}
#pragma mark - REALIZUJ ZAMOWIENIE
- (IBAction)realizujZamowienie:(id)sender{
    [self getPaymentData];
}
- (void)getPaymentData{
    
    
    [mgJSONHandler getPaymentData:^(NSDictionary *response) {
        NSNumber* status = response[@"status"];
        if(status.intValue==0) {
            // Brak platnosci
            
            [self realizujZamowienie];
        } else {
            NSDictionary* paymentData = response[@"p24"];
            // Pobierz dane do platnosci
            p24Config = [[P24Config alloc] init];
            p24Config.merchantId = (int) paymentData[@"p24_spid"];
            p24Config.crc = paymentData[@"p24_crc"];
            p24Config.storeLoginData = YES;
            // p24Config.useMobileBankStyles = NO;
            p24Config.p24Url = paymentData[@"p24_link"];
            
            p24 = [[P24 alloc] initWithConfig:p24Config delegate:self];
            [self realizujZamowienie];
        }
    }];
}
- (void)realizujZamowienie{
    
    NSString*userName = [mgTextHelper client:@"name"];
    if([userName isEqualToString:@""]){
        [_navController goToLoginWithBasket:self animated:YES];
    }
    else {
        MBProgressHUD* progressHud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        progressHud.mode = MBProgressHUDModeIndeterminate;
        progressHud.labelText = @"Wysyłam zamówienie...";
        progressHud.removeFromSuperViewOnHide = YES;
        [progressHud show:YES];
        [mgPOSTHandler registerOrderWith:_shippingSelected totalPrice:[_summaryPrice floatValue] completionHandler:^(NSString *response) {
            NSError *e;
            NSDictionary *data =
            [NSJSONSerialization JSONObjectWithData: [response dataUsingEncoding:NSUTF8StringEncoding]
                                            options: NSJSONReadingMutableContainers
                                              error: &e];
            NSDictionary* responseObject = [data objectForKey:@"response"];
            NSString* sessionId = [responseObject objectForKey:@"sessionId"];
            mgSummaryViewController* summaryVC = [_navController.storyboard instantiateViewControllerWithIdentifier:@"summaryVC"];
            summaryVC.request = responseObject;
            summaryVC.basketVC = self;
            summaryVC.orderId = sessionId;
            [_navController pushViewController:summaryVC animated:YES];
            //[self startPaymentWithData:response];
            [progressHud hide:YES];
        }];
    }
}
- (void)startPaymentWithData:(NSDictionary*)paymentData {
    /*
    NSError *e;
    if(!paymentData) return;
    NSDictionary *data =
    [NSJSONSerialization JSONObjectWithData: [paymentData dataUsingEncoding:NSUTF8StringEncoding]
                                    options: NSJSONReadingMutableContainers
                                      error: &e];
     */
    NSDictionary* response = paymentData;
    NSNumber* amount = [response objectForKey:@"amount"];
    P24Payment *p24Payment = [[P24Payment alloc] init];
    p24Payment.sessionId = [response objectForKey:@"sessionId"];
    p24Payment.clientAddress = [response objectForKey:@"clientAddress"];
    p24Payment.amount = [amount intValue];
    p24Payment.clientCity = @"Poznan";
    p24Payment.clientZipCode = @"61-600";
    p24Payment.clientName = [response objectForKey:@"clientName"];
    p24Payment.clientCountry = [response objectForKey:@"clientCountry"];
    p24Payment.language = @"pl";
    p24Payment.currency = [response objectForKey:@"currency"];
    p24Payment.clientEmail = [response objectForKey:@"clientEmail"];
    //    p24Payment.method = @"124"; credit card
    
    
    // start payment
    [p24 startPayment:p24Payment inViewController:self];
}
#pragma mark - Przelewy24 Delegate
- (void)p24:(P24 *)p24 didFinishPayment:(P24Payment *)p24Payment withResult:(P24PaymentResult *)p24PaymentResult {
    // transakcja zakończona
    // należy sprawdzić status transakcji
    BOOL paymentOk = [p24PaymentResult isOk];
    if (paymentOk) {
        NSLog(@"Payment ok");
    } else {
    }
    NSLog(@"Payment error: %d, %@",
          p24PaymentResult.status.code,
          p24PaymentResult.status.description);
}
- (void)p24:(P24 *)p24 didCancelPayment:(P24Payment *)p24Payment {
    // transakcja została anulowana przez użytkownika
    NSLog(@"Payment cancelled");
    _shouldOpenGamefication=YES;
    [self viewWillDisappear:YES];
    [self viewWillAppear:YES];
}
- (void)p24:(P24 *)p24 didFailPayment:(P24Payment *)p24Payment
  withError:(NSError *)error {
    // wystąpił błąd podczas wykonywania transakcji
    NSLog(@"Payment error: %@", [error localizedDescription]);
}
#pragma mark - SEGUE METHOD

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        //HIDE TOOLBAR
        [[self navigationController] setToolbarHidden:YES animated:YES];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Produkt*produkt = [_products objectAtIndex:indexPath.row];
        [[segue destinationViewController] setDetailItem:produkt];
    }
}

#pragma mark - Modal Views
- (void)goToMainView{
    [_navController goToMainView];
}
- (void)goToBasket{
    [_navController goToBasketAnimated:YES];
}
- (void)goToFavorites{
    [_navController goToFavoritesAnimated:YES];
}
- (void)openMenu{
    [_navController openMenu];
}


@end
