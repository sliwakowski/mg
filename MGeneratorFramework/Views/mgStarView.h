//
//  mgStarView.h
//  aplikacja
//
//  Created by Macbook Pro on 04.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mgStarView : UIView

- (id)initWithSize:(float)starSize maxStars:(float)maxStars activeStarColor:(UIColor*)activeStarColor;
@property (nonatomic, assign) float starSize;
@property (nonatomic, assign) float starMargin;
@property (nonatomic, assign) float maxStars;
@property (nonatomic, strong) UIColor* activeStarColor;
@end
