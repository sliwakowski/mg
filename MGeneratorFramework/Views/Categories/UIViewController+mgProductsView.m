//
//  UIViewController+mgProductsView.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 29.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "UIViewController+mgProductsView.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation UIViewController (mgProductsView)

-(NSArray*)filterDataWithType:(NSString*)type query:(NSString*)query products:(NSArray*)products searchBar:(UISearchBar*)searchBar segmentedControl:(UISegmentedControl*)segmentedControl{
    NSPredicate *predicate = nil;
    NSMutableArray*newProducts = [NSMutableArray new];
    if([type isEqualToString:@"search"]){
        predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", query];
        products = (NSMutableArray*) [Produkt MR_findAllWithPredicate:predicate];
        //[self.view endEditing:YES];
        [searchBar resignFirstResponder];
    }
    else if([type isEqualToString:@"type"]){
        NSMutableArray*allProducts = (NSMutableArray*)[Produkt MR_findAll];
        for(Produkt*p in allProducts){
            BOOL bestseller = [p.isBestseller boolValue];
            BOOL promocja = [p.isPromotion boolValue];
            BOOL nowosc = [p.isNew boolValue];
            if([query isEqualToString:@"isBestseller"] && bestseller==YES) {
                [segmentedControl setSelectedSegmentIndex:2];
                [newProducts addObject:p];
            }
            else if([query isEqualToString:@"isPromotion"] && promocja==YES) {
                [segmentedControl setSelectedSegmentIndex:0];
                [newProducts addObject:p];
            }
            else if([query isEqualToString:@"isNew"] && nowosc==YES) {
                [segmentedControl setSelectedSegmentIndex:1];
                [newProducts addObject:p];
            }
            else if([query isEqualToString:@"all"]){
                [segmentedControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
                [newProducts addObject:p];
            }
        }
        products = newProducts;
    }
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    //[self.tableView reloadData];
    return products;
}

-(IBAction)showActionSheet:(id)sender {
    UIBarButtonItem* senderButton = sender;
    UIActionSheet *popupQuery = [UIActionSheet new];
    if(senderButton.tag==1){
        popupQuery = [[UIActionSheet alloc] initWithTitle:@"Sortowanie" delegate:nil cancelButtonTitle:@"Anuluj" destructiveButtonTitle:nil otherButtonTitles:@"Nazwa A-Z", @"Nazwa Z-A", @"Cena rosnąco", @"Cena malejąco", nil];
    }
    else if(senderButton.tag==2){
        popupQuery = [[UIActionSheet alloc] initWithTitle:@"Filtrowanie" delegate:nil cancelButtonTitle:@"Anuluj" destructiveButtonTitle:nil otherButtonTitles:@"Wszystkie produkty", @"Promocje", @"Nowości", @"Bestsellery", nil];
    }
    popupQuery.tag = senderButton.tag;
    popupQuery.actionSheetStyle = UIActionSheetStyleDefault;
    [popupQuery showInView:self.view];
}

@end
