//
//  UIViewController+mgProductsView.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 29.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Produkt.h"
@interface UIViewController (mgProductsView)

-(NSArray*)filterDataWithType:(NSString*)type query:(NSString*)query products:(NSArray*)products searchBar:(UISearchBar*)searchBar segmentedControl:(UISegmentedControl*)segmentedControl;
-(IBAction)showActionSheet:(id)sender;
@end
