//
//  mgTableViewController.h
//  mGenerator
//
//  Created by Macbook Pro on 16.03.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgDetailViewController.h"
#import "mgJSONHandler.h"
#import "Produkt.h"
#import "UIImageView+WebCache.h"
#import "MFSideMenu.h"
#import "mgBasketViewController.h"
#import "mgFavoritesViewController.h"
#import "mgNavigationBarHandler.h"
#import "mgNavigationController.h"
#import "Kategoria.h"
#import "UIViewController+mgProductsView.h"

@interface mgTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, mgNavigationBarHandlerDelegate, UIActionSheetDelegate>
@property (nonatomic, weak) IBOutlet UISearchBar*           searchBar;
@property (nonatomic, weak) IBOutlet UISegmentedControl*    segmentedControl;
@property (nonatomic, strong) mgDetailViewController*       detailViewController;
@property (nonatomic, strong) NSMutableArray*               products;
@property (nonatomic, strong) mgNavigationController*       navController;
@property (nonatomic, strong) UIButton*                     homeButton;
@property (nonatomic, strong) UIButton*                     favoritesButton;
@property (nonatomic, strong) UIButton*                     basketButton;
@property (nonatomic, assign) int                           selectedSegmentedBarIndex;
- (IBAction)segmentedBarChanged:(id)sender;
@end
