//
//  mgBasketViewController.h
//  aplikacja
//
//  Created by Macbook Pro on 24.05.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgDetailViewController.h"
#import "mgNavigationBarHandler.h"
#import "mgNavigationController.h"
#import "Dostawa.h"
#import "P24.h"
#import "mgSummaryViewController.h"

@interface mgBasketViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, mgNavigationBarHandlerDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, P24Delegate> {
    P24Config *p24Config;
    P24 *p24;
}


@property (strong, nonatomic) mgDetailViewController*       detailViewController;
@property (strong, nonatomic) NSArray*                      koszykArray;
@property (nonatomic, strong) NSMutableArray*               products;
@property (nonatomic, strong) mgNavigationBarHandler*       navigationBarHandler;
@property (nonatomic, strong) IBOutlet UITextField*         shippingTextField;
@property (nonatomic, strong) IBOutlet UITextField*         shippingDetailTextField;
@property (nonatomic, strong) UIPickerView*                 shippingPickerView;
@property (nonatomic, strong) UIPickerView*                 shippingDetailPickerView;
@property (nonatomic, strong) NSMutableArray*               shippingTypes;
@property (nonatomic, strong) NSArray*                      shippingDetailTypes;
@property (nonatomic, strong) NSMutableArray*               shippingTypesStrings;
@property (nonatomic, strong) Dostawa*                      shippingSelected;
@property (nonatomic, strong) NSNumber*                     summaryPrice;
@property (nonatomic, strong) IBOutlet UILabel*             summaryPriceLabel;

@property (nonatomic, strong) IBOutlet UIButton*            edytujButton;
@property (nonatomic, strong) IBOutlet UIButton*            realizujButton;

@property (nonatomic, strong) mgNavigationController*       navController;
@property (nonatomic, strong) UIButton*                     homeButton;
@property (nonatomic, strong) UIButton*                     favoritesButton;
@property (nonatomic, strong) UIButton*                     basketButton;

@property (nonatomic, strong) IBOutlet UIView*              realizacjaContainer;
@property (nonatomic, strong) IBOutlet UIView*              brakContainer;

@property (nonatomic, assign) UIInterfaceOrientation        orientation;
@property (nonatomic, assign) BOOL                          shouldOpenGamefication;

- (void)startPaymentWithData:(NSDictionary*)paymentData;
- (void)realizujZamowienie;

@end
