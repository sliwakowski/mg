//
//  mgQRViewController.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 14.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgQRViewController.h"
#import "ScannerViewController.h"
#import "Barcode.h"
#import <MagicalRecord/MagicalRecord.h>
@interface mgQRViewController ()

@end

@implementation mgQRViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    _qrCodeHistory = [NSMutableArray new];
    _qrCodeHistory = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"qrCodeHistory"]];
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    _navController = (mgNavigationController*)self.navigationController;
    if([_qrCodeHistory count]==0){
        [self openScanner];
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [_tableView reloadData];
    [[self navigationController] setToolbarHidden:NO animated:YES];
    UIInterfaceOrientation oldOrientation = _orientation;
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(oldOrientation!=_orientation){
        [_navController popViewControllerAnimated:NO];
        [_navController goToPage:@"qrVC" animated:NO];
    }
    
    //NAVIGATION BAR
    mgNavigationBarHandler* navigationBarHandler = [mgNavigationBarHandler new];
    navigationBarHandler.delegate = self;
    [navigationBarHandler configureNavigationBar:self.navigationController];
    _favoritesButton = [navigationBarHandler favoritesButton];
    [_favoritesButton addTarget:self action:@selector(goToFavorites) forControlEvents:UIControlEventTouchUpInside];
    _basketButton = [navigationBarHandler basketButton];
    [_basketButton addTarget:self action:@selector(goToBasket) forControlEvents:UIControlEventTouchUpInside];
    _titleLabel = [navigationBarHandler titleLabel:@"Skaner QR"];
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [_favoritesButton removeFromSuperview];
    [_titleLabel removeFromSuperview];
    [_basketButton removeFromSuperview];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewWillDisappear:YES];
    [self viewWillAppear:YES];
}
- (IBAction)skanuj:(id)sender{
    [self openScanner];
}
- (void)openScanner{
    
    ScannerViewController* scanningVC = [self.navController.storyboard instantiateViewControllerWithIdentifier:@"ScannerViewController"];
    UINavigationController *scanningNavVC = [[UINavigationController alloc] initWithRootViewController:scanningVC];
    
    
    // configure the scanning view controller:
    scanningVC.resultBlock = ^(Barcode *result) {
        [self handleScannerResult:result];
        [scanningNavVC dismissViewControllerAnimated:YES completion:nil];
    };
    scanningVC.cancelBlock = ^() {
        [scanningNavVC dismissViewControllerAnimated:YES completion:nil];
    };
    scanningVC.errorBlock = ^(NSError *error) {
        // todo: show a UIAlertView orNSLog the error
        [scanningNavVC dismissViewControllerAnimated:YES completion:nil];
    };
    
    
    // present the view controller full-screen on iPhone; in a form sheet on iPad:
    scanningNavVC.modalPresentationStyle = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? UIModalPresentationFullScreen : UIModalPresentationFormSheet;
    [self presentViewController:scanningNavVC animated:YES completion:nil];
}
- (void)handleScannerResult:(Barcode*)barcode{
    
    if([[barcode getBarcodeType] isEqualToString:@"org.iso.QRCode"])
    {
        NSString* stringUrl = [barcode getBarcodeData];
        
        NSURL *url = [NSURL URLWithString:stringUrl];
        NSArray* urlComponents = [url pathComponents];
        NSLog(@"path components: %@", [url pathComponents]);
        
        NSString* shopId = [urlComponents objectAtIndex:1];
        NSString* action = [urlComponents objectAtIndex:2];
        NSString* parameter = [urlComponents objectAtIndex:3];
        if([[mgTextHelper shopId] isEqualToString:shopId]){
            if([action isEqualToString:@"openProduct"]){
                NSArray* products = [Produkt MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithInt:parameter.intValue]];
                if([products count]>0)
                {
                    Produkt* produkt = [products objectAtIndex:0];
                    [_navController goToProduct:produkt animated:YES];
                }
            }
            else if([action isEqualToString:@"openCategory"]){
                NSArray* kategorie = [Kategoria MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithInt:parameter.intValue]];
                if([kategorie count]>0){
                    Kategoria* kategoria = [kategorie objectAtIndex:0];
                    [_navController goToCategory:kategoria];
                }
            }
        }
        
        [_qrCodeHistory insertObject:stringUrl atIndex:0];
        [[NSUserDefaults standardUserDefaults] setObject:_qrCodeHistory forKey:@"qrCodeHistory"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_tableView reloadData];
    }
    else
    {
        NSString* barcodeString = [barcode getBarcodeData];
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        NSNumber* barcode = [f numberFromString:barcodeString];
        NSArray* products = [Produkt MR_findByAttribute:@"barcode" withValue:barcode];
        if([products count]>0)
        {
            Produkt* produkt = [products objectAtIndex:0];
            [_navController goToProduct:produkt animated:YES];
        }
        
        [_qrCodeHistory insertObject:barcode atIndex:0];
        [[NSUserDefaults standardUserDefaults] setObject:_qrCodeHistory forKey:@"qrCodeHistory"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_tableView reloadData];
    }
   
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_qrCodeHistory count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"qrCodeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    id codeID = [_qrCodeHistory objectAtIndex:indexPath.row];
    NSString* qrCodeUrl;
    NSNumber* barcode = 0;
    NSString*codeTitle = @"";
    if([codeID isKindOfClass:[NSString class]]){
        qrCodeUrl = codeID;
        NSURL *url = [NSURL URLWithString:qrCodeUrl];
        NSArray* urlComponents = [url pathComponents];
        NSString* shopId = [urlComponents objectAtIndex:1];
        NSString* action = [urlComponents objectAtIndex:2];
        NSString* parameter = [urlComponents objectAtIndex:3];
        if([[mgTextHelper shopId] isEqualToString:shopId]){
            if([action isEqualToString:@"openProduct"]){
                NSArray* products = [Produkt MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithInt:parameter.intValue]];
                if(products.count>0)
                {
                    Produkt* produkt = [products objectAtIndex:0];
                    codeTitle = [NSString stringWithFormat:@"Produkt: %@", produkt.name];
                }
                else {
                    codeTitle = @"Nieznany produkt";
                }
            }
            else if([action isEqualToString:@"openCategory"]){
                NSArray* kategorie = [Kategoria MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithInt:parameter.intValue]];
                if(kategorie.count>0)
                {
                   Kategoria* kategoria = [kategorie objectAtIndex:0];
                    codeTitle = [NSString stringWithFormat:@"Kategoria: %@", kategoria.name];
                }
                else {
                    codeTitle = @"Nieznana kategoria";
                }
            }
            else {
                codeTitle = @"Nieznana akcja";
            }
        }
        else {
            codeTitle = @"Błędny kod QR";
        }
    }
    else if([codeID isKindOfClass:[NSNumber class]])
    {
        barcode = codeID;
        NSArray* produkty = [Produkt MR_findByAttribute:@"barcode" withValue:barcode];
        if(produkty.count>0)
        {
            Produkt* produkt = [produkty objectAtIndex:0];
            codeTitle = [NSString stringWithFormat:@"Produkt: %@", produkt.name];
        }
        else{
            codeTitle = @"Nieznany produkt";
        }

    }
    
    UILabel* titleLabel = (UILabel*) [cell viewWithTag:1];
    titleLabel.text = codeTitle;
    UILabel* addressLabel = (UILabel*) [cell viewWithTag:2];
    addressLabel.text = qrCodeUrl ? qrCodeUrl : [NSString stringWithFormat:@"Kod: %@", barcode];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id codeID = [_qrCodeHistory objectAtIndex:indexPath.row];
    NSString* qrCodeUrl = @"";
    NSNumber* barcode = 0;
    if([codeID isKindOfClass:[NSString class]]){
        qrCodeUrl = codeID;
        NSURL *url = [NSURL URLWithString:qrCodeUrl];
        NSArray* urlComponents = [url pathComponents];
        NSLog(@"path components: %@", [url pathComponents]);
    
        NSString* shopId = [urlComponents objectAtIndex:1];
        NSString* action = [urlComponents objectAtIndex:2];
        NSString* parameter = [urlComponents objectAtIndex:3];
        if([[mgTextHelper shopId] isEqualToString:shopId]){
            if([action isEqualToString:@"openProduct"]){
                NSArray* produkty = [Produkt MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithInt:parameter.intValue]];
                if(produkty.count>0)
                {
                    Produkt* produkt = [produkty objectAtIndex:0];
                    [_navController goToProduct:produkt animated:YES];
                }
            }
            else if([action isEqualToString:@"openCategory"]){
                NSArray* kategorie = [Kategoria MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithInt:parameter.intValue]];
                if(kategorie.count>0)
                {
                    Kategoria* kategoria = [kategorie objectAtIndex:0];
                    [_navController goToCategory:kategoria];
                }
            }
        }
    }
    else{
        barcode = codeID;
        NSArray* produkty = [Produkt MR_findByAttribute:@"barcode" withValue:barcode];
        if(produkty.count>0)
        {
            Produkt* produkt = [produkty objectAtIndex:0];
            [_navController goToProduct:produkt animated:YES];
        }
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Modal Views
- (void)goToMainView{
    [_navController goToMainView];
}
- (void)goToBasket{
    [_navController goToBasketAnimated:YES];
}
- (void)goToFavorites{
    [_navController goToFavoritesAnimated:YES];
}
- (void)openMenu{
    [_navController openMenu];
}

@end
