//
//  mgCategoriesTableViewController.m
//  aplikacja
//
//  Created by Macbook Pro on 19.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgCategoriesTableViewController.h"
#import <MagicalRecord/MagicalRecord.h>


@interface mgCategoriesTableViewController ()

@end

@implementation mgCategoriesTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSMutableArray*tempCategories = [[Kategoria MR_findByAttribute:@"parentId" withValue:_kategoria.identyfikator] mutableCopy];
    _navController = (mgNavigationController*) self.navigationController;
    _categories = [NSMutableArray new];
    Kategoria* wszystkieProdukty = [[Kategoria MR_findByAttribute:@"name" withValue:@"Wszystkie produkty"] objectAtIndex:0];
    wszystkieProdukty.identyfikator = _kategoria.identyfikator;
    wszystkieProdukty.parentId = _kategoria.identyfikator;
    for(Kategoria*k in tempCategories){
        //if(k.disable==@NO){
            [_categories addObject:k];
        //}
    }
    [_categories addObject:wszystkieProdukty];
}
- (void)viewWillAppear:(BOOL)animated {
    //NAVIGATION BAR
    mgNavigationBarHandler* navigationBarHandler = [mgNavigationBarHandler new];
    navigationBarHandler.delegate = self;
    [navigationBarHandler configureNavigationBar:self.navigationController];
    _favoritesButton = [navigationBarHandler favoritesButton];
    [_favoritesButton addTarget:self action:@selector(goToFavorites) forControlEvents:UIControlEventTouchUpInside];
    _basketButton = [navigationBarHandler basketButton];
    [_basketButton addTarget:self action:@selector(goToBasket) forControlEvents:UIControlEventTouchUpInside];
    _homeButton = [navigationBarHandler homeButton];
    [_homeButton addTarget:self action:@selector(goToMainView) forControlEvents:UIControlEventTouchUpInside];
}
-(void)viewDidAppear:(BOOL)animated {
    [[self navigationController] setToolbarHidden:YES animated:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [_favoritesButton removeFromSuperview];
    [_homeButton removeFromSuperview];
    [_basketButton removeFromSuperview];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewWillDisappear:YES];
    [self viewWillAppear:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_categories count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"categoryCell" forIndexPath:indexPath];
    Kategoria* kat = [_categories objectAtIndex:indexPath.row];
    cell.textLabel.text = [[kat.name capitalizedString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if([kat.identyfikator intValue] != [kat.parentId intValue]) {
        cell.detailTextLabel.text = [self getProductsCount:kat];
    } else {
        cell.detailTextLabel.text = @"";
    }
    return cell;
}
- (NSString*) getProductsCount:(Kategoria*)category{
    if(category.products.count==0) return @"Brak produktów";
    else if(category.products.count==0) return @"1 produkt";
    else if(category.products.count>=2 && category.products.count<=4) return [NSString stringWithFormat:@"%i produkty", (int)category.products.count];
    else if(category.products.count>=5) return [NSString stringWithFormat:@"%i produktów", (int)category.products.count];
    return @"";
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Kategoria* selectedCategory = [_categories objectAtIndex:indexPath.row];
    NSMutableArray* categoriesArray = [NSMutableArray arrayWithObject:selectedCategory];
    
    if([selectedCategory.identyfikator intValue]==[selectedCategory.parentId intValue]){
        
        [categoriesArray addObject:_kategoria];
    }
    
    [_navController loadAllSubcategories:categoriesArray];
}
#pragma mark - Modal Views
- (void)goToMainView{
    [_navController goToMainView];
}
- (void)goToBasket{
    [_navController goToBasketAnimated:YES];
}
- (void)goToFavorites{
    [_navController goToFavoritesAnimated:YES];
}
- (void)openMenu{
    [_navController openMenu];
}

@end
