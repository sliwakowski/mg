//
//  mgFirstViewController.m
//  mGenerator
//
//  Created by Macbook Pro on 25.02.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgCollectionViewController.h"
#import <MagicalRecord/MagicalRecord.h>

@interface mgCollectionViewController ()
@end
@implementation mgCollectionViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    _navController = (mgNavigationController*) self.navigationController;
    _productsPage = 1;
    [self getData];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(filterOffers:)
                                                 name:@"filterOffers"
                                               object:nil];
}
-(void)viewDidAppear:(BOOL)animated {
    [[self navigationController] setToolbarHidden:NO animated:YES];
}
- (void)viewWillAppear:(BOOL)animated {
    //NAVIGATION BAR
    mgNavigationBarHandler* navigationBarHandler = [mgNavigationBarHandler new];
    navigationBarHandler.delegate = self;
    [navigationBarHandler configureNavigationBar:_navController];
    _favoritesButton = [navigationBarHandler favoritesButton];
    [_favoritesButton addTarget:self action:@selector(goToFavorites) forControlEvents:UIControlEventTouchUpInside];
    _basketButton = [navigationBarHandler basketButton];
    [_basketButton addTarget:self action:@selector(goToBasket) forControlEvents:UIControlEventTouchUpInside];
    
    _homeButton = [navigationBarHandler homeButton];
    [_homeButton addTarget:self action:@selector(goToMainView) forControlEvents:UIControlEventTouchUpInside];

    //PORTRAIT / LANDSCAPE HANDLING
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    [_segmentedControl setCenter:CGPointMake(navigationBarHandler.screenWidth/2, _segmentedControl.center.y)];
}
- (void)viewWillDisappear:(BOOL)animated {
    [_favoritesButton removeFromSuperview];
    [_homeButton removeFromSuperview];
    [_basketButton removeFromSuperview];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewWillDisappear:YES];
    [self viewWillAppear:YES];
}
- (void)getData {
    _products = [NSMutableArray new];
    _products = (NSMutableArray*)[Produkt MR_findAll];
    [self.collectionView reloadData];
    [mgJSONHandler synchronizeProducts:^(BOOL finished) {
        if(finished){
            _products = (NSMutableArray*)[Produkt MR_findAll];
            [self.collectionView reloadData];
        }
    }];
    [mgJSONHandler synchronizeShippings:^(BOOL finished) {
        // do
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark - UICollectionView Datasource
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [_products count]>60*_productsPage ? 60*_productsPage : [_products count];
}
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"productGridCell" forIndexPath:indexPath];
    Produkt*produkt = [_products objectAtIndex:indexPath.row];
    //ZDJECIE
    UIImageView*image = (UIImageView*) [cell viewWithTag:1];
    [image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@&w=235",produkt.mainImage]]
          placeholderImage:[UIImage imageNamed:@"ipadCollection.png"]];
    
    //TITLE
    UITextView*title = (UITextView*) [cell viewWithTag:2];
    title.text = produkt.name;
    [title setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
    //DOSTEPNY
    UILabel*dostepny = (UILabel*) [cell viewWithTag:7];
    dostepny.text = @"Dostępny";
    //NOWA CENA
    UILabel*nowaCena = (UILabel*) [cell viewWithTag:4];
    [mgTextHelper configureNewPriceLabel:nowaCena withProdukt:produkt];
    nowaCena.frame = CGRectMake(cell.frame.size.width-nowaCena.frame.size.width-5, nowaCena.frame.origin.y, nowaCena.frame.size.width, nowaCena.frame.size.height);
    //STARA CENA
    UILabel*staraCena = (UILabel*) [cell viewWithTag:5];
    [mgTextHelper configureOldPriceLabel:staraCena withNewPriceLabel:nowaCena withProdukt:produkt];
    //KROPKA
    UIView*kropka = [cell viewWithTag:6];
    [kropka.layer setCornerRadius:3.0f];
    kropka.backgroundColor = [mgTextHelper mainColor];
    if([produkt.disable intValue]!=0) {
        title.textColor = UIColorFromRGB(0xdddddd);
        nowaCena.textColor = UIColorFromRGB(0xd4d4d4);
        kropka.backgroundColor = UIColorFromRGB(0xd4d4d4);
        dostepny.textColor = UIColorFromRGB(0xd4d4d4);
        dostepny.text = @"Niedostępny";
    }
    else {
        title.textColor = [UIColor darkGrayColor];
        nowaCena.textColor = [mgTextHelper mainColor];
        kropka.backgroundColor = [mgTextHelper mainColor];
        dostepny.textColor = [UIColor darkGrayColor];
    }
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //HIDE TOOLBAR
    [[self navigationController] setToolbarHidden:YES animated:YES];
    Produkt*produkt = [_products objectAtIndex:indexPath.row];
    [_navController goToProduct:produkt animated:YES];
}
#pragma mark – UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(IDIOM == IPAD){
        return CGSizeMake(235, 255);
    }
    else {
        return CGSizeMake(150, 200);
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if(IDIOM == IPAD){
       return UIEdgeInsetsMake(10, 10, 10, 10);
    }
    else {
      return UIEdgeInsetsMake(5, 5, 5, 5);
    }
    
}

#pragma mark - SEARCH BAR METHODS

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
    [self.searchBar resignFirstResponder];
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    float scrollOffset = scrollView.contentOffset.y;
    if (scrollOffset == 0)
    {
        // then we are at the top
    }
    else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
    {
        _productsPage++;
        [self.collectionView reloadData];
    }
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBarParam{
    [self filterDataWithType:@"search" query:searchBarParam.text];
}
- (void)filterDataWithType:(NSString*)type query:(NSString*)query{
    NSPredicate *predicate = nil;
    NSMutableArray*newProducts = [NSMutableArray new];
    if([type isEqualToString:@"search"]){
        predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", query];
        _products = (NSMutableArray*) [Produkt MR_findAllWithPredicate:predicate];
        [self.view endEditing:YES];
        [self.searchBar resignFirstResponder];
    }
    else if([type isEqualToString:@"type"]){
        NSMutableArray*allProducts = (NSMutableArray*)[Produkt MR_findAll];
        for(Produkt*p in allProducts){
            BOOL bestseller = [p.isBestseller boolValue];
            BOOL promocja = [p.isPromotion boolValue];
            if([query isEqualToString:@"isBestseller"] && bestseller==YES) {
                [newProducts addObject:p];
            }
            else if([query isEqualToString:@"isPromotion"] && promocja==YES) {
                [newProducts addObject:p];
            }
            else if([query isEqualToString:@"all"]){
                [newProducts addObject:p];
            }
        }
        _products = newProducts;
        NSLog(@"a");
    }
    
    self.searchBar.text = @"";
    [self.searchBar resignFirstResponder];
    [self.collectionView reloadData];
}
- (void)filterOffers:(NSNotification *) notification{
    NSDictionary* userInfo = [notification object];
    NSString* filterType = [userInfo objectForKey:@"type"];
    if([filterType isEqualToString:@"kategoria"]){
        Kategoria* kategoria = [userInfo objectForKey:@"kategoria"];
        _products = [[Produkt MR_findByAttribute:@"categoryId" withValue:kategoria.identyfikator andOrderBy:@"identyfikator" ascending:NO] mutableCopy];
        [self.collectionView scrollsToTop];
        [self.collectionView reloadData];
    }
    else if([filterType isEqualToString:@"podkategorie"]){
        NSArray*podkategorie = [userInfo objectForKey:@"subcategories"];
        _products = [NSMutableArray new];
        for(Kategoria* kategoria in podkategorie){
            NSArray*tempArray = [[Produkt MR_findByAttribute:@"categoryId" withValue:kategoria.identyfikator andOrderBy:@"identyfikator" ascending:NO] mutableCopy];
            for(Produkt* produkt in tempArray){
                [_products addObject:produkt];
            }
        }
        [self.collectionView scrollsToTop];
        [self.collectionView reloadData];
    }
    else {
        [self filterDataWithType:filterType query:[userInfo objectForKey:@"query"]];
        if([[userInfo objectForKey:@"query"] isEqualToString:@"all"] || [[userInfo objectForKey:@"query"] isEqualToString:@"isNew"]) [_segmentedControl setSelectedSegmentIndex:0];
        else if([[userInfo objectForKey:@"query"] isEqualToString:@"isBestseller"]) [_segmentedControl setSelectedSegmentIndex:1];
        else if([[userInfo objectForKey:@"query"] isEqualToString:@"isPromotion"]) [_segmentedControl setSelectedSegmentIndex:2];
    }
    
}
#pragma mark - SEGMENTED BAR

- (IBAction)segmentedBarChanged:(id)sender {
    
    if(_segmentedControl.selectedSegmentIndex == 0) {
        [self filterDataWithType:@"type" query:@"all"];
    }else if(_segmentedControl.selectedSegmentIndex == 1){
        [self filterDataWithType:@"type" query:@"isBestseller"];
    }else if(_segmentedControl.selectedSegmentIndex == 2){
        [self filterDataWithType:@"type" query:@"isPromotion"];
    }
}
#pragma mark - ACTION SHEET
-(IBAction)showActionSheet:(id)sender {
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Sortowanie" delegate:self cancelButtonTitle:@"Anuluj" destructiveButtonTitle:nil otherButtonTitles:@"Nazwa A-Z", @"Nazwa Z-A", @"Cena rosnąco", @"Cena malejąco", nil];
    popupQuery.actionSheetStyle = UIActionSheetStyleDefault;
    [popupQuery showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSMutableArray*productsToSort = [_products mutableCopy];
    if (buttonIndex == 0) {
        _products = [[productsToSort sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Produkt*)a name];
            NSString *second = [(Produkt*)b name];
            return [first compare:second];
        }] mutableCopy];
        [self.collectionView reloadData];
        [self.collectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    } else if (buttonIndex == 1) {
        _products = [[productsToSort sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(Produkt*)a name];
            NSString *second = [(Produkt*)b name];
            return [first compare:second];
        }] mutableCopy];
        _products = [[[_products reverseObjectEnumerator] allObjects] mutableCopy];
        [self.collectionView reloadData];
        [self.collectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        
    } else if (buttonIndex == 2) {
        _products = [[productsToSort sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSNumber *first = [(Produkt*)a price];
            NSNumber *second = [(Produkt*)b price];
            return [first compare:second];
        }] mutableCopy];
        
        [self.collectionView reloadData];
        [self.collectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        
    } else if (buttonIndex == 3) {
        _products = [[productsToSort sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSNumber *first = [(Produkt*)a price];
            NSNumber *second = [(Produkt*)b price];
            return [first compare:second];
        }] mutableCopy];
        _products = [[[_products reverseObjectEnumerator] allObjects] mutableCopy];
        [self.collectionView reloadData];
        [self.collectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
}
#pragma mark - Modal Views
- (void)goToMainView{
    [self filterDataWithType:@"type" query:@"all"];
    [_segmentedControl setSelectedSegmentIndex:0];
}
- (void)goToBasket{
    [_navController goToBasketAnimated:YES];
}
- (void)goToFavorites{
    [_navController goToFavoritesAnimated:YES];
}
- (void)openMenu{
    [_navController openMenu];
}
- (IBAction)toggleMenu{
    [self openMenu];
}
@end
