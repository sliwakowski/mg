//
//  mgReviewCellView.h
//  aplikacja
//
//  Created by Macbook Pro on 04.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Komentarz.h"

@interface mgReviewCellView : UIView

@property (nonatomic, strong) Komentarz* comment;
@property (nonatomic, strong) UILabel* nameLabel;
@property (nonatomic, strong) UILabel* descriptionLabel;
@property (nonatomic, assign) float cellHeight;
- (id)initWithComment:(Komentarz*)comment atPosition:(float)position;

@end
