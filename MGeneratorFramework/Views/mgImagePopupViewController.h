//
//  mgImagePopupViewController.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 27.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mgImagePopupViewController : UIViewController

@property (nonatomic, strong) UIImage* image;
@property (nonatomic, strong) UIImageView* imageView;

@end
