//
//  mgMapViewController.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 14.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgMapViewController.h"
#import "mgTextHelper.h"
@interface mgMapViewController ()

@end

@implementation mgMapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_prowadzButton setTintColor:[UIColor lightGrayColor]];
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    _navController = (mgNavigationController*)self.navigationController;
    _annotationsArray = [NSMutableArray new];
    _distanceArray = [NSMutableArray new];
    _locationsArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"locationsArray"]];
    _locationManager = [[CLLocationManager alloc] init];
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.delegate = self;
    [_locationManager startUpdatingLocation];
    [mgJSONHandler synchronizeAddresses:^(NSArray *array) {
        _locationsArray = [array mutableCopy];
        [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"locationsArray"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self placeAnnotations];
    }];
    [self placeAnnotations];
}
- (void)viewWillAppear:(BOOL)animated {
    
    [[self navigationController] setToolbarHidden:NO animated:YES];
    UIInterfaceOrientation oldOrientation = _orientation;
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(oldOrientation!=_orientation){
        [_navController popViewControllerAnimated:NO];
        [_navController goToPage:@"mapVC" animated:NO];
    }

    //NAVIGATION BAR
    mgNavigationBarHandler* navigationBarHandler = [mgNavigationBarHandler new];
    navigationBarHandler.delegate = self;
    [navigationBarHandler configureNavigationBar:self.navigationController];
    _favoritesButton = [navigationBarHandler favoritesButton];
    [_favoritesButton addTarget:self action:@selector(goToFavorites) forControlEvents:UIControlEventTouchUpInside];
    _basketButton = [navigationBarHandler basketButton];
    [_basketButton addTarget:self action:@selector(goToBasket) forControlEvents:UIControlEventTouchUpInside];
    _titleLabel = [navigationBarHandler titleLabel:@"Mapa"];
}
- (void)viewWillDisappear:(BOOL)animated {
    [_favoritesButton removeFromSuperview];
    [_titleLabel removeFromSuperview];
    [_basketButton removeFromSuperview];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewWillDisappear:YES];
    [self viewWillAppear:YES];
}
- (void)placeAnnotations{
    [_mapView removeAnnotations:_annotationsArray];
    for (NSDictionary*locationObject in _locationsArray) {
        double latitude = [[locationObject objectForKey:@"lat"] doubleValue];
        double longitude = [[locationObject objectForKey:@"lan"] doubleValue];
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(latitude, longitude);
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        annotation.coordinate = coord;
        annotation.title = [locationObject objectForKey:@"title"];
        annotation.subtitle = [locationObject objectForKey:@"description"];
        [_annotationsArray addObject:annotation];
    }
    [_mapView addAnnotations:_annotationsArray];
    [_tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_locationsArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"mapElement";
    int arrayElementIndex = (int)indexPath.row;
    if([_distanceArray count]==[_locationsArray count]) {
        arrayElementIndex = [[[_distanceArray objectAtIndex:indexPath.row] objectForKey:@"locationItem"] intValue];
    }
    NSDictionary*locationObject = [_locationsArray objectAtIndex:arrayElementIndex];
    double latitude = [[locationObject objectForKey:@"lat"] doubleValue];
    double longitude = [[locationObject objectForKey:@"lan"] doubleValue];
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLLocationDistance distanceBetween = [_currentLocation distanceFromLocation:locA];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:identifier];
    }
    UILabel* titleLabel = (UILabel*) [cell viewWithTag:1];
    titleLabel.text = [locationObject objectForKey:@"title"];
    UILabel* addressLabel = (UILabel*) [cell viewWithTag:2];
    addressLabel.text = [locationObject objectForKey:@"address"];
    UILabel* distanceLabel = (UILabel*) [cell viewWithTag:3];
    if(distanceBetween/1000 > 1) distanceLabel.text = [[NSString alloc] initWithFormat:@"%.1fkm", distanceBetween/1000];
    else distanceLabel.text = [[NSString alloc] initWithFormat:@"%.1fm", distanceBetween];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_prowadzButton setTintColor:[mgTextHelper mainColor]];
    int arrayElementIndex = (int)indexPath.row;
    if([_distanceArray count]==[_locationsArray count]) {
        arrayElementIndex = [[[_distanceArray objectAtIndex:indexPath.row] objectForKey:@"locationItem"] intValue];
    }
    
    NSDictionary*locationObject = [_locationsArray objectAtIndex:arrayElementIndex];
    _selectedLocationObject = locationObject;
    double latitude = [[locationObject objectForKey:@"lat"] doubleValue];
    double longitude = [[locationObject objectForKey:@"lan"] doubleValue];
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(latitude, longitude);
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coord, 50*1000, 50*1000);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    [self.mapView setRegion:adjustedRegion animated:YES];
    self.mapView.showsUserLocation = YES;
}
-(void)locationManager:(CLLocationManager *)manager
   didUpdateToLocation:(CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
    if(!oldLocation){
        CLLocationCoordinate2D noLocation = newLocation.coordinate;
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(noLocation, 500*1000, 500*1000);
        MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
        [self.mapView setRegion:adjustedRegion animated:YES];
        self.mapView.showsUserLocation = YES;
    }
    
    
    
    _distanceArray = [NSMutableArray new];
    _currentLocation = newLocation;
    for (int i=0; i<[_locationsArray count]; i++) {
        NSDictionary*locationObject = [_locationsArray objectAtIndex:i];
        double latitude = [[locationObject objectForKey:@"lat"] doubleValue];
        double longitude = [[locationObject objectForKey:@"lan"] doubleValue];
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        CLLocationDistance distanceBetween = [_currentLocation distanceFromLocation:locA];
        NSMutableDictionary* singleDistanceDictionary = [NSMutableDictionary new];
        [singleDistanceDictionary setObject:[NSNumber numberWithFloat:distanceBetween] forKey:@"distanceBetween"];
        [singleDistanceDictionary setObject:[NSNumber numberWithInt:i] forKey:@"locationItem"];
        [_distanceArray addObject:singleDistanceDictionary];
    }
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distanceBetween" ascending:YES];
    NSArray* sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [_distanceArray sortedArrayUsingDescriptors:sortDescriptors];
    _distanceArray =[NSMutableArray arrayWithArray:sortedArray];
    [_tableView reloadData];
}
-(IBAction)navigateToPlace:(id)sender{
    if(_selectedLocationObject){
        double latitude = [[_selectedLocationObject objectForKey:@"lat"] doubleValue];
        double longitude = [[_selectedLocationObject objectForKey:@"lan"] doubleValue];
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(latitude, longitude);
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coord addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:[_selectedLocationObject objectForKey:@"title"]];
        // Pass the map item to the Maps app
        [mapItem openInMapsWithLaunchOptions:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Modal Views
- (void)goToMainView{
    [_navController goToMainView];
}
- (void)goToBasket{
    [_navController goToBasketAnimated:YES];
}
- (void)goToFavorites{
    [_navController goToFavoritesAnimated:YES];
}
- (void)openMenu{
    [_navController openMenu];
}


@end
