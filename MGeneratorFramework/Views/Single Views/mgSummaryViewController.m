//
//  mgSummaryViewController.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 31.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgSummaryViewController.h"
#import "mgBasketViewController.h"
#import "MBProgressHUD.h"
#import <MagicalRecord/MagicalRecord.h>
@interface mgSummaryViewController ()

@end

@implementation mgSummaryViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupDesign];
    
    NSString* fullUrl = [NSString stringWithFormat:@"%@%@/%@", [mgTextHelper getSimpleValueForKeyFromConfiguration:@"order_summary"], [mgTextHelper shopId], _orderId];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:fullUrl]];
    [_webView loadRequest:urlRequest];
    NSDictionary* p24 = _request[@"p24"];
    NSNumber* status = p24[@"status"];
    if([status isEqualToNumber:@0]) {
        [_button setTitle:@"Zakończ zamówienie" forState:UIControlStateNormal];
        [_button layoutIfNeeded];
        [_button addTarget:self action:@selector(cancelPayment) forControlEvents:UIControlEventTouchDown];
    }
}
- (void)setupDesign
{
    if(!IsIphone5 && IDIOM!=IPAD) {
        CGRect webFrame = _webView.frame;
        webFrame.size.height = 426;
        _webView.frame = webFrame;
        [_webView setNeedsLayout];
        
        CGRect buttonFrame = _button.frame;
        buttonFrame.origin.y = buttonFrame.origin.y-80;
        _button.frame = buttonFrame;
        [_button setNeedsLayout];
    }
}
-(IBAction)goToPayment:(id)sender{
    [self clearBasket];
    [self.navigationController popViewControllerAnimated:YES];
    mgBasketViewController* basketVC = (mgBasketViewController*) _basketVC;
    [basketVC startPaymentWithData:_request];
}
-(IBAction)cancelPayment{
    [self clearBasket];
    
    @try {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"Wysłano zamówienie";
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:1];

    }
    @catch (NSException *exception) {
        
    }
    
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)clearBasket{
    NSNumber* ulubione = [NSNumber numberWithBool:NO];
    NSArray* tempKoszyk = (NSArray*) [Koszyk MR_findByAttribute:@"ulubione" withValue:ulubione];
    for(Koszyk* koszyk in tempKoszyk) {
        [koszyk MR_deleteEntity];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
