//
//  mgAccountTableViewController.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 30.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgNavigationController.h"
#import "MBProgressHUD.h"
#import "mgPOSTHandler.h"

@interface mgAccountTableViewController : UITableViewController <UITextFieldDelegate>

@property (nonatomic, strong) mgNavigationController* navController;
@property (nonatomic, assign) BOOL accountDataChanged;
@property (nonatomic, strong) UIBarButtonItem* doneButton;

@property (nonatomic, weak) IBOutlet UITextField* imie;
@property (nonatomic, weak) IBOutlet UITextField* nazwisko;
@property (nonatomic, weak) IBOutlet UITextField* telefon;
@property (nonatomic, weak) IBOutlet UITextField* email;

@property (nonatomic, weak) IBOutlet UITextField* ulica;
@property (nonatomic, weak) IBOutlet UITextField* miasto;
@property (nonatomic, weak) IBOutlet UITextField* kod;
@property (nonatomic, weak) IBOutlet UITextField* kraj;

@property (nonatomic, assign) BOOL fvActive;
@property (nonatomic, weak) IBOutlet UISwitch* fv;
@property (nonatomic, weak) IBOutlet UITextField* fvNazwa;
@property (nonatomic, weak) IBOutlet UITextField* fvNIP;
@property (nonatomic, weak) IBOutlet UITextField* fvUlica;
@property (nonatomic, weak) IBOutlet UITextField* fvMiasto;
@property (nonatomic, weak) IBOutlet UITextField* fvKod;
@property (nonatomic, weak) IBOutlet UITextField* fvKraj;

@end
