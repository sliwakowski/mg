//
//  mgAccountTableViewController.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 30.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgAccountTableViewController.h"
#import "MBProgressHUD.h"

@interface mgAccountTableViewController ()

@end

@implementation mgAccountTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self fvSwitchChanged:_fv];
    [self setUserInfo];
    _navController = (mgNavigationController*) self.navigationController;
    _doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Zapisz" style:UIBarButtonItemStyleDone target:nil action:@selector(updateUserInfo:)];
}
-(void)viewDidAppear:(BOOL)animated {
    [[self navigationController] setToolbarHidden:YES animated:YES];
}
- (void)didReceiveMemoryWarning { [super didReceiveMemoryWarning];}
- (void)setUserInfo{
    NSDictionary* userObject = [UserDefaults objectForKey:@"user"];
    NSDictionary* deliveryAddress = [userObject objectForKey:@"delivery_address"];
    _imie.text = [[[deliveryAddress objectForKey:@"name"] componentsSeparatedByString:@" "] objectAtIndex:0];
    _nazwisko.text = [[[deliveryAddress objectForKey:@"name"] componentsSeparatedByString:@" "] objectAtIndex:1];
    _telefon.text = [deliveryAddress objectForKey:@"phone"];
    _email.text = [deliveryAddress objectForKey:@"email"];
    _ulica.text = [deliveryAddress objectForKey:@"address"];
    _miasto.text = [deliveryAddress objectForKey:@"city"];
    _kraj.text = [deliveryAddress objectForKey:@"country"];
    _kod.text = [deliveryAddress objectForKey:@"post_code"];
    
    NSDictionary* invoiceAddress = [userObject objectForKey:@"invoice_address"];
    _fvNazwa.text = [invoiceAddress objectForKey:@"name"];
    _fvNIP.text = [invoiceAddress objectForKey:@"nip"];
    _fvUlica.text = [invoiceAddress objectForKey:@"address"];
    _fvMiasto.text = [invoiceAddress objectForKey:@"city"];
    _fvKod.text = [invoiceAddress objectForKey:@"post_code"];
    _fvKraj.text = [invoiceAddress objectForKey:@"country"];
    for(NSString* invoiceKey in [invoiceAddress allKeys]) {
        if([invoiceAddress[invoiceKey] isKindOfClass:[NSString class]]) {
            if([invoiceAddress[invoiceKey] length]>0) {
                [_fv setOn:YES];
                [self fvSwitchChanged:_fv];
            }
        }
    }
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.navigationItem.rightBarButtonItem = _doneButton;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    _accountDataChanged = YES;
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
    if(!_accountDataChanged){
        self.navigationItem.rightBarButtonItem = nil;
    }
    
	return YES;
}
- (IBAction)fvSwitchChanged:(UISwitch *)theSwitch {
    _fvActive = theSwitch.on;
    float nonActiveOpacity = 0.2;
    
    [[_fvNazwa superview] setAlpha: (_fvActive ? 1 : nonActiveOpacity)];
    [[_fvNIP superview] setAlpha: (_fvActive ? 1 : nonActiveOpacity)];
    [[_fvUlica superview] setAlpha: (_fvActive ? 1 : nonActiveOpacity)];
    [[_fvMiasto superview] setAlpha: (_fvActive ? 1 : nonActiveOpacity)];
    [[_fvKraj superview] setAlpha: (_fvActive ? 1 : nonActiveOpacity)];
    [[_fvKod superview] setAlpha: (_fvActive ? 1 : nonActiveOpacity)];
}
- (IBAction)logout:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"user"];
    [defaults synchronize];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_navController.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = @"Pomyślnie wylogowano";
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:1];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshMenu" object:nil userInfo:nil];
    //[_navController.menuContainerViewController toggleLeftSideMenuCompletion:^{ }];
    [_navController goToMainView];
}
- (IBAction)updateUserInfo:(id)sender{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSDictionary* deliveryAddress = @{@"name": [NSString stringWithFormat:@"%@ %@", _imie.text, _nazwisko.text], @"address": _ulica.text, @"city":_miasto.text, @"post_code":_kod.text, @"phone": _telefon.text};
    
    NSDictionary* invoiceAddress = @{@"name": _fvNazwa.text, @"address": _fvUlica.text, @"city":_fvMiasto.text, @"country": _fvKraj.text, @"post_code":_fvKod.text, @"nip":_fvNIP.text};
    NSDictionary* fullData = @{@"email": _email.text, @"delivery_address":deliveryAddress, @"invoice_address":invoiceAddress};
    

    [mgPOSTHandler registerUserWith:fullData update:YES completionHandler:^(NSDictionary *response) {
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if(response) {
            [self setUserInfo];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = @"Zaktualizowano dane";
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:1];
            [self.view endEditing:YES];
        }
        
    }];
}

@end
