//
//  mgSummaryViewController.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 31.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgTextHelper.h"
@interface mgSummaryViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIWebView* webView;
@property (nonatomic, strong) IBOutlet UIButton* button;

@property (nonatomic, strong) NSString* orderId;
@property (nonatomic, strong) NSDictionary* request;
@property (nonatomic, strong) UIViewController* basketVC;

@end
