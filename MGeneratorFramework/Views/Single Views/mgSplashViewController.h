//
//  mgSplashViewController.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 07.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mgSplashViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIImageView* splashImageView;

@end
