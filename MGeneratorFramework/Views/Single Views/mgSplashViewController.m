//
//  mgSplashViewController.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 07.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgSplashViewController.h"
#import "UIImageView+WebCache.h"

@interface mgSplashViewController ()

@end

@implementation mgSplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSDictionary* config = [[NSUserDefaults standardUserDefaults] objectForKey:@"config"];
    NSString* splashUrl = config[@"splash"];
    [self.splashImageView sd_setImageWithURL:[NSURL URLWithString:splashUrl] placeholderImage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
