//
//  mgTableViewCell.m
//  aplikacja
//
//  Created by Macbook Pro on 01.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgTableViewCell.h"

@implementation mgTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void) setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    UILabel *label1 = (UILabel *)[self viewWithTag:4]; // whatever you tag the label as
    label1.hidden = editing;
    UILabel *label2 = (UILabel *)[self viewWithTag:5]; // whatever you tag the label as
    label2.hidden = editing;
}

@end
