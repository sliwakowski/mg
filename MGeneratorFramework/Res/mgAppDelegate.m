//#import <UIKit/UIKit.h>
//#import "AppDelegate.h"
//#import "mgAppDelegate.h"
//int main(int argc, char * argv[]) {
//    @autoreleasepool {
//        return UIApplicationMain(argc, argv, nil, NSStringFromClass([mgAppDelegate class]));
//    }
//}



//
//  mgAppDelegate.m
//  mGenerator
//
//  Created by Macbook Pro on 25.02.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgAppDelegate.h"
#import "MFSideMenu.h"
#import "mgMenuViewController.h"
#import "mgNavigationController.h"
#import "Produkt.h"
#import "Komentarz.h"
#import "Dostawa.h"
#import "mgTextHelper.h"
#import "mgJSONHandler.h"

@implementation mgAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [MagicalRecord setupAutoMigratingCoreDataStack];
    if(Demo){
        [self cleanUp];
    }

    UIStoryboard *storyboard = nil;
    if ( IDIOM == IPAD ) {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:[NSBundle mainBundle]];
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
    }
    
    _navController = [storyboard instantiateViewControllerWithIdentifier:@"rootViewController"];
    mgMenuViewController *leftMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"menuViewController"];
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:_navController
                                                    leftMenuViewController:leftMenuViewController
                                                    rightMenuViewController:nil];
    leftMenuViewController.navigationController = _navController;
    self.window.rootViewController = container;
    [self.window makeKeyAndVisible];
    
    
    // Register to Push notification types
    if (isIOS8)
    {
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert |
         UIUserNotificationTypeBadge |
         UIUserNotificationTypeSound
                                          categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    // Override point for customization after application launch.
    //[[UIView appearance] setTintColor:[mgTextHelper mainColor]];
    self.window.tintColor = [mgTextHelper mainColor];
    
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotificationPayload) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"notification" object:nil userInfo:remoteNotificationPayload];
    }
    
    
    [mgJSONHandler checkAuthAppPreview:[mgTextHelper key] completionBlock:^(BOOL a) { }];
    return YES;
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	NSLog(@"My token is: %@", deviceToken);
    NSString* deviceTokenString = [NSString stringWithFormat:@"%@", deviceToken];
    deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@"<" withString:@""];
    deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@">" withString:@""];
    [UserDefaults setObject:@"deviceTokenString" forKey:deviceTokenString];
    [mgJSONHandler registerDevice:deviceTokenString];
    [UserDefaults synchronize];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    NSString* url = [[userInfo objectForKey:@"aps"] objectForKey:@"url"];
    if(url){
        [self handleScannerUrl:url];
    }
    NSLog(@"userinfo %@",userInfo);
    
}
- (void)handleScannerUrl:(NSString*)stringUrl{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSArray* urlComponents = [url pathComponents];
    NSLog(@"path components: %@", [url pathComponents]);
    
    NSString* shopId = [urlComponents objectAtIndex:1];
    NSString* action = [urlComponents objectAtIndex:2];
    NSString* parameter = [urlComponents objectAtIndex:3];
    if([[mgTextHelper shopId] isEqualToString:shopId]){
        if([action isEqualToString:@"openProduct"]){
            NSArray* produkty = [Produkt MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithLong:parameter.integerValue]];
            if([produkty count]==0) return;
            Produkt* produkt = [produkty objectAtIndex:0];
            if(produkt){
                [_navController goToProduct:produkt animated:YES];
            }
        }
        else if([action isEqualToString:@"openCategory"]){
            Kategoria* kategoria = [[Kategoria MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithLong:parameter.integerValue]] objectAtIndex:0];
            if(kategoria){
                [_navController goToCategory:kategoria];
            }
        }
    }
    
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    if(Demo) {
        [self cleanUp];
    }
}

- (void)cleanUp
{
    bool debug = NO;
    if(!debug)
    {
    NSArray* defaultsToClean = @[@"locationsArray", @"user", @"qrCodeHistory", @"colors", @"config", @"demoShopId", @"lastSynchronization", @"alreadyLogged"];
    for (NSString*defaultToClean in defaultsToClean) {
        [UserDefaults setObject:nil forKey:defaultToClean];
    }
    
    NSArray* entitiesArray = @[[Produkt MR_findAll], [Kategoria MR_findAll], [Strona MR_findAll], [Dostawa MR_findAll], [Koszyk MR_findAll], [Komentarz MR_findAll]];
    for (NSArray*objectsArray in entitiesArray) {
        for(NSManagedObject* managedObject in objectsArray)
        {
            [managedObject MR_deleteEntity];
        }
    }
        
    }
    
}

@end
