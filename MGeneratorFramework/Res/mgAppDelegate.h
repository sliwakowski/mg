//
//  mgAppDelegate.h
//  mGenerator
//
//  Created by Macbook Pro on 25.02.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mgNavigationController.h"
#import <MagicalRecord/MagicalRecord.h>
@interface mgAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) mgNavigationController* navController;

- (void)cleanUp;
@end
