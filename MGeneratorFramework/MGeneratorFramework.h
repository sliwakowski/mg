//
//  MGeneratorFramework.h
//  MGeneratorFramework
//
//  Created by Adam Śliwakowski on 21.05.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MGeneratorFramework.
FOUNDATION_EXPORT double MGeneratorFrameworkVersionNumber;

//! Project version string for MGeneratorFramework.
FOUNDATION_EXPORT const unsigned char MGeneratorFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MGeneratorFramework/PublicHeader.h>
#import "P24.h"

