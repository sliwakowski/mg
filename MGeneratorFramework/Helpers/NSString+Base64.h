//
//  NSString+Base64.h
//  mGenerator
//
//  Created by Adam Śliwakowski on 25.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Base64)

- (NSString*) base64;

@end
