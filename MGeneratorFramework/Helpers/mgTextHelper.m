//
//  mgTextHelper.m
//  mGenerator
//
//  Created by Macbook Pro on 25.02.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgTextHelper.h"
#import <QuartzCore/QuartzCore.h>
#import "NSString+MD5.h"
@implementation mgTextHelper

+(NSString*)appKey{
    NSString* keyBase = @"ilesztorobotywmgeneratorzezaplikacjami";
    NSDate *now = [NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *keyTime = [dateFormatter stringFromDate:now];
    NSString* keyFull = [NSString stringWithFormat:@"%@%@", keyBase, keyTime];
    NSString* keyMd5 = [keyFull MD5String];
    keyMd5 = [keyMd5 lowercaseString];
    return keyMd5;
}
+(NSString*)key{
    return [mgTextHelper getSimpleValueForKeyFromConfiguration:@"shop_key"];
}
+(NSString*)getValueForKeyFromConfiguration:(NSString*)key{
    NSBundle *bundle             = [NSBundle bundleForClass:[mgTextHelper class]];
    NSString *configurationPlist = [bundle pathForResource:@"Configuration" ofType:@"plist"];
    NSDictionary *configurationDictionary = [[NSDictionary alloc] initWithContentsOfFile:configurationPlist];
    
    return [NSString stringWithFormat:@"%@%@",configurationDictionary[@"json_base"],configurationDictionary[key]];
}
+(NSString*)getSimpleValueForKeyFromConfiguration:(NSString*)key{
    NSBundle *bundle             = [NSBundle bundleForClass:[mgTextHelper class]];
    NSString *configurationPlist = [bundle pathForResource:@"Configuration" ofType:@"plist"];
    NSDictionary *configurationDictionary = [[NSDictionary alloc] initWithContentsOfFile:configurationPlist];
    
    return [NSString stringWithFormat:@"%@",configurationDictionary[key]];
}
+(NSString*)shopId{
    NSString* shopId = @"";
    @try {
        shopId = [UserDefaults objectForKey:@"demoShopId"];
    }
    @catch (NSException *exception) { }
    if([shopId isKindOfClass:[NSNumber class]]){
        return shopId;
    }
    return shopId = [mgTextHelper getSimpleValueForKeyFromConfiguration:@"shop_id"];
}
+(NSString*)logo{
    NSString* logo = [[UserDefaults objectForKey:@"config"] objectForKey:@"logo"];
    return logo;
}
+(NSString*)client:(NSString*)field{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary* userObject = [defaults objectForKey:@"user"];
    NSDictionary* deliveryAddress = [userObject objectForKey:@"delivery_address"];
    if(![userObject isKindOfClass:[NSDictionary class]]){
        return @"";
    }
    else if([field isEqualToString:@"id"]){
        return [NSString stringWithFormat:@"%i", [(NSNumber*)userObject[@"id"] intValue]];
    }
    else return deliveryAddress[field];
}
+(UIColor*)mainColor{
    NSString* color = @"";
    @try {
        color = [[UserDefaults objectForKey:@"colors"] objectForKey:@"top_tabs_active"];
    }
    @catch (NSException *exception) { }
    if([color isEqualToString:@""] || !color) color = [mgTextHelper getSimpleValueForKeyFromConfiguration:@"base_color"];
    
    [UIApplication sharedApplication].delegate.window.tintColor = [mgTextHelper pxColorWithHexValue:color];
    return [mgTextHelper pxColorWithHexValue:color];
}
+(NSString*)isLoginFormValid:(NSString*)email password:(NSString*)password{
    NSString* errorMessage = @"";
    
    if(![mgTextHelper isEmailValid:email]
       && email.length<7) errorMessage = @"Wpisałeś niepoprawny email";
    return errorMessage;
}
+(NSString*)isRegistrationFormValid:(NSString*)imie_nazwisko haslo:(NSString*)haslo haslo2:(NSString*)haslo2 email:(NSString*)email ulica:(NSString*)ulica miasto:(NSString*)miasto kodpocztowy:(NSString*)kod_pocztowy{
    NSString* message = @"";
    
    //Email
    if(imie_nazwisko.length<1) message = @"Nie podałeś imienia i nazwiska";
    else if([imie_nazwisko rangeOfString:@" "].length<1) message = @"Nie podałeś imienia i nazwiska";
    else if(haslo.length<6) message = @"Hasło jest za krótkie";
    else if(haslo2.length<6) message = @"Nie powtórzyłeś hasła";
    else if(![haslo isEqualToString:haslo2]) message = @"Hasła nie są identyczne";
    else if(![mgTextHelper isEmailValid:email] || email.length<7) message = @"Wpisałeś niepoprawny email";
    else if(ulica.length<1) message = @"Nie podałeś ulicy";
    else if(miasto.length<1) message = @"Nie podałeś miasta";
    else if(kod_pocztowy.length<1) message = @"Nie podałeś kodu pocztowego";
    else if(![mgTextHelper isZipcodeValid:kod_pocztowy]) message = @"Nieprawidłowy kod pocztowy";
    
    return message;
}
+ (BOOL)isEmailValid:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
+ (BOOL)isZipcodeValid:(NSString*)zipcode{
    NSString *zipcodeRegex = @"[0-9]{2}-[0-9]{3}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", zipcodeRegex];
    BOOL matches = [test evaluateWithObject:zipcode];
    return matches;
}
+ (UIColor*)pxColorWithHexValue:(NSString*)hexValue
{
    //Default
    UIColor *defaultResult = [UIColor blackColor];
    
    //Strip prefixed # hash
    if ([hexValue hasPrefix:@"#"] && [hexValue length] > 1) {
        hexValue = [hexValue substringFromIndex:1];
    }
    
    //Determine if 3 or 6 digits
    NSUInteger componentLength = 0;
    if ([hexValue length] == 3)
    {
        componentLength = 1;
    }
    else if ([hexValue length] == 6)
    {
        componentLength = 2;
    }
    else
    {
        return defaultResult;
    }
    
    BOOL isValid = YES;
    CGFloat components[3];
    
    //Seperate the R,G,B values
    for (NSUInteger i = 0; i < 3; i++) {
        NSString *component = [hexValue substringWithRange:NSMakeRange(componentLength * i, componentLength)];
        if (componentLength == 1) {
            component = [component stringByAppendingString:component];
        }
        
        NSScanner *scanner = [NSScanner scannerWithString:component];
        unsigned int value;
        isValid &= [scanner scanHexInt:&value];
        components[i] = (CGFloat)value / 255.0f;
    }
    
    if (!isValid) {
        return defaultResult;
    }
    
    return [UIColor colorWithRed:components[0]
                           green:components[1]
                            blue:components[2]
                           alpha:1.0];
}
+(NSString*)lastSynchronizationForType:(NSString*)type set:(BOOL)set{
    NSString*currentTimestamp = [NSString stringWithFormat:@"%0.f", [[NSDate date] timeIntervalSince1970]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary*synchronizationsDictionary = [[defaults objectForKey:@"lastSynchronization"] mutableCopy];
    if(synchronizationsDictionary==nil){
        synchronizationsDictionary = [NSMutableDictionary new];
    }
    NSString* lastSynchronization = synchronizationsDictionary[type];
    synchronizationsDictionary[type]=currentTimestamp;
    if(set)
    {
        [defaults setObject:synchronizationsDictionary forKey:@"lastSynchronization"];
        [defaults synchronize];
    }
    if(lastSynchronization==nil){
        return @"0";
    }
    else {
        return lastSynchronization;
    }
}
#pragma mark - PRICES
+ (void)configureNewPriceLabel:(UILabel*)newPriceLabel withProdukt:(Produkt*)produkt{
    NSString* newPriceString = [NSString stringWithFormat:@"%@ zł", produkt.promotionPrice];
    if(![produkt.isPromotion boolValue]){
        newPriceString = [NSString stringWithFormat:@"%@ zł", produkt.price];
    }
    newPriceString = [newPriceString stringByReplacingOccurrencesOfString:@"." withString:@","];
    newPriceLabel.text = newPriceString;
    newPriceLabel.textColor = [mgTextHelper mainColor];
    [newPriceLabel sizeToFit];
}
+ (void)configureOldPriceLabel:(UILabel*)oldPriceLabel withNewPriceLabel:(UILabel*)newPriceLabel withProdukt:(Produkt*)produkt{
    NSString* oldPriceString = [NSString stringWithFormat:@"%@ zł", produkt.price];
    if(![produkt.isPromotion boolValue]){
        oldPriceString = @"";
    }
    oldPriceString = [oldPriceString stringByReplacingOccurrencesOfString:@"." withString:@","];
    oldPriceLabel.text = oldPriceString;
    [oldPriceLabel sizeToFit];
    CGRect newOldPriceLabelFrame = oldPriceLabel.frame;
    newOldPriceLabelFrame.origin.x = [newPriceLabel superview].frame.size.width - oldPriceLabel.frame.size.width - 5;
    oldPriceLabel.frame = newOldPriceLabelFrame;
    CALayer *line = [CALayer layer];
    line.frame = CGRectMake(0, oldPriceLabel.frame.size.height/2, oldPriceLabel.frame.size.width, 1);
    line.backgroundColor = [UIColor colorWithWhite:0.6f alpha:1.0f].CGColor;
    [oldPriceLabel.layer addSublayer:line];
}
#pragma mark - ORIENTATION
+ (BOOL)isLandscape{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(orientation==UIInterfaceOrientationLandscapeRight || orientation==UIInterfaceOrientationLandscapeLeft){
        return YES;
    }
    return NO;
}
@end
