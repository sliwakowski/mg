//
//  NSString+MD5.h
//  mGenerator
//
//  Created by Macbook Pro on 27.02.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5String;

@end
