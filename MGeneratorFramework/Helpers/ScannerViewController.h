//
//  ViewController.h
//  iOS7_BarcodeScanner
//
//  Created by Jake Widmer on 11/16/13.
//  Copyright (c) 2013 Jake Widmer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsViewController.h"
#import "Barcode.h"

typedef void (^ScanResultBlock)(Barcode *scanResult);
typedef void (^ScanErrorBlock)(NSError *error);
typedef void (^ScanCancelBlock)();

@interface ScannerViewController : UIViewController<UIAlertViewDelegate, SettingsDelegate>
@property (strong, nonatomic) NSMutableArray * allowedBarcodeTypes;


@property (nonatomic, copy) ScanResultBlock resultBlock;
@property (nonatomic, copy) ScanErrorBlock errorBlock;
@property (nonatomic, copy) ScanCancelBlock cancelBlock;

@end
