//
//  NSString+Base64.m
//  mGenerator
//
//  Created by Adam Śliwakowski on 25.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "NSString+Base64.h"

@implementation NSString (Base64)

- (NSString*)base64 {
    NSData *plainData = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [plainData base64EncodedStringWithOptions:0];
    return base64String;
}

@end
