//
//  mgNavigationBarHandler.h
//  aplikacja
//
//  Created by Macbook Pro on 30.05.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol mgNavigationBarHandlerDelegate <NSObject>
@required
- (void)goToMainView;
- (void)goToBasket;
- (void)goToFavorites;
- (void)openMenu;

@end

@interface mgNavigationBarHandler : NSObject

- (void)configureNavigationBar:(UINavigationController*) navigationController;
- (UIButton*)homeButton;
- (UIButton*)favoritesButton;
- (UIButton*)basketButton;
- (UILabel*)titleLabel:(NSString*) title;
- (UIButton*)rightButtonWithTitle:(NSString*) title;
@property (nonatomic, strong) id<mgNavigationBarHandlerDelegate>            delegate;
@property (nonatomic, strong) UINavigationController*                       navigationController;
@property (nonatomic, strong) UINavigationBar*                              navigationBar;
@property (nonatomic, assign) UIInterfaceOrientation                        orientation;
@property (nonatomic, assign) float                                         screenWidth;
@property (nonatomic, strong) UIButton*                                     homeButton;
@property (nonatomic, strong) UIButton*                                     favoritesButton;
@property (nonatomic, strong) UIButton*                                     basketButton;

@end
