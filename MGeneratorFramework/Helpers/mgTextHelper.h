//
//  mgTextHelper.h
//  mGenerator
//
//  Created by Macbook Pro on 25.02.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Produkt.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
@interface mgTextHelper : NSObject

+(NSString*)appKey;
+(NSString*)key;
+(NSString*)getValueForKeyFromConfiguration:(NSString*)key;
+(NSString*)getSimpleValueForKeyFromConfiguration:(NSString*)key;
+(NSString*)shopId;
+(NSString*)logo;
+(NSString*)client:(NSString*)field;
+(UIColor*)mainColor;
+(NSString*)isLoginFormValid:(NSString*)email password:(NSString*)password;
+(NSString*)isRegistrationFormValid:(NSString*)imie_nazwisko haslo:(NSString*)haslo haslo2:(NSString*)haslo2 email:(NSString*)email ulica:(NSString*)ulica miasto:(NSString*)miasto kodpocztowy:(NSString*)kod_pocztowy;
+ (UIColor*)pxColorWithHexValue:(NSString*)hexValue;
+ (NSString*)lastSynchronizationForType:(NSString*)type set:(BOOL)set;
+ (void)configureNewPriceLabel:(UILabel*)newPriceLabel withProdukt:(Produkt*)produkt;
+ (void)configureOldPriceLabel:(UILabel*)oldPriceLabel withNewPriceLabel:(UILabel*)newPriceLabel withProdukt:(Produkt*)produkt;
+ (BOOL)isLandscape;
@end
