//
//  mgNavigationBarHandler.m
//  aplikacja
//
//  Created by Macbook Pro on 30.05.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgNavigationBarHandler.h"
#import "Koszyk.h"
#import "mgTextHelper.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
#import <MagicalRecord/MagicalRecord.h>

static int const kLogoTag = 100;
static int const kFavoritesTag = 101;
static int const kBasketTag = 102;
static int const kTitleTag = 103;
static int const kTitleButtonTag = 104;
@implementation mgNavigationBarHandler

- (void)configureNavigationBar:(UINavigationController*) navigationController {
    _navigationController = navigationController;
    _navigationBar = _navigationController.navigationBar;
    _orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    if(_orientation == 0 || _orientation == UIInterfaceOrientationPortrait || _orientation==UIInterfaceOrientationPortraitUpsideDown) {
        _screenWidth = IDIOM==IPAD ? 768 : 320;
    }
    else {
        _screenWidth = 1024;
    }
}
- (UIButton*)buttonWithImage:(NSString*)image tag:(int)tag holderFrame:(CGRect)holderFrame imageFrame:(CGRect)imageFrame setTint:(BOOL)setTint{
    UIView *removeView;
    while((removeView = [_navigationBar viewWithTag:tag]) != nil) {
        [removeView removeFromSuperview];
    }
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = holderFrame;
    button.tag = tag;
    UIImage*buttonImage = [UIImage imageNamed:image];
    if(setTint){
        buttonImage = [buttonImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    [button setImage:buttonImage forState:UIControlStateNormal];
    [_navigationBar addSubview:button];
    button.userInteractionEnabled = YES;
    return button;

}
- (UIButton*)buttonWithImageFromUrl:(NSString*)image tag:(int)tag holderFrame:(CGRect)holderFrame imageFrame:(CGRect)imageFrame setTint:(BOOL)setTint{
    UIView *removeView;
    while((removeView = [_navigationBar viewWithTag:tag]) != nil) {
        [removeView removeFromSuperview];
    }
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = holderFrame;
    button.tag = tag;
    [button sd_setImageWithURL:[NSURL URLWithString:image] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"logo_m"]];
    [_navigationBar addSubview:button];
    button.userInteractionEnabled = YES;
    return button;
}
- (UILabel*)labelWithTitle:(NSString*)title tag:(int)tag holderFrame:(CGRect)holderFrame{
    UIView *removeView;
    while((removeView = [_navigationBar viewWithTag:tag]) != nil) {
        [removeView removeFromSuperview];
    }
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:holderFrame];
    titleLabel.text = title;
    titleLabel.tag = tag;
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [_navigationBar addSubview:titleLabel];
    return titleLabel;
}
- (UIButton*)buttonWithTitle:(NSString*)title tag:(int)tag holderFrame:(CGRect)holderFrame {
    UIView *removeView;
    while((removeView = [_navigationBar viewWithTag:tag]) != nil) {
        [removeView removeFromSuperview];
    }
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.titleLabel.textColor = [UIColor colorWithWhite:0.5 alpha:1];
    [button setTintColor:[UIColor colorWithWhite:0.5 alpha:1]];
    [button setTitleColor:[UIColor colorWithWhite:0.5 alpha:1] forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
    button.frame = holderFrame;
    button.tag = tag;
    [button setTitle:title forState:UIControlStateNormal];
    [_navigationBar addSubview:button];
    button.userInteractionEnabled = YES;
    return button;
    
}
- (UIButton*)homeButton {
    _homeButton = [self buttonWithImageFromUrl:[mgTextHelper logo] tag:kLogoTag holderFrame:CGRectMake(_screenWidth-448, 0, 130, 45) imageFrame:CGRectMake(30, 15, 70, 15) setTint:NO];
    [_homeButton setCenter:CGPointMake(_screenWidth/2, _homeButton.center.y)];
    return _homeButton;
}
- (UIButton*)favoritesButton {
    if(IDIOM==IPAD){
        _favoritesButton = [self buttonWithImage:@"heart.png" tag:kFavoritesTag holderFrame:CGRectMake(_screenWidth-105, 0, 33, 45) imageFrame:CGRectMake(5, 13, 22, 22) setTint:YES];
    }
    else {
       _favoritesButton = [self buttonWithImage:@"heart.png" tag:kFavoritesTag holderFrame:CGRectMake(_screenWidth-87, 0, 33, 45) imageFrame:CGRectMake(5, 13, 22, 22) setTint:YES];
    }
    return _favoritesButton;
}
- (UIButton*)basketButton {
    CGRect basketButtonFrame = IDIOM==IPAD ? CGRectMake(_screenWidth-53, 0, 33, 45) : CGRectMake(_screenWidth-45, 0, 33, 45);
    
    _basketButton = [self buttonWithImage:@"cart.png" tag:kBasketTag holderFrame:basketButtonFrame imageFrame:CGRectMake(5, 8, 22, 22) setTint:YES];
    UILabel* itemsNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(17, 11, 13, 10)];
    itemsNumberLabel.font = [ UIFont fontWithName:@"HelveticaNeue-Bold" size:10.0f];
    itemsNumberLabel.textColor = [UIColor whiteColor];
    itemsNumberLabel.text = @"0";
    itemsNumberLabel.textAlignment=NSTextAlignmentCenter;
    NSNumber* ulubione = [NSNumber numberWithBool:NO];
    int itemsNumber = 0;
    NSArray* koszykArray = (NSArray*) [Koszyk MR_findByAttribute:@"ulubione" withValue:ulubione];
    for(Koszyk* koszyk in koszykArray){
        itemsNumber+=[koszyk.ilosc intValue];
    }
    itemsNumberLabel.text = [NSString stringWithFormat:@"%i", itemsNumber];
    if(itemsNumber>9) {
        itemsNumberLabel.text = @"9+";
    }
    itemsNumberLabel.userInteractionEnabled  = NO;
    [_basketButton addSubview:itemsNumberLabel];
    
    return _basketButton;
}
- (UILabel*)titleLabel:(NSString*) title{
    UILabel* titleLabel = [self labelWithTitle:title tag:kTitleTag holderFrame:CGRectMake(_screenWidth-448, 0, 130, 45)];
    [titleLabel setCenter:CGPointMake(_screenWidth/2, titleLabel.center.y)];
    return titleLabel;
}
- (UIButton*)rightButtonWithTitle:(NSString*) title{
    if(IDIOM==IPAD){
        return [self buttonWithTitle:title tag:kTitleButtonTag holderFrame:CGRectMake(_screenWidth-105, 0, 90, 45)];
    }
    return [self buttonWithTitle:title tag:kTitleButtonTag holderFrame:CGRectMake(_screenWidth-87, 0, 90, 45)];
}
@end
