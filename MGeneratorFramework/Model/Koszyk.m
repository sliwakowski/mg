//
//  Koszyk.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 06.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "Koszyk.h"


@implementation Koszyk

@dynamic attributes;
@dynamic identyfikator;
@dynamic ilosc;
@dynamic timestamp;
@dynamic ulubione;

@end
