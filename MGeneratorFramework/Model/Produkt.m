//
//  Produkt.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 06.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "Produkt.h"
#import "Kategoria.h"


@implementation Produkt

@dynamic attributes;
@dynamic barcode;
@dynamic categoryId;
@dynamic details;
@dynamic disable;
@dynamic identyfikator;
@dynamic images;
@dynamic isBestseller;
@dynamic isNew;
@dynamic isPromotion;
@dynamic mainImage;
@dynamic name;
@dynamic price;
@dynamic promotionPrice;
@dynamic shopId;
@dynamic text;
@dynamic categories;

@end
