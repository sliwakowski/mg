//
//  Dostawa.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 06.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "Dostawa.h"


@implementation Dostawa

@dynamic detail;
@dynamic identyfikator;
@dynamic name;
@dynamic price;

@end
