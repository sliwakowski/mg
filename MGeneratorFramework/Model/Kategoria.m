//
//  Kategoria.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 06.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "Kategoria.h"
#import "Produkt.h"


@implementation Kategoria

@dynamic disable;
@dynamic identyfikator;
@dynamic name;
@dynamic parentId;
@dynamic products;

@end
