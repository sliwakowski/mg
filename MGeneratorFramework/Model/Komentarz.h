//
//  Komentarz.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 06.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Komentarz : NSManagedObject

@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSNumber * komentarzId;
@property (nonatomic, retain) NSString * nick;
@property (nonatomic, retain) NSNumber * produktId;
@property (nonatomic, retain) NSNumber * rate;

@end
