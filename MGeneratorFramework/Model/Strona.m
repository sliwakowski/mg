//
//  Strona.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 06.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "Strona.h"


@implementation Strona

@dynamic title;
@dynamic url;

@end
