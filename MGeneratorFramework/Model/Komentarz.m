//
//  Komentarz.m
//  aplikacja
//
//  Created by Adam Śliwakowski on 06.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "Komentarz.h"


@implementation Komentarz

@dynamic comment;
@dynamic date;
@dynamic komentarzId;
@dynamic nick;
@dynamic produktId;
@dynamic rate;

@end
