//
//  Kategoria.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 06.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Produkt;

@interface Kategoria : NSManagedObject

@property (nonatomic, retain) NSNumber * disable;
@property (nonatomic, retain) NSNumber * identyfikator;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * parentId;
@property (nonatomic, retain) NSSet *products;
@end

@interface Kategoria (CoreDataGeneratedAccessors)

- (void)addProductsObject:(Produkt *)value;
- (void)removeProductsObject:(Produkt *)value;
- (void)addProducts:(NSSet *)values;
- (void)removeProducts:(NSSet *)values;

@end
