//
//  Produkt.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 06.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Kategoria;

@interface Produkt : NSManagedObject

@property (nonatomic, retain) NSString * attributes;
@property (nonatomic, retain) NSNumber * barcode;
@property (nonatomic, retain) NSNumber * categoryId;
@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSNumber * disable;
@property (nonatomic, retain) NSNumber * identyfikator;
@property (nonatomic, retain) NSString * images;
@property (nonatomic, retain) NSNumber * isBestseller;
@property (nonatomic, retain) NSNumber * isNew;
@property (nonatomic, retain) NSNumber * isPromotion;
@property (nonatomic, retain) NSString * mainImage;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSNumber * promotionPrice;
@property (nonatomic, retain) NSNumber * shopId;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSSet *categories;
@end

@interface Produkt (CoreDataGeneratedAccessors)

- (void)addCategoriesObject:(Kategoria *)value;
- (void)removeCategoriesObject:(Kategoria *)value;
- (void)addCategories:(NSSet *)values;
- (void)removeCategories:(NSSet *)values;

@end
