//
//  Dostawa.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 06.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Dostawa : NSManagedObject

@property (nonatomic, retain) NSString * detail;
@property (nonatomic, retain) NSNumber * identyfikator;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * price;

@end
