//
//  Koszyk.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 06.11.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Koszyk : NSManagedObject

@property (nonatomic, retain) NSString * attributes;
@property (nonatomic, retain) NSNumber * identyfikator;
@property (nonatomic, retain) NSNumber * ilosc;
@property (nonatomic, retain) NSNumber * timestamp;
@property (nonatomic, retain) NSNumber * ulubione;

@end
