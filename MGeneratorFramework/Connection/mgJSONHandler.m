//
//  mgJSONHandler.m
//  mGenerator
//
//  Created by Macbook Pro on 25.02.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgJSONHandler.h"
#import "AFNetworking.h"
#import "mgTextHelper.h"
#import "Produkt.h"
#import "Kategoria.h"
#import "Dostawa.h"
#import "Komentarz.h"
#import "Strona.h"
#import <MagicalRecord/MagicalRecord.h>


@implementation mgJSONHandler


+(void)synchronizeProducts:(synchronizeCompletion) completionBlock{
    NSArray*oldProducts = [Produkt MR_findAll];
    NSString* lastSynchronization;
    if([oldProducts count]==0) lastSynchronization = @"0";
    else lastSynchronization = [mgTextHelper lastSynchronizationForType:@"products" set:NO];
    [mgJSONHandler synchronizeProducts:completionBlock page:1 time:lastSynchronization];
}
+(void)synchronizeProducts:(synchronizeCompletion) completionBlock page:(int) page time:(NSString*) lastSynchronization{
    NSLog(@"Start synchronize products");
    
    __block NSArray*productsArray = [NSArray new];
    NSString*fullSynchronizationUrl =
    [NSString stringWithFormat:@"%@%@&time=%@%@%@&limit=100",
     [mgTextHelper getValueForKeyFromConfiguration:@"json_products"],
     [mgTextHelper shopId],
     lastSynchronization,
     [NSString stringWithFormat:@"&key=%@", [mgTextHelper appKey]],
     [NSString stringWithFormat:@"&page=%i", page]];
     //[@"&exclude=description|images|attributes|details" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:fullSynchronizationUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [mgTextHelper lastSynchronizationForType:@"products" set:YES];
        NSDictionary*productsDictionary = responseObject;
        productsArray = [productsDictionary valueForKey:@"produkty"];
        
        NSLog(@"br1");
        NSMutableArray* productCategoryTable = [NSMutableArray new];
        for(NSDictionary*singleProductDictionary in productsArray){
            
            NSDate* startDate = [[NSDate alloc] init];
            
            long identyfikator = [[singleProductDictionary objectForKey:@"id"] longValue];
            int shopId = [[singleProductDictionary objectForKey:@"shop_id"] intValue];
            int categoryId = [[singleProductDictionary objectForKey:@"category_id"] intValue];
            NSString*name = [singleProductDictionary objectForKey:@"name"];
            NSString*text = [singleProductDictionary objectForKey:@"description"];
            NSString*details = [singleProductDictionary objectForKey:@"details"];
            NSString*priceString = [singleProductDictionary objectForKey:@"price"] ? : @"";
            NSString*promotionPriceString = [singleProductDictionary objectForKey:@"promotion_price"];
            
            NSNumber*price = @([priceString doubleValue]);
            NSNumber*promotionPrice = @([promotionPriceString doubleValue]);
            NSNumber*barcode = [singleProductDictionary objectForKey:@"barcode"];
            
            BOOL isNew = [[singleProductDictionary objectForKey:@"is_new"] intValue]==0 ? NO :YES;
            BOOL isPromotion = [[singleProductDictionary objectForKey:@"is_promotion"] intValue]==0 ? NO :YES;
            BOOL isBestseller = [[singleProductDictionary objectForKey:@"is_bestseller"] intValue]==0 ? NO :YES;
            BOOL disable = [[singleProductDictionary objectForKey:@"disable"] intValue]==0 ? NO :YES;
            NSString*images = @"";
            NSArray*imagesArray = [singleProductDictionary objectForKey:@"images"];
            for(NSString* imageString in imagesArray){
                images = [NSString stringWithFormat:@"%@;%@", images, imageString];
            }
            NSString* mainImage = [singleProductDictionary objectForKey:@"main_image"];
            if(mainImage==nil) mainImage = [imagesArray objectAtIndex:0];
            NSArray* attributesArray = [singleProductDictionary objectForKey:@"attributes"];
            NSString* attributes = @"";
            for(NSDictionary*singleAttrDict in attributesArray){
                attributes = [NSString stringWithFormat:@"%@%@#%@&", attributes, singleAttrDict[@"name"], singleAttrDict[@"val"]];
            }
            Produkt *produkt = nil;
            if([lastSynchronization isEqual: @"0"]) {
                produkt = [Produkt MR_createEntity];
            } else {
                if(![mgJSONHandler checkIfProductExists:identyfikator]){
                    produkt = [Produkt MR_createEntity];
                    
                }else{
                    // UPDATE
                    NSArray *produkty = [Produkt MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithLong:identyfikator] andOrderBy:@"identyfikator" ascending:YES];
                    produkt = [produkty objectAtIndex:0];
                }
            }
            
            produkt.identyfikator = [NSNumber numberWithLong:identyfikator];
            produkt.shopId = [NSNumber numberWithInt:shopId];
            produkt.categoryId = [NSNumber numberWithInt:categoryId];
            produkt.name = name;
            produkt.text = text;
            produkt.details = details;
            produkt.price = price;
            produkt.promotionPrice = promotionPrice;
            produkt.isNew =[NSNumber numberWithBool:isNew];
            produkt.isPromotion = [NSNumber numberWithBool:isPromotion];
            produkt.isBestseller = [NSNumber numberWithBool:isBestseller];
            produkt.disable = [NSNumber numberWithBool:disable];
            produkt.images = images;
            produkt.mainImage = mainImage;
            produkt.attributes = attributes;
            produkt.barcode = barcode;
            
            NSArray* categories = [singleProductDictionary objectForKey:@"categories"];
            
            if(categories && [categories isKindOfClass:[NSArray class]])
            {
                [productCategoryTable addObject:@{@"product":produkt, @"categories": categories}];
//                for (NSNumber* singleCategoryID in categories) {
//                    Kategoria* singleCategory = [mgJSONHandler checkIfCategoryExists:singleCategoryID.intValue];
//                    if(singleCategory==nil){
//                        singleCategory = [Kategoria MR_createEntity];
//                        singleCategory.identyfikator = singleCategoryID;
//                    }
//                    [produkt addCategoriesObject:singleCategory];
//                }
            }
            
            NSLog(@"%f", [[[NSDate alloc] init] timeIntervalSinceDate:startDate]*1000);
        }
        
        //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        NSLog(@"End synchronize products");
        if([productsArray count]>0) [mgJSONHandler synchronizeProducts:completionBlock page:(page+1) time:lastSynchronization];
        
        if(page==1) {
            completionBlock(YES);
        } else {
            
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            for(NSDictionary* map in productCategoryTable){
                Produkt* produkt = map[@"product"];
                NSArray* categories = map[@"categories"];
                NSMutableSet* setOfCategories = [NSMutableSet new];
                for (NSNumber* singleCategoryID in categories) {
                    Kategoria* singleCategory = nil;
                    NSArray *kategorie = [Kategoria MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithInt:singleCategoryID.intValue] andOrderBy:@"identyfikator" ascending:YES];
                    if([kategorie count]>0) singleCategory = [kategorie objectAtIndex:0];
                    
                    if(singleCategory!=nil){
//                        singleCategory = [Kategoria MR_createEntity];
//                        singleCategory.identyfikator = singleCategoryID;
                        [setOfCategories addObject:singleCategory];
                    }
                    //[[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
                    
                }
                [produkt addCategories:setOfCategories];
            }
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
            
        });
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}

+(void)synchronizeCategories:(synchronizeCompletion) completionBlock{
    NSArray* wszystkieKategoriaArray = [Kategoria MR_findByAttribute:@"identyfikator" withValue:@1];
    if([wszystkieKategoriaArray count]==0){
        Kategoria* wszystkieKategoria = [Kategoria MR_createEntity];
        wszystkieKategoria.identyfikator = @1;
        wszystkieKategoria.parentId = @1;
        wszystkieKategoria.name = @"Wszystkie produkty";
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
    
    NSArray*oldCategories = [Kategoria MR_findAll];
    NSString* lastSynchronization;
    if([oldCategories count]==0) lastSynchronization = @"0";
    else lastSynchronization = [mgTextHelper lastSynchronizationForType:@"categories" set:NO];
    NSString*fullSynchronizationUrl =
        [NSString stringWithFormat:@"%@%@&time=%@%@",
         [mgTextHelper getValueForKeyFromConfiguration:@"json_categories"],
         [mgTextHelper shopId],
         lastSynchronization,
         [NSString stringWithFormat:@"&key=%@", [mgTextHelper appKey]]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:fullSynchronizationUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [mgTextHelper lastSynchronizationForType:@"categories" set:YES];
        NSDictionary*categoriesDictionary = responseObject;
        NSArray*categoriesArray = [[categoriesDictionary valueForKey:@"response"] valueForKey:@"kategorie"];
        for(NSDictionary*singleCategoryDictionary in categoriesArray){
            int identyfikator = [[singleCategoryDictionary objectForKey:@"id"] intValue];
            int parentId = [[singleCategoryDictionary objectForKey:@"parent_id"] intValue];
            NSString*name = [singleCategoryDictionary objectForKey:@"name"];
            BOOL disable = [[singleCategoryDictionary objectForKey:@"disable"] boolValue];
            if(!disable){
                Kategoria*kategoria = nil;
                if(![mgJSONHandler checkIfCategoryExists:identyfikator]){
                    kategoria = [Kategoria MR_createEntity];
                    
                }else{
                    // UPDATE
                    NSArray *kategorie = [Kategoria MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithInt:identyfikator] andOrderBy:@"identyfikator" ascending:YES];
                    kategoria = [kategorie objectAtIndex:0];
                }
                kategoria.identyfikator = [NSNumber numberWithInt:identyfikator];
                kategoria.parentId = [NSNumber numberWithInt:parentId];
                kategoria.name = name;
                kategoria.disable = [NSNumber numberWithBool:disable];
                [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
            }
            
        }
        completionBlock(YES);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

+(void)synchronizeShippings:(synchronizeCompletion) completionBlock{
    NSArray*oldShippings = [Dostawa MR_findAll];
    NSString* lastSynchronization;
    if([oldShippings count]==0) lastSynchronization = @"0";
    else lastSynchronization = [mgTextHelper lastSynchronizationForType:@"shippings" set:NO];
    NSString*fullSynchronizationUrl =
        [NSString stringWithFormat:@"%@%@&time=%@%@",
         [mgTextHelper getValueForKeyFromConfiguration:@"json_shippings"],
         [mgTextHelper shopId],
         lastSynchronization,
         [NSString stringWithFormat:@"&key=%@", [mgTextHelper appKey]]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:fullSynchronizationUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [mgTextHelper lastSynchronizationForType:@"shippings" set:YES];
        NSDictionary*shippingsDictionary = responseObject;
        NSArray*shippingsArray = [shippingsDictionary valueForKey:@"przesylki"];
        for(NSDictionary*singleShippingDictionary in shippingsArray){
            int identyfikator = [[singleShippingDictionary objectForKey:@"id"] intValue];
            NSString*name = [singleShippingDictionary objectForKey:@"name"];
            NSString*detail = [singleShippingDictionary objectForKey:@"description"];
            NSString*price = [singleShippingDictionary objectForKey:@"price"];
            NSNumber * priceNumber = [NSNumber numberWithFloat:[price floatValue]];
            Dostawa*dostawa = nil;
            if(![mgJSONHandler checkIfShippingExists:identyfikator]){
                dostawa = [Dostawa MR_createEntity];
                
            }else{
                // UPDATE
                NSArray *dostawy = [Dostawa MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithInt:identyfikator] andOrderBy:@"identyfikator" ascending:YES];
                dostawa = [dostawy objectAtIndex:0];
            }
            dostawa.identyfikator = [NSNumber numberWithInt:identyfikator];
            dostawa.name=name;
            dostawa.detail=detail;
            dostawa.price=priceNumber;
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        }
        completionBlock(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

+(void)synchronizeSites:(synchronizeCompletion) completionBlock{
    NSArray*oldSites = [Strona MR_findAll];
    NSString* lastSynchronization;
    if([oldSites count]==0) lastSynchronization = @"0";
    else lastSynchronization = [mgTextHelper lastSynchronizationForType:@"sites" set:NO];
    NSString*fullSynchronizationUrl =
        [NSString stringWithFormat:@"%@%@&time=%@%@",
         [mgTextHelper getValueForKeyFromConfiguration:@"json_sites"],
         [mgTextHelper shopId],
         lastSynchronization,
         [NSString stringWithFormat:@"&key=%@", [mgTextHelper appKey]]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:fullSynchronizationUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [mgTextHelper lastSynchronizationForType:@"sites" set:YES];
        NSDictionary*shippingsSites = responseObject;
        NSArray*sitesArray = [shippingsSites valueForKey:@"strony"];
        for(NSDictionary*singleSiteDictionary in sitesArray){
            NSString*title = [singleSiteDictionary objectForKey:@"title"];
            NSString*url = [singleSiteDictionary objectForKey:@"url"];
            Strona*strona = nil;
            if(![mgJSONHandler checkIfSiteExists:title]){
                strona = [Strona MR_createEntity];
                
            }else{
                // UPDATE
                NSArray *strony = [Strona MR_findByAttribute:@"title" withValue:title andOrderBy:@"title" ascending:YES];
                strona = [strony objectAtIndex:0];
            }
            strona.title = title;
            strona.url = url;
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        }
        completionBlock(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

+(void)synchronizeCommentsForProductId:(int)productId completionBlock:(synchronizeCompletion)completionBlock{
    NSString*fullSynchronizationUrl =
        [NSString stringWithFormat:@"%@%i%@",
         [mgTextHelper getValueForKeyFromConfiguration:@"json_single_product_comments"],
         productId,
         [NSString stringWithFormat:@"&key=%@", [mgTextHelper appKey]]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:fullSynchronizationUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary*commentsDictionary = responseObject;
        NSArray*commentsArray = [commentsDictionary valueForKey:@"komentarze"];
        for(NSDictionary*singleCommentDictionary in commentsArray){
            int identyfikator = [[singleCommentDictionary objectForKey:@"id"] intValue];
            NSString*nick = [singleCommentDictionary objectForKey:@"nick"];
            NSString*comment = [singleCommentDictionary objectForKey:@"comment"];
            float rate = [[singleCommentDictionary objectForKey:@"rate"] floatValue];
            NSString*date = [[singleCommentDictionary objectForKey:@"created"] description];
            Komentarz*komentarz = nil;
            if(![mgJSONHandler checkIfCommentExists:identyfikator]){
                 komentarz = [Komentarz MR_createEntity];
            }else{
                // UPDATE
                NSArray *komentarze = [Komentarz MR_findByAttribute:@"komentarzId" withValue:[NSNumber numberWithInt:identyfikator] andOrderBy:@"komentarzId" ascending:YES];
                komentarz = [komentarze objectAtIndex:0];
            }
            komentarz.komentarzId = [NSNumber numberWithInt:identyfikator];
            komentarz.nick = nick;
            komentarz.comment = comment;
            komentarz.rate = [NSNumber numberWithFloat:rate];
            komentarz.date = date;
            komentarz.produktId = [NSNumber numberWithInt:productId];
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        }
        completionBlock(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
+(void)synchronizeAddresses:(addressesSynchronizeCompletion) addressesCompletionBlock{
    NSString*fullSynchronizationUrl =
        [NSString stringWithFormat:@"%@%@%@",
         [mgTextHelper getValueForKeyFromConfiguration:@"json_addresses"],
         [mgTextHelper shopId],
         [NSString stringWithFormat:@"&key=%@", [mgTextHelper appKey]]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:fullSynchronizationUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray* addressesArray = responseObject;
        addressesCompletionBlock(addressesArray);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
+(void)registerDevice:(NSString*)deviceId {
    NSString* data = [NSString stringWithFormat:@"{\"shopId\":\"%@\",\"type\":\"ios\",\"deviceId\":\"%@\"}",[mgTextHelper shopId], deviceId];
    data = [data stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString* deviceRegistrationUrl =
        [NSString stringWithFormat:@"%@%@%@",
         [mgTextHelper getValueForKeyFromConfiguration:@"json_shopdevice"],
         data,
         [NSString stringWithFormat:@"&key=%@", [mgTextHelper appKey]]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:deviceRegistrationUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success device registration");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
+(void)getPaymentData:(getCompletionWithDictionary) completionBlock{
    NSString*fullSynchronizationUrl =
        [NSString stringWithFormat:@"%@%@%@",
         [mgTextHelper getValueForKeyFromConfiguration:@"json_ppldata"],
         [mgTextHelper shopId],
         [NSString stringWithFormat:@"&key=%@", [mgTextHelper appKey]]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:fullSynchronizationUrl parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary* responseObject) {
        completionBlock(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
+(BOOL)checkIfProductExists:(long)productId{
    NSArray *produkty = [Produkt MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithLong:productId] andOrderBy:@"identyfikator" ascending:YES];
    if([produkty count]>0) return YES;
    return NO;
}
+(Kategoria*)checkIfCategoryExists:(int)categoryId{
    NSArray *kategorie = [Kategoria MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithInt:categoryId] andOrderBy:@"identyfikator" ascending:YES];
    if([kategorie count]>0) return [kategorie objectAtIndex:0];
    return nil;
}
+(BOOL)checkIfShippingExists:(int)shippingId{
    NSArray *dostawy = [Dostawa MR_findByAttribute:@"identyfikator" withValue:[NSNumber numberWithInt:shippingId] andOrderBy:@"identyfikator" ascending:YES];
    if([dostawy count]>0) return YES;
    return NO;
}
+(BOOL)checkIfSiteExists:(NSString*)siteTitle{
    NSArray *strony = [Strona MR_findByAttribute:@"title" withValue:siteTitle andOrderBy:@"title" ascending:YES];
    if([strony count]>0) return YES;
    return NO;
}
+(BOOL)checkIfCommentExists:(int)commentId{
    NSArray *komentarze = [Komentarz MR_findByAttribute:@"komentarzId" withValue:[NSNumber numberWithInt:commentId] andOrderBy:@"komentarzId" ascending:YES];
    if([komentarze count]>0) return YES;
    return NO;
}
+(void)checkAuthAppPreview:(NSString*)appKey completionBlock:(synchronizeCompletion)completionBlock{
    NSString*fullSynchronizationUrl =
        [NSString stringWithFormat:@"%@%@%@",
         [mgTextHelper getValueForKeyFromConfiguration:@"json_authapp"],
         appKey,
         [NSString stringWithFormat:@"&key=%@", [mgTextHelper appKey]]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:fullSynchronizationUrl parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary* responseObject) {
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            NSNumber* status = responseObject[@"status"];
            if(status.intValue==1)
            {
                [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"shop_id"] forKey:@"demoShopId"];
                // INFO
                NSMutableDictionary* config = [NSMutableDictionary new];
                for (NSString*key in [responseObject[@"info"] allKeys]) {
                    if(![responseObject[@"info"][key] isKindOfClass:[NSNull class]])
                    {
                        [config setObject:responseObject[@"info"][key] forKey:key];
                    }
                }
                [[NSUserDefaults standardUserDefaults] setObject:config forKey:@"config"];
                // COLORS
                NSMutableDictionary* colors = [NSMutableDictionary new];
                for (NSString*key in [responseObject[@"layout"][@"g1"] allKeys]) {
                    [colors setObject:responseObject[@"layout"][@"g1"][key] forKey:key];
                }
                [[NSUserDefaults standardUserDefaults] setObject:colors forKey:@"colors"];

            }
        }
        completionBlock(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];

}


@end
