//
//  mgPOSTHandler.h
//  mGenerator
//
//  Created by Macbook Pro on 27.02.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dostawa.h"
#import "Koszyk.h"
typedef void(^postCompletion)(NSString*response);
typedef void(^postCompletionWithDictionary)(NSDictionary*response);

@interface mgPOSTHandler : NSObject

+(void)postStatisticsWithAction:(NSString*)action andProductId:(NSString*)productId completionHandler:(postCompletion) completionBlock;
+(void)postCheckUserLogin:(NSString*)email withPassword:(NSString*)password completionHandler:(postCompletion) completionBlock;
+(void)postCommentWithProductId:(NSString*)productId comment:(NSString*)comment nick:(NSString*)nick rate:(int)rate completionHandler:(postCompletion) completionBlock;
+(void)registerOrderWith:(Dostawa*)delivery totalPrice:(float)totalPrice completionHandler:(postCompletion) completionBlock;
+(void)registerUserWith:(NSDictionary*)userData update:(BOOL)update completionHandler:(postCompletionWithDictionary) completionBlock;
@end
