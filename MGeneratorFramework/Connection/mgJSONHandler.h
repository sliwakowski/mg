//
//  mgJSONHandler.h
//  mGenerator
//
//  Created by Macbook Pro on 25.02.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^synchronizeCompletion)(BOOL);
typedef void(^addressesSynchronizeCompletion)(NSArray* array);
typedef void(^getCompletionWithDictionary)(NSDictionary*response);
@interface mgJSONHandler : NSObject

+(void)synchronizeProducts:(synchronizeCompletion) completionBlock;
+(void)synchronizeProducts:(synchronizeCompletion) completionBlock page:(int) page time:(NSString*) lastSynchronization;
+(void)synchronizeCategories:(synchronizeCompletion) completionBlock;
+(void)synchronizeShippings:(synchronizeCompletion) completionBlock;
+(void)synchronizeSites:(synchronizeCompletion) completionBlock;
+(void)synchronizeCommentsForProductId:(int)productId completionBlock:(synchronizeCompletion)completionBlock;
+(void)synchronizeAddresses:(addressesSynchronizeCompletion) addressesCompletionBlock;
+(void)registerDevice:(NSString*)deviceId;
+(void)checkAuthAppPreview:(NSString*)appKey completionBlock:(synchronizeCompletion)completionBlock;
+(void)getPaymentData:(getCompletionWithDictionary) completionBlock;
@end
