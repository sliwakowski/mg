//
//  mgPOSTHandler.m
//  mGenerator
//
//  Created by Macbook Pro on 27.02.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgPOSTHandler.h"
#import "AFHTTPRequestOperationManager.h"
#import "mgTextHelper.h"
#import "Produkt.h"
#import "Kategoria.h"
#import "Dostawa.h"
#import "Komentarz.h"
#import "NSString+MD5.h"
#import "NSString+Base64.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation mgPOSTHandler

+(void)postStatisticsWithAction:(NSString*)action andProductId:(NSString*)productId completionHandler:(postCompletion) completionBlock{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString*fullPostUrl =
    [NSString stringWithFormat:@"%@%@",
     [mgTextHelper getValueForKeyFromConfiguration:@"post_statistics"],
     [NSString stringWithFormat:@"?key=%@", [mgTextHelper appKey]]];
    
    NSDictionary *parameters = @{@"shopId": [mgTextHelper shopId], @"clientId": [mgTextHelper client:@"id"], @"action": action, @"productId": productId};
    [manager POST:fullPostUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString*responseString =[responseObject objectForKey:@"status"];
        NSLog(@"Statistics status: %@", responseString);
        completionBlock(responseString);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
+(void)postCheckUserLogin:(NSString*)email withPassword:(NSString*)password completionHandler:(postCompletion) completionBlock{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString*fullPostUrl =
        [NSString stringWithFormat:@"%@%@",
         [mgTextHelper getValueForKeyFromConfiguration:@"post_client_login"],
         [NSString stringWithFormat:@"?key=%@", [mgTextHelper appKey]]];
    
    NSLog(@"fullPostUrl: %@", fullPostUrl);
    NSString *md5Password = [password MD5String];
    
    NSDictionary *parameters = @{@"email": email, @"password": md5Password, @"shopId":[mgTextHelper shopId]};
    [manager POST:fullPostUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString*responseString =[NSString stringWithFormat:@"%i",[[responseObject objectForKey:@"status"] intValue]];
        NSDictionary*userDictionary = (NSDictionary*) [responseObject objectForKey:@"client"];
        NSLog(@"Login status: %@", responseString);
        
        //SAVE USER TO NSUserDefaults
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:userDictionary forKey:@"user"];
        [defaults synchronize];
        
        completionBlock(responseString);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
+(void)postCommentWithProductId:(NSString*)productId comment:(NSString*)comment nick:(NSString*)nick rate:(int)rate completionHandler:(postCompletion) completionBlock{
    NSMutableDictionary*fullCommentJSON = [NSMutableDictionary new];
    [fullCommentJSON setObject:productId forKey:@"productId"];
    NSMutableDictionary*commentJSON = [NSMutableDictionary new];
    [commentJSON setObject:comment forKey:@"comment"];
    [commentJSON setObject:nick forKey:@"nick"];
    [commentJSON setObject:[NSNumber numberWithInt:rate] forKey:@"rate"];
    [fullCommentJSON setObject:commentJSON forKey:@"comment"];
    
    NSError *error;
    NSString *jsonString = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:fullCommentJSON
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    //NSDictionary*parameters = @{@"data": fullCommentJSON};
    NSDictionary*parameters = @{@"data": jsonString};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString*fullPostUrl =
        [NSString stringWithFormat:@"%@%@",
        [mgTextHelper getValueForKeyFromConfiguration:@"post_product_comment"],
        [NSString stringWithFormat:@"?key=%@", [mgTextHelper appKey]]];
    
    NSLog(@"fullPostUrl: %@", fullPostUrl);
    [manager POST:fullPostUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString*responseString =[responseObject objectForKey:@"status"];
        NSLog(@"Comment post status: %@", responseString);
        completionBlock(@"1");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completionBlock(0);
    }];

}
+(void)registerOrderWith:(Dostawa*)delivery totalPrice:(float)totalPrice completionHandler:(postCompletion) completionBlock {
    NSString* shopId = [mgTextHelper shopId];
    NSNumber* deliveryId = delivery.identyfikator;
    NSString* clientId = [mgTextHelper client:@"id"];
    
    NSMutableDictionary* fullOrderJSON = [NSMutableDictionary new];
    [fullOrderJSON setObject:shopId forKey:@"shopId"];
    [fullOrderJSON setObject:deliveryId forKey:@"deliveryId"];
    [fullOrderJSON setObject:clientId forKey:@"clientId"];
    [fullOrderJSON setObject:[NSNumber numberWithFloat:totalPrice] forKey:@"totalPrice"];
    // Products
    NSNumber* ulubione = [NSNumber numberWithBool:NO];
    NSArray* koszykArray = (NSArray*) [Koszyk MR_findByAttribute:@"ulubione" withValue:ulubione];
    NSMutableArray* products = [NSMutableArray new];
    for(Koszyk* koszyk in koszykArray) {
        Produkt* product = [Produkt MR_findFirstByAttribute:@"identyfikator" withValue:koszyk.identyfikator];
        NSNumber* price = product.promotionPrice.intValue == 0 ? product.price : product.promotionPrice;

        NSMutableDictionary* productDictionary = [@{@"id":product.identyfikator, @"quantity":koszyk.ilosc, @"price":price} mutableCopy];
        [products addObject:productDictionary];
    }
    [fullOrderJSON setObject:products forKey:@"products"];
    
    NSError *error;
    NSString *fullOrderString = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:fullOrderJSON
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        fullOrderString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSDictionary*parameters = @{@"data": fullOrderString};
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSString*fullPostUrl =
        [NSString stringWithFormat:@"%@%@",
        [mgTextHelper getValueForKeyFromConfiguration:@"post_register_order"],
        [NSString stringWithFormat:@"?key=%@", [mgTextHelper appKey]]];
    
    [manager POST:fullPostUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary*responseDictionary =responseObject;
        NSError *error;
        NSString *fullResponseString = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseDictionary
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];
        fullResponseString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSString* status = [NSString stringWithFormat:@"%@",[[responseDictionary objectForKey:@"response"] objectForKey:@"sessionId"]];
        completionBlock(fullResponseString);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completionBlock(0);
    }];

}
+(void)registerUserWith:(NSDictionary*)userData update:(BOOL)update completionHandler:(postCompletionWithDictionary) completionBlock{
    NSString*fullPostUrl =
    [NSString stringWithFormat:@"%@%@",
        [mgTextHelper getValueForKeyFromConfiguration:@"post_client_register"],
        [NSString stringWithFormat:@"?key=%@", [mgTextHelper appKey]]];
    NSString* deviceTokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceTokenString"];
    NSMutableDictionary* fullRegistrationJSON = [userData mutableCopy];
    //NSString *md5Password = [haslo MD5String];
    //NSString* base64Password = [haslo base64];
    if(update){
        [fullRegistrationJSON setObject:[mgTextHelper client:@"id"] forKey:@"id"];
        fullPostUrl =
            [NSString stringWithFormat:@"%@%@",
             [mgTextHelper getValueForKeyFromConfiguration:@"post_client_change"],
             [NSString stringWithFormat:@"?key=%@", [mgTextHelper appKey]]];
    }
    else {
        if(deviceTokenString != nil)
        {
            [fullRegistrationJSON setObject:deviceTokenString forKey:@"deviceTokenString"];
        }
    }
    [fullRegistrationJSON setObject:[mgTextHelper shopId] forKey:@"shopId"];
    NSError *error;
    NSString *fullRegistrationString = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:fullRegistrationJSON options:0 error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        fullRegistrationString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSDictionary*parameters = @{@"data": fullRegistrationString};
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSLog(@"fullPostUrl: %@", fullPostUrl);
    [manager POST:fullPostUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary*responseDictionary =responseObject;
        NSString* status = [NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]];
        
        NSDictionary*userDictionary = (NSDictionary*) [responseObject objectForKey:@"client"];
        
        //SAVE USER TO NSUserDefaults
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:userDictionary forKey:@"user"];
        [defaults synchronize];
        
        NSLog(@"Comment post status: %@", status);
        completionBlock(responseDictionary);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completionBlock(0);
    }];
    
}


@end
