//
//  mgSimilarProductView.h
//  aplikacja
//
//  Created by Macbook Pro on 20.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Produkt.h"
@interface mgSimilarProductView : UIView

@property (nonatomic, strong) Produkt* produkt;
- (id)initWithFrame:(CGRect)frame andProduct:(Produkt*)produkt;
@end
