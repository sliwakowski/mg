//
//  mgSegmentedControl.h
//  aplikacja
//
//  Created by Adam Śliwakowski on 29.07.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol mgSegmentedControlDelegate <NSObject>
@required
- (void)didReceiveTouchFromState:(int)oldState toState:(int)newState;

@end
@interface mgSegmentedControl : UISegmentedControl

@property (nonatomic, strong) id<mgSegmentedControlDelegate>            delegate;

@end
