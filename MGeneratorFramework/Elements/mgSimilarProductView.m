//
//  mgSimilarProductView.m
//  aplikacja
//
//  Created by Macbook Pro on 20.06.2014.
//  Copyright (c) 2014 mgenerator. All rights reserved.
//

#import "mgSimilarProductView.h"
#import "UIImageView+WebCache.h"
#import "mgTextHelper.h"
@implementation mgSimilarProductView

- (id)initWithFrame:(CGRect)frame andProduct:(Produkt*)produkt
{
    self = [super initWithFrame:frame];
    if (self) {
        _produkt = produkt;
        [self createImage];
        [self createTitle];
        [self createPrice];
    }
    return self;
}

- (void)createImage{
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 120)];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@&w=235",_produkt.mainImage]]
          placeholderImage:[UIImage imageNamed:@"ipadCollection.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imageView];
}
- (void)createTitle{
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 123, self.frame.size.width-10, 20.0f)];
    titleLabel.numberOfLines = 1;
    titleLabel.textColor = UIColorFromRGB(0x939393);
    titleLabel.text = _produkt.name;
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0f];
    [self addSubview:titleLabel];
}
- (void)createPrice{
    //NOWA CENA
    UILabel* nowaCenaLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 143, 100, 18.0f)];
    nowaCenaLabel.textColor = [mgTextHelper mainColor];
    nowaCenaLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0f];
    [mgTextHelper configureNewPriceLabel:nowaCenaLabel withProdukt:_produkt];
    CGRect nowaCenaFrame = nowaCenaLabel.frame;
    nowaCenaFrame.origin.x = 5;
    nowaCenaLabel.frame = nowaCenaFrame;
    //STARA CENA
    UILabel* staraCenaLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 143, 100, 16.0f)];
    staraCenaLabel.textColor = UIColorFromRGB(0x939393);
    staraCenaLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0f];
    [mgTextHelper configureOldPriceLabel:staraCenaLabel withNewPriceLabel:nowaCenaLabel withProdukt:_produkt];
    CGRect staraCenaFrame = staraCenaLabel.frame;
    staraCenaFrame.origin.x = nowaCenaLabel.frame.size.width+nowaCenaLabel.frame.origin.x + 10;
    staraCenaFrame.origin.y = staraCenaFrame.origin.y+2;
    staraCenaLabel.frame = staraCenaFrame;
    [self addSubview:nowaCenaLabel];
    [self addSubview:staraCenaLabel];
}
@end
